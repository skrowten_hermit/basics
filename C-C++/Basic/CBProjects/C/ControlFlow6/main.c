/*******************************************************************************
PROGRAM NAME    :   A018-for-loop-demo.c
TITLE           :   Program to Demonstrate For Loop.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstates a for loop by printing a
                    table using 2 loop variables with different increments.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int j;

    for (int i = 1, j = 2; i <= 5; ++i, j = j + 2)
        printf("%2d * %2d = %2d\n", i, j, i * j);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

 1 *  2 =  2
 2 *  4 =  8
 3 *  6 = 18
 4 *  8 = 32
 5 * 10 = 50

Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
