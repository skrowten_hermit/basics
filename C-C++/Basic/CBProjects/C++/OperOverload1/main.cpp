/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Operator Overloading in Complex Class.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate operator overloading through
                    a class implementing complex numbers.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Complex
{
    private:
        int real;
        int img;

    public:
        Complex(int r = 0, int i = 0)
        {
            real = r;
            img = i;
        }

        void display()
        {
            cout << real << " + i" << img << endl;
        }

        Complex operator+(Complex c)
        {
            Complex temp;
            temp.real = real + c1.real;
            temp.img = img + c1.img;
            return temp;
        }
};


int main()
{
    Complex c1(5,3), c2(10,5), c3;
    c3 = c1 + c2;
    c3.display();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
