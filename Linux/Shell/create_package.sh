#!/bin/bash


################################################################################
################################################################################
## File              :: create_package.sh
## Description       :: Makes a '.tar.gz' file out of the entire automation
##                      working directory.
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 23/01/2019
## Changes made      :: The script for fetching the latest tag from the Linux
##                      VoIP automation branch. Works as an update script.
## Changes made Date :: 23/01/2019
## Changes made by   :: Sreekanth S
## Execution example :: ./create_package.sh
################################################################################
################################################################################


echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..29}
echo "VoIP Automation Framework : Create Package"
echo $(printf '=%.0s' {1..100})
echo $'\n'

#Change the working directory, do git commands
cd
#echo $PWD

arg=$1
#echo "Input : $arg"
#echo

if [[ "$arg" == @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    echo "Help info for execution requested...."
    echo $'\n\n'
    echo $(printf '~%.0s' {1..120})
    printf ' %.0s' {1..53}
    echo "Help Info"
    echo $(printf '~%.0s' {1..120})
    echo $'\n\n'
    echo "Usage Description : " | boxes
    echo
    echo -e "./create_package.sh: \n \n makes a '.tar.gz' file out of the entire automation working directory." | boxes -d shell -s 120
    echo
    echo $'\n\n'
    echo $(printf '~%.0s' {1..120})
    printf ' %.0s' {1..53}
    echo "Help Info"
    echo $(printf '~%.0s' {1..120})
    echo $'\n\n'
    echo "Help info printed...."
elif [[ "$arg" != @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    sdk_ver=$1
    fname="automation_history/VA_archive_$1.tar.gz"
    cd ~/
    tar -czvf "$fname" --exclude=./automation/voip_automation/DVF_Automation/Output --exclude='.[^/]*' --transform='s!^\./!!' ./automation
fi

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..29}
echo "VoIP Automation Framework : Create Package"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0