/*******************************************************************************
PROGRAM NAME    :   A025-array-avg.c
TITLE           :   Program to Store Values in Array and Process Them.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program accepts grades as values from user, stores them
                    in an array andcomputes the average of the entered values.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define COUNT 10


int main()
{
    int grades[COUNT];
    long sum = 0;
    float avg = 0.0f;

    printf("Enter 10 grades :\n");

    for (int i = 0; i < COUNT; ++i)
    {
        printf("%2u> ", i + 1);
        scanf("%d", &grades[i]);
        sum += grades[i];
    }

    avg = (float) sum / COUNT;

    printf("\nThe Average of above entered grades is : %.2f\n", avg);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter 10 grades :
 1> 8
 2> 7
 3> 6
 4> 9
 5> 8
 6> 9
 7> 7
 8> 8
 9> 9
10> 10

The Average of above entered grades is : 8.10

Process returned 0 (0x0)   execution time : 18.516 s
Press ENTER to continue.

*******************************************************************************/
