/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to perform division validation.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to perform a division validation
                    by also checking for a divide by zero condition. The user
                    enters the numerator and denominator and gets the output if
                    a valid data is given.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int a, b, c;
    cout << "Enter two numbers for division : " << endl;
    cin >> a >> b;
    if(b == 0)
    {
        cout << "Invalid denominator!" << endl;
    }
    else
    {
        c = a / b;
        cout << c << endl;
    }
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
