/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Display digits of a Number.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to display all the digits in the given
                    number.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int n, r;
    cout << "Enter a no. : ";
    cin >> n;
    while(n != 0)
    {
        r = n % 10;
        n = n / 10;
        cout << r << " ";
    }
    cout << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
