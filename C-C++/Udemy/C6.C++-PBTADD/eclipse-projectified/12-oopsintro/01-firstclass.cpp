/*
 * 01-firstclass.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include <iostream>
using namespace std;
class Rectangle
{
    public:
        int length;
        int breadth;
        int area()
        {
            return length*breadth;
        }
        int perimeter()
        {
            return 2*(length+breadth);
        }
};
