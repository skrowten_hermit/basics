/*******************************************************************************
PROGRAM NAME    :   A007-arith-operations.c
TITLE           :   Program to Demonstrate Arithmetic Operations.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates different arithmetic operations
                    available in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int a = 33;
    int b = 15;
    int result = 0;

    result = a + b;
//    result = a - b;
//    result = a * b;
//    result = a / b;
//    result = a % b;
//    result = a ^ b;

//    printf("Result is : %d\n", result);
    printf("Result is : %d\n", a + b);
    printf("Result is : %d\n", ++a);
//    printf("Result is : %d\n", a++);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Result is : 48
Result is : 34

Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
