/*******************************************************************************
PROGRAM NAME    :   A070-struct-structofchrptrs.c
TITLE           :   Program to Demonstrate a Structure With Character Pointer
                    Members.
DATE            :   10/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create a structure with
                    character pointer type members, allocate memory and assign
                    values to them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SLEN 50


struct namedet
{
    char *fname;
    char *lname;
    int letters;
};


void getinfo(struct namedet *pd);


int main()
{
    printf("\n");

    return 0;
}


void getinfo(struct namedet *det)
{
    char temp[SLEN];

    printf("Please Enter First Name : ");
    s_gets(temp, SLEN);

    // Allocate memory to hold first name.
    det -> fname = (char *) malloc(strlen(temp) + 1);
    strcpy(det -> fname, temp);     // Copy first name to allocated memory.

    printf("Please Enter Last Name : ");
    s_gets(temp, SLEN);

    // Allocate memory to hold last name.
    det -> lname = (char *) malloc(strlen(temp) + 1);
    strcpy(det -> lname, temp);     // Copy last name to allocated memory.
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
