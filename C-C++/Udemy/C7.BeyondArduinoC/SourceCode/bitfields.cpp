#include <stdint.h>
#include <stdio.h>

typedef union{
  struct{
  	unsigned sunday    : 1;
  	unsigned monday    : 1;
  	unsigned tuesday   : 1;
  	unsigned wednesday : 1;
  	unsigned thursday  : 1;
  	unsigned friday    : 1;  	                        
  	unsigned saturday  : 1;
  } days;
  uint8_t byte_sized;
}weekdays;                            

int main(){
	weekdays active_days;
	active_days.byte_sized=0x36; // 0b00110110
	printf("Working days: ");
	if(active_days.days.monday) printf(" monday");
	if(active_days.days.tuesday) printf(" tuesday");
	if(active_days.days.wednesday) printf(" wednesday");
	if(active_days.days.thursday) printf(" thursday");
	if(active_days.days.friday) printf(" friday");
	if(active_days.days.saturday) printf(" saturday");
	if(active_days.days.sunday) printf(" sunday");
	
	printf("\n\nThis, by the way takes %d bytes :)",sizeof(weekdays));
}
