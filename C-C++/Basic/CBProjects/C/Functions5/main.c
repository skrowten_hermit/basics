/*******************************************************************************
PROGRAM NAME    :   A062-func-pass-pointer.c
TITLE           :   Program to Demonstrate Passing of Pointers to Functions.
DATE            :   20/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates passing of a pointer as argument
                    to a function.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/


#include <stdio.h>


void square(int * const x);


int main()
{
    int num1 = 9;                                                // Pass as integer.
    int *num2 = (int *)malloc(sizeof(int));                      // Pass as pointer.
    *num2 = 4;                                                   // Pass as pointer.

    printf("The square of the given number %d is : ", num1);     // Pass as integer.
    square(&num1);                                               // Pass as integer.
    printf("%d\n", num1);                                        // Pass as integer.

    printf("The square of the given number %d is : ", *num2);    // Pass as pointer.
    square(num2);                                                // Pass as pointer.
    printf("%d\n", *num2);                                       // Pass as pointer.

    return 0;
}


void square(int * const n)
{
    *n = (*n) * (*n);
}



/*******************************************************************************

OUTPUT  :
---------

The square of the given number 9 is : 81
The square of the given number 4 is : 16

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
