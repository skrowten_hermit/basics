#!/bin/bash


################################################################################
################################################################################
## File              :: env_install.sh
## Description       :: Installs all the dependent softwares and python
##                      libraries required for VoIP automation.
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 23/01/2019
## Changes made      :: Updated with latest libraries and softwares.
## Changes made Date :: 23/01/2019
## Changes made by   :: Sreekanth S
## Execution example :: ./env_install.sh
################################################################################
################################################################################



# Add application sources to repository and install applications/packages required for the environment

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
apt-get update -y
apt-add-repository ppa:git-core/ppa -y
apt-get update -y
add-apt-repository ppa:webupd8team/sublime-text-3 -y
apt-get update -y
apt-get install git -y
apt-get install git-core -y
apt-get install git-sh -y
apt-get install gitk -y
apt-get install google-chrome-stable -y
apt-get install synaptic -y
apt-get install samba -y
apt-get install smbclient -y
apt-get install putty -y
apt-get install putty-tools -y
apt-get install minicom -y
apt-get install filezilla -y
apt-get install python-pip -y
apt-get install python3-pip
pip install -U pip
apt-get install python-tksnack -y
apt-get install setserial -y
apt-get install build-essential -y
apt-get install sox -y
apt-get install mediainfo -y
apt-get install smbclient -y
apt-get install audacity -y
DEBIAN_FRONTEND=noninteractive apt-get install wireshark -y
apt-get install tshark
apt-get install ckermit -y
apt-get install tftp -y
apt-get install tftpd-hpa -y
apt-get install usbrelay -y
apt-get install libhidapi-dev -y
apt-get install sublime-text-installer -y
apt-get install xclip -y
apt-get install meld -y
apt-get install net-tools
snap install pycharm-community --classic
apt-get install vim -y
apt-get install repo -y
apt-get install diffstat -y
apt-get install texi2html -y
apt-get install texinfo -y
apt-get install subversion -y
apt-get install gawk -y
apt-get install chrpath -y
apt-get install libbfd-dev -y
apt-get install libssl-dev -y
apt-get install libusb-dev -y
apt-get install pkg-config -y
apt-get install essentia -y
apt-get install xvfb -y
apt-get install curl -y


# Installation of external python libraries

pip install scipy
pip install matplotlib
pip install pyserial
pip install pyparallel
pip install openpyxl
pip install selenium
pip install pyro4
pip install xmodem
pip install memory-profiler
pip3 install pyro4

# Other binaries

curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
