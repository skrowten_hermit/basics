/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Function Overloading using sum().
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate function overloading of a
                    function sum() so that it can accept a variety of data types
                    according to the values entered by the user.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


int sum(int a, int b)
{
    return a + b;
}


float sum(float a, float b)
{
    return a + b;
}


int sum(int a, int b, int c)
{
    return a + b + c;
}


int main()
{
    cout << sum(10, 5) << endl;
    cout << sum(12.5f, 3.4f) << endl;
    cout << sum(10, 20, 3) << endl;
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
