/*******************************************************************************
PROGRAM NAME    :   A054-pointers-voidtype.c
TITLE           :   Program to Demonstrate Void Type Pointers.
DATE            :   18/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates void type pointers and how to use
                    them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int x1 = -10;
    float x2 = 20.25;
    char x3 = 'k';
    void *ptr;

    printf("x1 = %d, x2 = %f, x3 = %c\n", x1, x2, x3);
    printf("x1 = %f, x2 = %d, x3 = %c\n", x1, x2, x3);
    printf("x1 = %f, x2 = %d, x3 = %d\n\n", (float)x1, (int)x2, (int)x3);
    // The following throws error "Invalid use of void expression" if *ptr is
    // uninitialized to a variable value.
//    printf("*ptr = ", (int)*ptr);

    ptr = &x1;

    printf("*ptr as \"int\" = %d\n", *(int *)ptr);
    printf("*ptr as \"float\" = %f\n", *(float *)ptr);
    printf("*ptr as \"unsigned int\" = %u\n", *(unsigned int *)ptr);
    printf("*ptr as \"long\" = %ld\n", *(long *)ptr);
    printf("*ptr as \"double\" = %lf\n\n", *(double *)ptr);

    ptr = &x2;

    printf("*ptr as \"float\" = %f\n", *(float *)ptr);
    printf("*ptr as \"int\" = %d\n", *(int *)ptr);
    printf("*ptr as \"double\" = %lf\n", *(double *)ptr);
    printf("*ptr as \"unsigned int\" = %u\n", *(unsigned int *)ptr);
    printf("*ptr as \"long\" = %ld\n\n", *(long *)ptr);

    ptr = &x3;

    printf("*ptr as \"char\" = %c\n", *(char *)ptr);
    printf("*ptr as \"int\" = %d\n", *(int *)ptr);
    printf("*ptr as \"float\" = %f\n", *(float *)ptr);
    printf("*ptr as \"double\" = %lf\n", *(double *)ptr);
    printf("*ptr as \"unsigned int\" = %u\n", *(unsigned int *)ptr);
    printf("*ptr as \"long\" = %ld\n\n", *(long *)ptr);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

x1 = -10, x2 = 20.250000, x3 = k
x1 = 20.250000, x2 = -10, x3 = k
x1 = -10.000000, x2 = 20, x3 = 107

*ptr as "int" = -10
*ptr as "float" = -nan
*ptr as "unsigned int" = 4294967286
*ptr as "long" = 4729342562987409398
*ptr as "double" = 150995072.000000

*ptr as "float" = 20.250000
*ptr as "int" = 1101135872
*ptr as "double" = -0.000000
*ptr as "unsigned int" = 1101135872
*ptr as "long" = -5913406371845242880

*ptr as "char" = k
*ptr as "int" = -2453
*ptr as "float" = -nan
*ptr as "double" = -0.000000
*ptr as "unsigned int" = 4294964843
*ptr as "long" = -6773412740053600661


Process returned 0 (0x0)   execution time : 0.005 s
Press ENTER to continue.

*******************************************************************************/
