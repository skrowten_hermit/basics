/*
 * 02-leapyrcheck.cpp
 * Program to to check if its a Leap Year
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */




#include <iostream>
using namespace std;
int main()
{
    int year;
    cout << "Enter a year: ";
    cin >> year;
    if (year % 4 == 0)
    {
        if (year % 100 == 0)
        {
            if (year % 400 == 0)
                cout << year << " is a leap year.";
            else
                cout << year << " is not a leap year.";
        }
        else
            cout << year << " is a leap year.";
    }
    else
        cout << year << " is not a leap year.";
    return 0;
}
