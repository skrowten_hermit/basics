/*******************************************************************************
PROGRAM NAME    :   A012-condition-operation.c
TITLE           :   Program to Demonstrate Conditional Operator.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates conditional operator in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    printf("Output : %d\n", 1 == 2 != 3);
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Output : 1

Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
