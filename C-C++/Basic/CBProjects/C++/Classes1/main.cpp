/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Example of a basic Student Class.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate a simple class and its
                    functions using a class Student.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


class Student
{
    private:
        int roll;
        string name;
        int mathMarks;
        int phyMarks;
        int chemMarks;

    public:
        Student(int r, string n, int m, int p, int c)
        {
            roll = r;
            name = n;
            mathMarks = m;
            phyMarks = p;
            chemMarks = c;
        }

        int total()
        {
            return mathMarks + phyMarks + chemMarks;
        }

        char grade()
        {
            float average = total() / 3;

            if(average > 60)
                return 'A';
            else if(average >= 40 && average < 60)
                return 'B';
            else
                return 'C';
        }
};


int main()
{
    int roll;
    string name;
    int m, p, c;

    cout << "Enter Roll number of the Student : ";
    cin >> roll;
    cout << "Enter the Name of the Student : ";
    cin >> name;
    cout << "Enter the marks in 3 subjects : " << endl;
    cin >> m >> p >> c;

    Student s(roll, name, m, p, c);

    cout << "Total Marks : " << s.total() << endl;
    cout << "Grade of Student : " << s.grade() << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
