/*******************************************************************************
PROGRAM NAME    :   A038-strings-countchar.c
TITLE           :   Program to Count the No. of Characters Within a String
                    Using Loops.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program counts the number of characters in a string
                    using the string library functions. It uses while loops to
                    count the number of characters in a given string.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main(void)
{
    char str1[] = "To be or not to be";
    char str2[] = ", that is the question";
    unsigned int count = 0;

    while(str1[count] != '\0')
        ++count;

    printf("String 1 : %s\n", str1);
    printf("The length of string 1 is : %u\n\n", count);

    count = 0;

    while(str2[count] != '\0')
        ++count;

    printf("String 2 : %s\n", str2);
    printf("The length of string 2 is : %u\n\n", count);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

String 1 : To be or not to be
The length of string 1 is : 18

String 2 : , that is the question
The length of string 2 is : 22


Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
