#!/bin/bash


# Add application sources to repository and install applications/packages required for the environment

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
apt-get update -y
apt-add-repository ppa:git-core/ppa -y
apt-get update -y
add-apt-repository ppa:mystic-mirage/pycharm -y
apt-get update -y
add-apt-repository ppa:webupd8team/sublime-text-3 -y
apt-get update -y
apt-get install git -y
apt-get install git-core -y
apt-get install git-sh -y
apt-get install google-chrome-stable -y
apt-get install synaptic -y
apt-get install samba -y
apt-get install putty -y
apt-get install minicom -y
apt-get install filezilla -y
apt-get install python-pip -y
pip install -U pip
apt-get install python-tksnack -y
apt-get install setserial -y
apt-get install build-essential -y
apt-get install sox -y
apt-get install mediainfo -y
apt-get install smbclient -y
apt-get install audacity -y
DEBIAN_FRONTEND=noninteractive apt-get install wireshark -y
apt-get install ckermit -y
apt-get install tftp -y
apt-get install tftpd-hpa -y
apt-get install pycharm-community -y
apt-get install sublime-text-installer -y
apt-get install xclip

# Installation of external python libraries

pip install scipy
pip install matplotlib
pip install pyserial
pip install pyparallel
pip install openpyxl
pip install selenium
pip install memory-profiler

# Configure and start the tftp, samba servers

mkdir /home/$(whoami)/DVF_Images
chmod 777 /home/$(whoami)/DVF_Images
sed -i '/TFTP_DIRECTORY/c\TFTP_DIRECTORY="/home/'$(whoami)'/DVF_Images"' /etc/default/tftpd-hpa
service tftpd-hpa restart
#(echo "$pass"; echo "$pass") | smbpasswd -s -a
mkdir /home/$(whoami)/Shared
chmod 777 /home/$(whoami)/Shared
cp /etc/samba/smb.conf /etc/samba/smb_bak.conf
chmod 777 /etc/samba/smb_bak.conf
echo "[Shared]" >> /etc/samba/smb.conf
echo "path = /home/$(whoami)/Shared" >> /etc/samba/smb.conf
echo "valid users = $(whoami)" >> /etc/samba/smb.conf
echo "read only = no" >> /etc/samba/smb.conf
service smbd restart

# Write udev rules for serial port assignments

#echo "SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="A6008isP", SYMLINK+="arduino"" >> /etc/udev/rules.d/99-usb-serial.rules

# Adding user to dialout, wireshark groups

chown $(whoami) /dev/ttyUSB*
gpasswd -a $(whoami) dialout
adduser $(whoami) wireshark

# Configure git and clone, fetch the latest code, configuration files from repo

git init /home/$(whoami)/
ssh-keygen -t rsa #asks for filename, passphrase and passphrase confirmation after this command
xclip -sel clip < ~/.ssh/id_rsa.pub
echo "Now, login to your Gitlab profile and add the above generated SSH key (copied to clipboard) to the profile...."
read -p "After adding SSH key to profile, Press enter key to continue"
git clone git@gitlab.dspg.com:cordless/automation.git --branch 

# Allowing user to execute without password

cp /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Configure/userConfig /etc/sudoers.d/userConfig
chmod 777 /etc/sudoers.d/userConfig
sed -i '/NOPASSWD:ALL/c\'$(whoami)' ALL=(ALL) NOPASSWD:ALL' /etc/sudoers.d/userConfig
chmod 0440 /etc/sudoers.d/userConfig

# Write path to desktop bookmarks file to create bookmarks

echo "file:///home/$(whoami)/automation/voip_automation/DVF99_DVF101" >> /home/$(whoami)/.config/gtk-3.0/bookmarks
echo "file:///home/$(whoami)/automation/voip_automation/DVF99_DVF101/Input DA_Input" >> /home/$(whoami)/.config/gtk-3.0/bookmarks
echo "file:///home/$(whoami)/automation/voip_automation/DVF99_DVF101/Output DA_Output" >> /home/$(whoami)/.config/gtk-3.0/bookmarks
echo "file:///home/$(whoami)/automation/voip_automation/DVF99_DVF101/Scripts DA_Scripts" >> /home/$(whoami)/.config/gtk-3.0/bookmarks

# Change permissions of executable files

chmod 777 /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Scripts/env_init.sh
chmod 777 /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Scripts/login_init
chmod 777 /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Scripts/DAT_run.sh

# Copy PC custom configuration files (ALSA, minicom, userconfig, nautilus)

cp /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Configure/.asoundrc /home/$(whoami)/.asoundrc
chmod 777 /home/$(whoami)/.asoundrc
cp /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Configure/.minirc.dfl /home/$(whoami)/.minirc.dfl
chmod 777 /home/$(whoami)/.minirc.dfl
cp /home/$(whoami)/automation/voip_automation/DVF99_DVF101/Configure/.kermrc /home/$(whoami)/.kermrc
chmod 777 /home/$(whoami)/.kermrc
