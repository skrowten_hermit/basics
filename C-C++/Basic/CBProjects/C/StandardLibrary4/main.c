/*******************************************************************************
PROGRAM NAME    :   A068-stanlib-mathfuncs.c
TITLE           :
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main()
{
    /* To Use :
    acosh() asin() atan() ceil() cos() floor() log() nan() pow() remainder() round()
    sin() sqrt() tan()
    */
    printf("Hello world!\n");
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
