/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Example of Multi-Level Inheritance using Employee (types)
                    Classes PartTime and FullTime.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate multi-level inheritance
                    using the two employee types in a company - part-time and
                    full-time.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


class Employee
{
    private:
        int eid;
        string name;

    public:
        Employee(int e, string n)
        {
            eid = e;
            name = n;
        }

        int getEmployeeID(){return eid;}

        string getName(){return name;}
};


class FullTimeEmployee:public Employee
{
    private:
        int salary;

    public:
        FullTimeEmployee(int e, string n, int sal):Employee(e, n)
        {
            salary = sal;
        }

        int getSalary(){return salary;}
};


class PartTimeEmployee: public Employee
{
    private:
        int wage;

    public:
        PartTimeEmployee(int e, string n, int w):Employee(e, n)
        {
            wage = w;
        }

        int getWage(){return wage;}
};


int main()
{
    PartTimeEmployee p1(1, "John", 300);
    FullTimeEmployee p2(2, "Raj", 5000);

    cout << "Salary of " << p2.getName() << " is " << p2.getSalary() << endl;
    cout << "Daily wage of " << p1.getName() << " is " << p1.getWage() << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
