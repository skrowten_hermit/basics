/*******************************************************************************
PROGRAM NAME    :   A001-helloworld.c
TITLE           :   Hello World Program.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a basic "Hello World" program as an introduction to
                    C Language.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

int main()
{
    printf("Hello world!\n");
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Hello world!

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
