/*******************************************************************************
PROGRAM NAME    :   A035-weather-analysis.c
TITLE           :   Program to Analyse Average Rainfall in a Given Period.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program takes rainfall data over a period of 5 years
                    and calculates the average monthly rainfall recorded in
                    inches. The data is formatted in a tabular style.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define MONTHS 12
#define YEARS 5


int main()
{
    // Initialize rainfall data for 2011-2015.

    float rain[YEARS][MONTHS] = {
                                    {4.3, 4.3, 4.3, 3.0, 2.0, 1.2, 0.2, 0.2, 0.4, 2.4, 3.5, 6.6},
                                    {8.5, 8.2, 1.2, 1.6, 2.4, 0.0, 5.2, 0.9, 0.3, 0.9, 1.4, 7.3},
                                    {9.1, 8.5, 6.7, 4.3, 2.1, 0.8, 0.2, 0.2, 1.1, 2.3, 6.1, 8.4},
                                    {7.2, 9.9, 8.4, 3.3, 1.2, 0.8, 0.4, 0.0, 0.6, 1.7, 4.3, 6.2},
                                    {7.6, 5.6, 3.8, 2.8, 3.8, 0.2, 0.0, 0.0, 0.0, 1.3, 2.6, 5.2}
                                };

    int year, month;
    float subtotal, total;

    printf(" YEAR\t\tRAINFALL (in inches)\n");

    for (year = 0, total = 0; year < YEARS; year++)
    {
        for (month = 0, subtotal = 0; month < MONTHS; month++)
        {
            subtotal += rain[year][month];
        }
        printf("%5d\t%15.1f\n", 2010 + year, subtotal);
        total += subtotal;
    }

    printf("\nThe yearly average is %.1f inches.\n\n", total / YEARS);

    printf("MONTHLY AVERAGES (in inches):\n\n");
    printf(" Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct  Nov  Dec\n");

    for (month = 0; month < MONTHS; month++)
    {
        for (year = 0, subtotal = 0; year < YEARS; year++)
            subtotal += rain[year][month];
        printf(" %3.1f ", subtotal / YEARS);
    }

    printf("\n");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

 YEAR		RAINFALL (in inches)
 2010	           32.4
 2011	           37.9
 2012	           49.8
 2013	           44.0
 2014	           32.9

The yearly average is 39.4 inches.

MONTHLY AVERAGES (in inches):

 Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct  Nov  Dec
 7.3  7.3  4.9  3.0  2.3  0.6  1.2  0.3  0.5  1.7  3.6  6.7

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
