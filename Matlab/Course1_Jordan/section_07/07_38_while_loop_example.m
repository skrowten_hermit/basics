% While loop example

max_value = 10;
value = 0;

while value <= max_value
    disp(value);
    value = value + 1;
end

max_value