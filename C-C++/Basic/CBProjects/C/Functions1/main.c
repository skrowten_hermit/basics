/*******************************************************************************
PROGRAM NAME    :   A030-func-multiply-nret.c
TITLE           :   Program to Multiply 2 Nos. Using A Non-Returning Function
                    multiplyTwoNums().
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program defines a function multiplyTwoNums() which can
                    multiply 2 numbers and prints the result.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


void multiplyTwoNums(int x, int y)
{
    int result = x * y;
    printf("The product of %d and %d is : %d\n\n", x, y, result);
}


int main(void)
{
    multiplyTwoNums(10, 20);
    multiplyTwoNums(20, 30);
    multiplyTwoNums(50, -2);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

The product of 10 and 20 is : 200

The product of 20 and 30 is : 600

The product of 50 and -2 is : -100


Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
