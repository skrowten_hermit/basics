/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program for Sum of Digits of a Number.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to calculate the sum of all the digits in
                    a given number.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int n, r, sum = 0;

    cout << "Enter a number : ";
    cin >> n;

    while(n != 0)
    {
        r = n % 10;
        n = n / 10;
        sum = sum + r;
    }

    cout << sum;
    cout << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
