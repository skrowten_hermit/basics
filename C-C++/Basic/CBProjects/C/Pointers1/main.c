/*******************************************************************************
PROGRAM NAME    :   A050-pointers-operations.c
TITLE           :   Program to Demonstrate Pointer Operations and their Effects.
DATE            :   12/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates various pointer operations using
                    addresses, values etc and shows the outputs.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    // 'num' is an alias for the memory address location where the value 99 is
    // stored.
    int num = 99, *ptr = &num, pum;
    // '&num' is the address where 99 is stored. The values of 'pnum' and
    // '&num' are always the same - the memory location.
    int *pnum = &num, *ppum = NULL;
    // The values of 'pnum' and '&num' are always the same - the memory
    // location.
    // The values of '*pnum' and 'num' are always the same - the value at the
    // memory loacation
    pum = *pnum;

    printf("Value (num) : %d\n", num);
    printf("Address where %d is stored (&num) : %d\n\n", num, &num);
    printf("Value (pum) : %d\n", pum);
    printf("Address where %d is stored (&pum) : %d\n\n", pum, &pum);

    printf("Reference to %d (pnum) : %d\n", num, pnum);
    printf("Reference to %d (pnum) as pointer type : %p\n", num, pnum);
    printf("Pointer to %d (*pnum) : %d\n", num, *pnum);
    printf("Address of the pointer (pnum) : %d\n", &pnum);
    printf("Address of the pointer (pnum) as pointer type : %p\n\n",
            (void *) &pnum);
    // The following is the same as above.
//    printf("Address of the pointer (pnum) as pointer type : %p\n", &pnum);
    printf("Reference to %d (ptr) : %d\n", num, ptr);
    printf("Reference to %d (ptr) as pointer type : %p\n", num, ptr);
    printf("Pointer to %d (*ptr) : %d\n", num, *ptr);
    printf("Address of the pointer (ptr) : %d\n", &ptr);
    printf("Address of the pointer (ptr) as pointer type : %p\n\n",
            (void *) &ptr);
    // The following is the same as above.
//    printf("Address of the pointer (ptr) as pointer type : %p\n\n", &ptr);
    // The following throws segmentation fault.
//    printf("Pointer value *ppum : %d\n", *ppum);
    // The following gives a result '0' (refers to NULL as initialized above).
    printf("Reference to ppum : %d\n\n", ppum);
    printf("Sizes of :\n\tintegers --> num = %d, pum = %d\
            \n\tpointers --> pnum = %d, ptr = %d, ppum = %d\n", sizeof(num),
            sizeof(pum), sizeof(pnum), sizeof(ptr), sizeof(ppum));

    printf("\n************************************************\n\n");

    ppum = &num;

    num = 100;          // Change at original variable.
    printf("Value (num) : %d\n", num);
    printf("Address where %d is stored (&num) : %d\n(Shouldn't change)\n\n",
            num, &num);
    printf("Value (pum) : %d\n", pum);
    printf("Address where %d is stored (&pum) : %d\n(Shouldn't change)\n\n",
            pum, &pum);

    printf("Reference to %d (pnum) : %d\n", num, pnum);
    printf("Reference to %d (pnum) as pointer type : %p\n", num, pnum);
    printf("Pointer to %d (*pnum) : %d\n", num, *pnum);
    printf("Address of the pointer (pnum) : %d\n", &pnum);
    printf("Address of the pointer (pnum) as pointer type : %p\n\n",
            (void *) &pnum);
    // The following is the same as above.
//    printf("Address of the pointer (pnum) as pointer type : %p\n\n", &pnum);
    printf("Reference to %d (ptr) : %d\n", num, ptr);
    printf("Reference to %d (ptr) as pointer type : %p\n", num, ptr);
    printf("Pointer to %d (*ptr) : %d\n", num, *ptr);
    printf("Address of the pointer (ptr) : %d\n", &ptr);
    printf("Address of the pointer (ptr) as pointer type : %p\n\n",
            (void *) &ptr);
    // The following is the same as above.
//    printf("Address of the pointer (ptr) as pointer type : %p\n\n", &ptr);

    // The following throws segmentation fault.
    printf("Pointer value *ppum : %d\n", *ppum);
    // The following gives a result '0' (refers to NULL as initialized above).
    printf("Reference to ppum : %d\n\n", ppum);
    printf("Sizes of :\n\tintegers --> num = %d, pum = %d\
            \n\tpointers --> pnum = %d, ptr = %d, ppum = %d\n", sizeof(num),
            sizeof(pum), sizeof(pnum), sizeof(ptr), sizeof(ppum));

    printf("\n************************************************\n\n");

    *pnum = 101;        // Change at original variable via pointer.
    printf("Value (num) : %d\n", num);
    printf("Address where %d is stored (&num) : %d\n(Shouldn't change)\n\n",
            num, &num);
    printf("Value (pum) : %d\n", pum);
    printf("Address where %d is stored (&pum) : %d\n(Shouldn't change)\n\n",
            pum, &pum);

    printf("Reference to %d (pnum) : %d\n", num, pnum);
    printf("Reference to %d (pnum) as pointer type : %p\n", num, pnum);
    printf("Pointer to %d (*pnum) : %d\n", num, *pnum);
    printf("Address of the pointer (pnum) : %d\n", &pnum);
    printf("Address of the pointer (pnum) as pointer type : %p\n\n",
            (void *) &pnum);
    // The following is the same as above.
//    printf("Address of the pointer (pnum) as pointer type : %p\n\n", &pnum);
    printf("Reference to %d (ptr) : %d\n", num, ptr);
    printf("Reference to %d (ptr) as pointer type : %p\n", num, ptr);
    printf("Pointer to %d (*ptr) : %d\n", num, *ptr);
    printf("Address of the pointer (ptr) : %d\n", &ptr);
    printf("Address of the pointer (ptr) as pointer type : %p\n\n",
            (void *) &ptr);
    // The following is the same as above.
//    printf("Address of the pointer (ptr) as pointer type : %p\n\n", &ptr);

    printf("Pointer value *ppum : %d\n", *ppum);  // Throws segmentation fault.
    // The following gives a result '0' (refers to NULL as initialized above).
    printf("Reference to ppum : %d\n\n", ppum);
    printf("Sizes of :\n\tintegers --> num = %d, pum = %d\
            \n\tpointers --> pnum = %d, ptr = %d, ppum = %d\n", sizeof(num),
            sizeof(pum), sizeof(pnum), sizeof(ptr), sizeof(ppum));

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Value (num) : 99
Address where 99 is stored (&num) : 350061912

Value (pum) : 99
Address where 99 is stored (&pum) : 350061916

Reference to 99 (pnum) : 350061912
Reference to 99 (pnum) as pointer type : 0x7fff14dd8558
Pointer to 99 (*pnum) : 99
Address of the pointer (pnum) : 350061928
Address of the pointer (pnum) as pointer type : 0x7fff14dd8568

Reference to 99 (ptr) : 350061912
Reference to 99 (ptr) as pointer type : 0x7fff14dd8558
Pointer to 99 (*ptr) : 99
Address of the pointer (ptr) : 350061920
Address of the pointer (ptr) as pointer type : 0x7fff14dd8560

Reference to ppum : 0

Sizes of :
	integers --> num = 4, pum = 4
	pointers --> pnum = 8, ptr = 8, ppum = 8

************************************************

Value (num) : 100
Address where 100 is stored (&num) : 350061912
(Shouldn't change)

Value (pum) : 99
Address where 99 is stored (&pum) : 350061916
(Shouldn't change)

Reference to 100 (pnum) : 350061912
Reference to 100 (pnum) as pointer type : 0x7fff14dd8558
Pointer to 100 (*pnum) : 100
Address of the pointer (pnum) : 350061928
Address of the pointer (pnum) as pointer type : 0x7fff14dd8568

Reference to 100 (ptr) : 350061912
Reference to 100 (ptr) as pointer type : 0x7fff14dd8558
Pointer to 100 (*ptr) : 100
Address of the pointer (ptr) : 350061920
Address of the pointer (ptr) as pointer type : 0x7fff14dd8560

Pointer value *ppum : 100
Reference to ppum : 350061912

Sizes of :
	integers --> num = 4, pum = 4
	pointers --> pnum = 8, ptr = 8, ppum = 8

************************************************

Value (num) : 101
Address where 101 is stored (&num) : 350061912
(Shouldn't change)

Value (pum) : 99
Address where 99 is stored (&pum) : 350061916
(Shouldn't change)

Reference to 101 (pnum) : 350061912
Reference to 101 (pnum) as pointer type : 0x7fff14dd8558
Pointer to 101 (*pnum) : 101
Address of the pointer (pnum) : 350061928
Address of the pointer (pnum) as pointer type : 0x7fff14dd8568

Reference to 101 (ptr) : 350061912
Reference to 101 (ptr) as pointer type : 0x7fff14dd8558
Pointer to 101 (*ptr) : 101
Address of the pointer (ptr) : 350061920
Address of the pointer (ptr) as pointer type : 0x7fff14dd8560

Pointer value *ppum : 101
Reference to ppum : 350061912

Sizes of :
	integers --> num = 4, pum = 4
	pointers --> pnum = 8, ptr = 8, ppum = 8

Process returned 0 (0x0)   execution time : 0.003 s
Press ENTER to continue.

*******************************************************************************/
