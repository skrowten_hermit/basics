/*
 * Small test utility for rtp-kernel-test and voip-control.
 *
 * This tool is meant to run on the host side. It echos every package seen from
 * the target.
 *
 * Compile with:
 *
 * 	$ gcc udp_reply.c -o udp_reply
 *
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#define BUFLEN 1292
#define PORT 5000

//For ipv6 calls
//#define IPV6_SUPPORT 1

char *cmd;
int port = PORT;

void usage(void)
{
	printf("%s [-h] [-p <port>]\n"
	       "\n"
	       "Options:\n"
	       "  -h         show this help\n"
	       "  -p <port>  bind to <port>number instead of %d\n",
	       cmd,
	       PORT);
	exit(EXIT_FAILURE);
}

void parse_cmdline(int argc, char *argv[])
{
	char *options = "hp:";
	int optchar;

	cmd = argv[0];

	while ((optchar = getopt(argc, argv, options)) != -1) {
		switch (optchar) {
		case 'h':
			usage();
			break;
		case 'p':
			port = atoi(optarg);
			break;
		default:
			usage();
		}
	}
}

void diep(char *s)
{
	perror(s);
	exit(1);
}

int main(int argc, char *argv[])
{
	
#ifdef IPV6_SUPPORT	
	struct sockaddr_in6 si_me, si_other;
#else
	struct sockaddr_in si_me, si_other;
#endif 


	int s, i, slen=sizeof(si_other);
	char buf[BUFLEN];
	int len;

	parse_cmdline(argc, argv);


#ifdef IPV6_SUPPORT	

	if ((s=socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP))==-1)
		diep("socket");

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin6_family = AF_INET6;
	si_me.sin6_port = htons(port);
	//si_me.sin6_addr.s6_addr = htonl(INADDR_ANY);in6addr_any
	//si_me.sin6_addr.s6_addr = in6addr_any;
	memset(si_me.sin6_addr.s6_addr,0,16);

#else
	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
		diep("socket");

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(port);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);

#endif


	if (bind(s, (struct sockaddr *)&si_me, sizeof(si_me))==-1)
		diep("bind");

	for (;;) {
		len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *)&si_other,
		               &slen);
		
		if(len < 0)
			diep("recvfrom()");

		printf(".");
		fflush(stdout);

		len = sendto(s, buf, len, 0, (struct sockaddr *)&si_other,
		             slen);
		if(len < 0)
			diep("sendto()");
	}

	close(s);
	return 0;
}
