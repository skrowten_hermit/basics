/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Example of Function Template.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate function templates

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


template <class T>
T maxim(T a,T b)
{
    return a > b ? a : b;
}


int main()
{
    cout << maxim(12,14) << endl;
    cout << maxim(2.3,1.4) << endl;
    cout << maxim(2.3f,5.6f) << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
