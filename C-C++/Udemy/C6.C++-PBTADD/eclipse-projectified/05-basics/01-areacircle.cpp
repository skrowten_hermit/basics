/*
 * 01-areacircle.cpp
 * Program to Calculate Area of Circle
 *
 *  Created on: 01-Apr-2020
 *      Author: sreekanth.s
 */


#include<iostream>

using namespace std;

int main()
{
    float radius;
    float area;
    cout<<"Enter Radius of a Circle";
    cin>>radius;
    area=3.1425*radius*radius;
    cout<<"Area of a Circle is "<<area<<endl;
}
