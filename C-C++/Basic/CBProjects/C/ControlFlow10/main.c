/*******************************************************************************
PROGRAM NAME    :   A022-while-loops-enum+nested-inputs.c
TITLE           :   Program to Demonstrate 'enum' and nested loops.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates nested loops and exit conditions
                    on nested loops.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    enum Weekday {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday};
    int p = 0, q = 0;

    printf("Enter a number p :\n");
    scanf("%d", &p);
    printf("\n");

    for (enum Weekday day = Monday; day <= Sunday; ++day)
    {
        if (day == Wednesday)
        {
            continue;
        }
        printf("(Day = %d) It's Not Wednesday!\n", day);
    }

    while (p > 0)
    {
        printf("\nUpdated value of p : %d\n\n", p);
        printf("Enter another number q (<=100, 0 to skip) :\n");
        scanf("%d", &q);

        while (q > 0)
        {
            printf("%d * %d = %d\n", p, q, p * q);
            if (q > 100)
                printf("\nLimit exceeded (>100)!\n");
                break;              // Break from inner loop.
            scanf("%d", &q);
        }

        if (q > 100)
            break;                  // Break from outer loop.

        printf("\nEnter number p again (0 to stop) :\n");
        scanf("%d", &p);
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter a number p :
5

(Day = 0) It's Not Wednesday!
(Day = 1) It's Not Wednesday!
(Day = 3) It's Not Wednesday!
(Day = 4) It's Not Wednesday!
(Day = 5) It's Not Wednesday!
(Day = 6) It's Not Wednesday!

Updated value of p : 5

Enter another number q (<=100, 0 to skip) :
2
5 * 2 = 10

Enter number p again (0 to stop) :
8

Updated value of p : 8

Enter another number q (<=100, 0 to skip) :
0

Enter number p again (0 to stop) :
8

Updated value of p : 8

Enter another number q (<=100, 0 to skip) :
19
8 * 19 = 152

Enter number p again (0 to stop) :
100

Updated value of p : 100

Enter another number q (<=100, 0 to skip) :
8
100 * 8 = 800

Enter number p again (0 to stop) :
7

Updated value of p : 7

Enter another number q (<=100, 0 to skip) :
2157
7 * 2157 = 15099

Limit exceeded (>100)!

Process returned 0 (0x0)   execution time : 165.177 s
Press ENTER to continue.

*******************************************************************************/
