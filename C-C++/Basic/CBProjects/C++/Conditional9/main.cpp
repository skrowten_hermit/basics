/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate switch case.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to demonstrate a simple "switch
                    case" statement which prints the day for the day number
                    entered by the user.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


int main()
{
    int day;

    cout << "Enter a day no. :" << endl;
    cin >> day;

    switch(day)
    {
        case 1 :
                cout << "Monday" << endl;
                break;
        case 2 :
                cout << "Tuesday" << endl;
                break;
        case 3 :
                cout << "Wednesday" << endl;
                break;
        case 4 :
                cout << "Thursday" << endl;
                break;
        case 5 :
                cout << "Friday" << endl;
                break;
        case 6 :
                cout << "Saturday" << endl;
                break;
        case 7 :
                cout << "Sunday" << endl;
                break;
        default :
                cout << "Invalid day no. entered!" << endl;
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
