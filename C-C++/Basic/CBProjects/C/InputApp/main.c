/*******************************************************************************
PROGRAM NAME    :   A005-user-input.c
TITLE           :   Program to Demonstrate User Inputs.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program accepts user input using scanf() function and
                    displays it back to the user.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    char str[100];
    int i;
    double x;

    printf("Enter The Values (integer, string, and float) : \n");

//    scanf("%d", &i);
    scanf("%d %s %lf", &i, str, &x);

//    printf("\nYou Entered : %d\n", i);
    printf("\nYou Entered :\n\"%d\"\n\"%s\"\n\"%lf\"\n", i, str, x);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter The Values (integer, string, and float) :
92
Sree
14.0856496857344214832

You Entered :
"92"
"Sree"
"14.085650"

Process returned 0 (0x0)   execution time : 38.921 s
Press ENTER to continue.

*******************************************************************************/
