#!/bin/bash

echo "This is another script...."

f()
{
    printf 'I am called with %d arguments: %s\n' "$#" "$*";
#    cd ../source
#    python test.py "$*"
    ./a_script.sh "${c[@]}"
}

arg=$1
#param='a-e,f/b-1/c-5,g/d'

IFS='/' read -ra test_list <<<"$arg"
for i in "${test_list[@]}"; do
    printf "This is $i loop entry....\n"
    IFS='-' read -r _ b <<<"$i"
    IFS='-' read -r t _ <<<"$i"
    printf "intermediate : $b\n"
    printf "type : $t\n"
    IFS=',' read -ra c <<<"$b"
    printf "final : $c \n"
    f "${c[@]}"
done