/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Example of Default Arguments.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate defining default  arguments
                    in the function declaration.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int sum(int a, int b, int c = 0)
{
    return a + b + c;
}


int main()
{
    cout << "sum(10, 20, 3) = " << sum(10, 20, 3) << endl;
    cout << "sum(10, 20) = " << sum(10, 20) << endl;
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
