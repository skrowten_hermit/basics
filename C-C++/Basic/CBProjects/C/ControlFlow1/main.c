/*******************************************************************************
PROGRAM NAME    :   A013-odd-even.c
TITLE           :   Program to Check if a Number is Odd or Even.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program takes a number from the user as input and
                    checks/determines if it is an odd number or an even number.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int num, remainder;

    printf("Enter The Number To Be Tested :\n");
    scanf("%i", &num);

    remainder = num % 2;

    if (remainder == 0)
        printf("The Number %d Is Even.\n", num);
    else
        printf("The Number %i Is Odd.\n", num);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Run(1):

Enter The Number To Be Tested :
89
The Number 89 Is Odd.

Process returned 0 (0x0)   execution time : 6.550 s
Press ENTER to continue.


Run(2):

Enter The Number To Be Tested :
-1042
The Number -1042 Is Even.

Process returned 0 (0x0)   execution time : 7.366 s
Press ENTER to continue.

*******************************************************************************/
