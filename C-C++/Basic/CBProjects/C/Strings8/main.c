/*******************************************************************************
PROGRAM NAME    :   A043-strings-srch-uppercsae.c
TITLE           :   Program to Demonstrate Search Using UpperCase Function.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to perform a search in a
                    string without using any string library search functions.
                    This uses toupper() function instead - converts the
                    substring and string to upper case first and then looks for
                    the substring in the string. This makes this search function
                    case-insensitive.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    char buf[] = "hello this is an example text";
    printf("Example string :\n%s\n\n", buf);

//    printf("ToUpper :\n%s\n\n", toupper(buf));    // This doesn't work.

    for(int i = 0; (buf[i] = (char)toupper(buf[i])) != '\0'; ++i);

    printf("Example string converted to all caps :\n%s\n\n\n", buf);

    char text[100];
    char substring[40];

    printf("Enter the string to be searched (less than %d characters) : \n", 100);
    scanf("%s", text);
    printf("Enter the string sought (less than %d characters) : \n", 40);
    scanf("%s", substring);
    printf("\n\nFirst string entered  : %s\n", text);
    printf("Second string entered : %s\n", substring);

    for(int i = 0; (text[i] = (char)toupper(text[i])) != '\0'; ++i);
    for(int i = 0; (substring[i] = (char)toupper(substring[i])) != '\0'; ++i);

    printf("\nThe second string %s FOUND in the first.\n", (strstr(text, substring) == NULL) ? "WAS NOT" : "WAS");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Example string :
hello this is an example text

Example string converted to all caps :
HELLO THIS IS AN EXAMPLE TEXT


Enter the string to be searched (less than 100 characters) :
SREEkanth
Enter the string sought (less than 40 characters) :
sree


First string entered  : SREEkanth
Second string entered : sree

The second string WAS FOUND in the first.

Process returned 0 (0x0)   execution time : 17.863 s
Press ENTER to continue.

*******************************************************************************/
