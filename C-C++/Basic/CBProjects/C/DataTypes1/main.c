/*******************************************************************************
PROGRAM NAME    :   A002-diff-datatypes.c
TITLE           :   Program to Display Different Data Types.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program shows the different datatypes and prints it in
                    a single specified format.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    float x1 = 23.333;
    double x2 = 55.55555555555e+11;
    _Bool x3 = 1;
    bool x4 = true;

    printf("x1 = %20f\n", x1);
    printf("x2 = %20lf\n", x2);
    printf("x3 = %20i\n", x3);
    printf("x4 = %20i\n", x4);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

x1 =            23.333000
x2 = 5555555555555.000000
x3 =                    1
x4 =                    1

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
