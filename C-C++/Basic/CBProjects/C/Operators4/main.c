/*******************************************************************************
PROGRAM NAME    :   A010-assign-operations.c
TITLE           :   Program to Demonstrate Assignment Operator.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates assignment operation in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int x = 0;
    float f = 12.125;

    x = f;
//    f = x;

    printf("x = %d\n", x);
    printf("f = %f\n", f);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

x = 12
f = 12.125000

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
