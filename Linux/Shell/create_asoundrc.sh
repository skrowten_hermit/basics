#!/bin/bash


################################################################################
################################################################################
## File              :: create_asoundrc.sh
## Description       :: Makes and installs a asoundrc file based on hardware
##                      used/required.
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 23/01/2019
## Changes made      :: The script for fetching the latest tag from the Linux
##                      VoIP automation branch. Works as an update script.
## Changes made Date :: 23/01/2019
## Changes made by   :: Sreekanth S
## Execution example :: ./create_asoundrc.sh
################################################################################
################################################################################


echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..22}
echo "VoIP Automation Framework : Create ALSA Configuration"
echo $(printf '=%.0s' {1..100})
echo $'\n'

#Change the working directory, do git commands
cd
#echo $PWD

arg=$1
#echo "Input : $arg"
#echo

SOUNDCARD="$1" CHANNEL_TYPE="$2" IO_PORTS="$3" python - <<'END_SCRIPT'

import os
from lib.asp.audio.asoundrcgen import ALSAconfigGenerator as ACG

soundcard=os.environ['SOUNDCARD']
channeltype=os.environ['CHANNEL_TYPE']
ioports=os.environ['IO_PORTS']

CONFIG = ACG(soundcard, channeltype, ioports, debug=0)
del CONFIG

END_SCRIPT

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..22}
echo "VoIP Automation Framework : Create ALSA Configuration"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0