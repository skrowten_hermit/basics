/*******************************************************************************
PROGRAM NAME    :   A024-net-salary.c
TITLE           :   Program to Calculate the Net Salary from Hours Worked.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program takes as input the number of hours worked in
                    a week and calculates the Net Salary after tax deduction.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define PAYRATE 12.00       // Dollars paid per hour.
#define TAXRATE_300 .15     // Tax % upto first $300.
#define TAXRATE_150 .20     // Tax % upto next $150.
#define TAXRATE_REM .25     // Tax % after $450.
#define OVERTIME 40


int main()
{
    int hrs = 0;
    double grossPay = 0.0;
    double taxes = 0.0;
    double netPay = 0.0;

    // Get the Input.

    printf("Enter the number of hours you've worked this week :\n");
    scanf("%d", &hrs);

    // Calculate the Gross Pay.

    if (hrs <= 40)
        grossPay = hrs * PAYRATE;
    else
    {
        grossPay = 40 * PAYRATE;
        double overtimePay = (hrs - 40) * (PAYRATE * 1.5);
        grossPay += overtimePay;
    }

    // Display the Gross Pay.

    printf("\nYour Gross Pay is : $%.2f\n", grossPay);

    // Calculate the taxes.

    if (grossPay <= 300)
    {
        taxes = grossPay * TAXRATE_300;
    }
    else if (grossPay > 300 && grossPay <= 450)
    {
        taxes = 300 * TAXRATE_300;
        taxes += (grossPay - 300) * TAXRATE_150;
    }
    else if (grossPay > 450)
    {
        taxes = 300 * TAXRATE_300;
        taxes += 150 * TAXRATE_150;
        taxes += (grossPay - 450) * TAXRATE_REM;
    }

    // Display the Taxes.

    printf("\nYour Tax is : $%.2f\n", taxes);

    // Calculate the Net Pay.

    netPay = grossPay - taxes;

    // Display the Net Pay.

    printf("\nYour Net Pay is : $%.2f\n", netPay);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter the number of hours you've worked this week :
42

Your Gross Pay is : $516.00

Your Tax is : $91.50

Your Net Pay is : $424.50

Process returned 0 (0x0)   execution time : 3.069 s
Press ENTER to continue.

*******************************************************************************/
