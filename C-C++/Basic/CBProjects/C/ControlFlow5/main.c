/*******************************************************************************
PROGRAM NAME    :   A017-switch-case-arithmetic.c
TITLE           :   Program to Do Arithmetic Calculations Based on User Input.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program accepts an operation from the user along with
                    2 data values to perform the given operation on them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    float val1, val2;
    char operation;

//    printf("Enter the 2 values you want to operate on followed by the the"
//           "operation (+, -, *, /) you want to perform on the 2 values:\n");
//    scanf("%f %f %c", &val1, &val2, &operation);

    printf("Enter the 2 values you want to operate on :\n");
    scanf("%f %f", &val1, &val2);
    printf("Enter the operation (+, -, *, /) you want to perform on the above "
            "2 values :\n");
    // The space before %c is important in scanf() otherwise the enter is taken
    // as input character!
    scanf(" %c", &operation);

    printf("\n");

    switch(operation)
    {
        case '+':
            printf("%.2f %c %.2f = %.2f\n",val1, operation, val2, val1 + val2);
            break;
        case '-':
            printf("%.2f %c %.2f = %.2f\n",val1, operation, val2, val1 - val2);
            break;
        case '*':
            printf("%.2f %c %.2f = %.2f\n",val1, operation, val2, val1 * val2);
            break;
        case '/':
            if (val2 == 0)
                printf("Division by Zero Encountered!!!\n");
            else
                printf("%f %c %f = %.2f\n",val1, operation, val2, val1 / val2);
            break;
        default:
            printf("Unsupported Operation Entered!!!\n");
            break;
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Run(1):

Enter the 2 values you want to operate on :
8
-7
Enter the operation (+, -, *, /) you want to perform on the above 2 values :
/

8.000000 / -7.000000 = -1.14

Process returned 0 (0x0)   execution time : 9.378 s
Press ENTER to continue.


Run(2):

Enter the 2 values you want to operate on :
978
29
Enter the operation (+, -, *, /) you want to perform on the above 2 values :
*

978.00 * 29.00 = 28362.00

Process returned 0 (0x0)   execution time : 17.888 s
Press ENTER to continue.


Run(3):

Enter the 2 values you want to operate on :
-8943
-99
Enter the operation (+, -, *, /) you want to perform on the above 2 values :
-

-8943.00 - -99.00 = -8844.00

Process returned 0 (0x0)   execution time : 15.209 s
Press ENTER to continue.


Run(4):

Enter the 2 values you want to operate on :
32
0
Enter the operation (+, -, *, /) you want to perform on the above 2 values :
/

Division by Zero Encountered!!!

Process returned 0 (0x0)   execution time : 9.564 s
Press ENTER to continue.


Run(5):

Enter the 2 values you want to operate on :
6
9
Enter the operation (+, -, *, /) you want to perform on the above 2 values :
^

Unsupported Operation Entered!!!

Process returned 0 (0x0)   execution time : 13.987 s
Press ENTER to continue.

*******************************************************************************/
