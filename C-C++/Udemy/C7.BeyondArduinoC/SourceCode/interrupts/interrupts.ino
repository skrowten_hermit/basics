// Comment out the following line for the Polling Implementation
#define INTERRUPT_DRIVEN

#define miliseconds 1000
#define LED1  8
#define LED2  13
#define interruptPin  2

#ifdef INTERRUPT_DRIVEN
  // This is the Interrupt-Driven Implementation
  void setup() {
    pinMode(LED2, OUTPUT);
    pinMode(LED1, OUTPUT);
    pinMode(interruptPin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(interruptPin), myISR, CHANGE);
  }
  
  void loop() {
    digitalWrite(LED1, HIGH);  // turn the LED on 
    delay(miliseconds);        // wait for a second
    digitalWrite(LED1, LOW);   // turn the LED off
    delay(miliseconds);        // wait for a second
  }

  void myISR() {
    digitalWrite(LED2, !digitalRead(interruptPin));  // Update the second LED
  }

#else
  // This is the Polling Implementation
  void setup() {
    pinMode(LED2, OUTPUT);
    pinMode(LED1, OUTPUT);
    pinMode(interruptPin, INPUT_PULLUP);
  }

  void loop() {
    digitalWrite(LED1, HIGH);  // turn the LED on 
    delay(miliseconds);        // wait for a second
    digitalWrite(LED1, LOW);   // turn the LED off
    delay(miliseconds);        // wait for a second 
    digitalWrite(LED2, !digitalRead(interruptPin));  // Update the second LED
  }
#endif

