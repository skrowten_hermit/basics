/*******************************************************************************
PROGRAM NAME    :   A057-pointers-ptrstr.c
TITLE           :
DATE            :   19/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main(void)
{
    char multiple1[] = "a string";
    char *p1 = multiple1;

    long multiple2[] = {15L, 25L, 35L, 45L};
    long *p2 = multiple2;

    printf("Character array (multiple1) details:\n\n");

    for(int i = 0; i < strlen(multiple1); i++)
        printf("multiple1[%d] = %c,\t*(p1 + %d) = %c,\t&multiple1[%d] = %p,"
                "\tp1 + %d = %p\n", i, multiple1[i], i, *(p1 + i),
                i, &multiple1[i], i, p1 + i);

    printf("\nCharacter pointer (*p1) walkthrough:\n\n");

    for(; *p1 != '\0'; p1++)
        printf("*p1 = %c,\tp1 = %p\n",*p1, p1);

    printf("\nCharacter pointer (*p1) walkback:\n\n");

    do
    {
        p1--;
        printf("*p1 = %c,\tp1 = %p\n",*p1, p1);
    }while(*p1 != multiple1[0]);

    printf("\n\n\t\t\t\tType char occupies : %d bytes\n", (int)sizeof(char));

    printf("\n\nLong array (multiple1) details:\n\n");

    for(int i = 0; i < sizeof(multiple2)/sizeof(multiple2[0]); i++)
        printf("multiple2[%d] = %ld,\t*(p2 + %d) = %ld,\t&multiple2[%d] = %d,"
                "\tp2 + %d = %d\n", i, multiple2[i], i, *(p2 + i),
                i, &multiple2[i], i, p2 + i);

    printf("\n\n\t\t\t\tType long occupies : %d bytes\n\n", (int)sizeof(long));

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

multiple1[3] = t,	*(p1 + 3) = t,
&multiple1[3] = 0x7ffcb93d4e92,	p1 + 3 = 0x7ffcb93d4e92
multiple1[4] = r,	*(p1 + 4) = r,
&multiple1[4] = 0x7ffcb93d4e93,	p1 + 4 = 0x7ffcb93d4e93
multiple1[5] = i,	*(p1 + 5) = i,
&multiple1[5] = 0x7ffcb93d4e94,	p1 + 5 = 0x7ffcb93d4e94
multiple1[6] = n,	*(p1 + 6) = n,
&multiple1[6] = 0x7ffcb93d4e95,	p1 + 6 = 0x7ffcb93d4e95
multiple1[7] = g,	*(p1 + 7) = g,
&multiple1[7] = 0x7ffcb93d4e96,	p1 + 7 = 0x7ffcb93d4e96

Character pointer (*p1) walkthrough:

*p1 = a,	p1 = 0x7ffcb93d4e8f
*p1 =  ,	p1 = 0x7ffcb93d4e90
*p1 = s,	p1 = 0x7ffcb93d4e91
*p1 = t,	p1 = 0x7ffcb93d4e92
*p1 = r,	p1 = 0x7ffcb93d4e93
*p1 = i,	p1 = 0x7ffcb93d4e94
*p1 = n,	p1 = 0x7ffcb93d4e95
*p1 = g,	p1 = 0x7ffcb93d4e96

Character pointer (*p1) walkback:

*p1 = g,	p1 = 0x7ffcb93d4e96
*p1 = n,	p1 = 0x7ffcb93d4e95
*p1 = i,	p1 = 0x7ffcb93d4e94
*p1 = r,	p1 = 0x7ffcb93d4e93
*p1 = t,	p1 = 0x7ffcb93d4e92
*p1 = s,	p1 = 0x7ffcb93d4e91
*p1 =  ,	p1 = 0x7ffcb93d4e90
*p1 = a,	p1 = 0x7ffcb93d4e8f


		Type char occupies : 1 bytes


Long array (multiple1) details:

multiple2[0] = 15,	*(p2 + 0) = 15,
&multiple2[0] = -1187164576,	p2 + 0 = -1187164576
multiple2[1] = 25,	*(p2 + 1) = 25,
&multiple2[1] = -1187164568,	p2 + 1 = -1187164568
multiple2[2] = 35,	*(p2 + 2) = 35,
&multiple2[2] = -1187164560,	p2 + 2 = -1187164560
multiple2[3] = 45,	*(p2 + 3) = 45,
&multiple2[3] = -1187164552,	p2 + 3 = -1187164552


		Type long occupies : 8 bytes


Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
