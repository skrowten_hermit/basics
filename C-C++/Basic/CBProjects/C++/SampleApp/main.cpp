/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Demonstration of Multiple File Project.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program illustrates how to create functions in
                    different source files and how to use them in the "main.cpp".
                    The function add() is defined in "add.cpp" and sayhello() is
                    defined in "sayhello.cpp".
*******************************************************************************/



#include <iostream>

using namespace std;


void add(void);

void sayhello(void);


int main(void)
{
    sayhello();
    add();

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
