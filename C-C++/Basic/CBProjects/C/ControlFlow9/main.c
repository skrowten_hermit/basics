/*******************************************************************************
PROGRAM NAME    :   A021-nested-loops-demo.c
TITLE           :   Program to Demonstrate Nested Loops.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstates nested loops by calculating sum of
                    loop control variables.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int count = 0, sum = 0, j = 0;

    printf("Enter a limit :\n");
    scanf("%d", &count);

    printf("\nPrinting 1st loop....\n");

    for(int i = 1; i <= count; ++i)
    {
        sum = 0;

        // Calculate sum of integers from 1 to i.

        for (int j = 0; j <= i; ++j)
        {
            sum += j;

            // Output sum of 1 to i.
            printf("i = %d\tj = %d\tsum(j) till now = %d\n", i, j, sum);
        }

        printf("\n");
    }

    printf("Printing 2nd loop....\n");

    for(int i = 1; i <= count; ++i)
    {
        sum = 1;
        j = 1;

        printf("1");

        // Calculate sum of integers from 1 to i.

        while (j < i)
        {
            sum += ++j;
            printf(" + %d", j);
        }

        printf(" = %d\n", sum);
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter a limit :
5

Printing 1st loop....
i = 1	j = 0	sum(j) till now = 0
i = 1	j = 1	sum(j) till now = 1

i = 2	j = 0	sum(j) till now = 0
i = 2	j = 1	sum(j) till now = 1
i = 2	j = 2	sum(j) till now = 3

i = 3	j = 0	sum(j) till now = 0
i = 3	j = 1	sum(j) till now = 1
i = 3	j = 2	sum(j) till now = 3
i = 3	j = 3	sum(j) till now = 6

i = 4	j = 0	sum(j) till now = 0
i = 4	j = 1	sum(j) till now = 1
i = 4	j = 2	sum(j) till now = 3
i = 4	j = 3	sum(j) till now = 6
i = 4	j = 4	sum(j) till now = 10

i = 5	j = 0	sum(j) till now = 0
i = 5	j = 1	sum(j) till now = 1
i = 5	j = 2	sum(j) till now = 3
i = 5	j = 3	sum(j) till now = 6
i = 5	j = 4	sum(j) till now = 10
i = 5	j = 5	sum(j) till now = 15

Printing 2nd loop....
1 = 1
1 + 2 = 3
1 + 2 + 3 = 6
1 + 2 + 3 + 4 = 10
1 + 2 + 3 + 4 + 5 = 15

Process returned 0 (0x0)   execution time : 1.513 s
Press ENTER to continue.

*******************************************************************************/
