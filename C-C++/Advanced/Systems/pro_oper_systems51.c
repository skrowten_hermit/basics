#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
int main()
{
pid_t pid;
pid=fork();
if(pid==-1)
{
perror("fork failure\n");
}
else if(pid==0)
{
printf("pid in child=%d  and  parentid=%d \n",getpid(),getppid());
}
else
{
printf("pid in parent=%d  and  childid=%d \n",getpid(),pid);
}
exit(0);
//exit(EXIT_SUCCESSFUL);
}
