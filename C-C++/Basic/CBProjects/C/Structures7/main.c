/*******************************************************************************
PROGRAM NAME    :   A071-struct-structargs.c
TITLE           :   Program to Demonstrate a Structure Passed as an Argument
                    in a Function.
DATE            :   13/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to pass a structure as an
                    argument to a function using the "pass-by-value" technique
                    and use the values.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define FUNDLEN 50


struct funds
{
    char bank[FUNDLEN];
    double bankfund;
    char save[FUNDLEN];
    double savefund;
};


double sum(struct funds money);


int main()
{
    struct funds stan = {"Garlic-Melon Bank", 4032.27,
                        "Lucky's Savings and Loan", 8543.94};

    printf("Stan has a total of $%.2f.\n", sum(stan));

    return 0;
}


double sum(struct funds moolah)
{
    return(moolah.bankfund + moolah.savefund);
}



/*******************************************************************************

OUTPUT  :
---------

Stan has a total of $12576.21.

Process returned 0 (0x0)   execution time : 0.006 s
Press ENTER to continue.

*******************************************************************************/
