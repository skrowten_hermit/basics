/*******************************************************************************
PROGRAM NAME    :   A046-strings-rev+sort.c
TITLE           :   Program to Implement String Reverse and String Sort.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program implements string reverse, revString(), and
                    string sort, sortString() functions using character arrays
                    to reverse a given string and sort a given set of n strings
                    respectively.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


void revString(char str);

void sortString(char str, int num);


int main()
{
    char str1[100], str2[25][50], temp[25];
    int len, i, j, num;

    printf("\n\nPrint Individual Characters of a String in Reverse Order\n");
    printf("----------------------------------------------------------\n");

    printf("Enter the Input String : ");
    scanf("%s", str1);

    len = strlen(str1);

    printf("The characters of the string \"%s\" in reverse order are : \n",
    str1);

    for(i = len; i >= 0; i--)
        printf("%c", str1[i]);

    printf("\n----------------------------------------------------------\n");

    printf("\n\nSort Strings From User\n");
    printf("------------------------\n");

    printf("Input the Number of Strings : ");
    scanf("%d", &num);
    printf("Enter the Input String(s) : ");

    for(i = 0; i < num; i++)
        scanf("%s", str2[i]);

    for(i = 0; i < num; i++)
        for(j = 0; j < num - i; j++)
            if(strcmp(str2[j], str2[j + 1]) > 0)
            {
                strncpy(temp, str2[j], sizeof(temp) - 1);
                strncpy(str2[j], str2[j + 1], sizeof(str2[j]) - 1);
                strncpy(str2[j + 1], temp, sizeof(str2[j + 1]) - 1);
            }

    printf("Sorted Strings : \n");

    for(i = 0; i < num; i++)
        printf("%s\n", str2[i]);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Sort Strings From User
--------------------------
Input the Number of Strings : 3
Enter the Input String(s) :
Heck
Dear
My
Lord
Sorted Strings :

Dear
Lord

Process returned 0 (0x0)   execution time : 29.799 s
Press ENTER to continue.

*******************************************************************************/
