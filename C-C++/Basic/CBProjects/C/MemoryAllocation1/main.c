/*******************************************************************************
PROGRAM NAME    :   A063-memalloc-strex.c
TITLE           :   Program to Demonstrate Allocation of String Size.
DATE            :   24/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates allocation of memory using the
                    malloc() and realloc() functions for strings. This shows how
                    memory can be saved with character pointers when compared to
                    character arrays where memory allocation is static.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    char *str = NULL;

    // Initial memory allocation.

    str = (char *) malloc(15);
    strcpy(str, "Sreekanth");
    printf("str = %25s,\taddress(str) = %10u,\tlen(str) = %2d,\tsize(str) = %2d\
            \n", str, str, strlen(str), sizeof(str));

    free(str);
    str = NULL;

    printf("str = %25s,\taddress(str) = %10u,\tsize(str) = %2d\n", str, str,
            sizeof(str));

    // 2nd memory allocation.

    str = (char *) malloc(20);
    strcpy(str, "Sreekanth");
    printf("str = %25s,\taddress(str) = %10u,\tlen(str) = %2d,\tsize(str) = %2d\
            \n", str, str, strlen(str), sizeof(str));

    // Reallocation of memory.

    str = (char *) realloc(str, 25);
    strcat(str, ".com");
    printf("str = %25s,\taddress(str) = %10u,\tlen(str) = %2d,\tsize(str) = %2d\
            \n", str, str, strlen(str), sizeof(str));

    free(str);
    str = NULL;

    printf("str = %25s,\taddress(str) = %10u,\tsize(str) = %2d\n", str, str,
            sizeof(str));

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

str = Sreekanth,
address(str) = 3077608032,	len(str) =  9,	size(str) =  8
str = (null),
address(str) =          0,	size(str) =  8
str = Sreekanth,
address(str) = 3077608032,	len(str) =  9,	size(str) =  8
str = Sreekanth.com,
address(str) = 3077609104,	len(str) = 13,	size(str) =  8
str = (null),
address(str) =          0,	size(str) =  8

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
