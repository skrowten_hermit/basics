#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#define MAXSIZE 90
main()
{
int sockfd,newsockfd,retval;
socklen_t actuallen;
int recedbytes,sentbytes;
struct sockaddr_in serveraddr,clientaddr;

char buff[MAXSIZE];
int a=0;
sockfd=socket(AF_INET,SOCK_STREAM,0);
if(sockfd==-1)
{
printf("\n socket creation error");
}
serveraddr.sin_family=AF_INET;
serveraddr.sin_port=htons(3388);
serveraddr.sin_addr.s_addr=htonl(INADDR_ANY);
retval=bind(sockfd, (struct sockaddr*)&serveraddr,sizeof(serveraddr));
if(retval==-1)
{
printf("Binding error");
close(sockfd);
}
retval=listen(sockfd,1);
if(retval==-1)
{
close(sockfd);
}
actuallen=sizeof(clientaddr);
newsockfd=accept(sockfd,(struct sockaddr*)&clientaddr,&actuallen);
if(newsockfd==-1)
{
close(sockfd);
}
recedbytes=recv(newsockfd,buff,sizeof(buff),0);
if(recedbytes==-1)
{
close(sockfd);
close(newsockfd);
}
//puts(buff);
printf("received string is= %s\n",buff);
//reverse a string
char rev[MAXSIZE];
int l=strlen(buff);
int i;
int l1=l;

//printf("the size of buff= %d",l); 

for(i=0,l1=l-1;i<l-1,l1>-1;i++,l1--)
{
rev[l1]=buff[i];
}
rev[l]='\0';
printf("reversed string is= %s\n",rev);
/*printf("\n");
scanf("%s",buff);*/
strcpy(buff,rev);
sentbytes=send(newsockfd,buff,sizeof(buff),0);
if(sentbytes==-1)
{
close(sockfd);
close(newsockfd);
}
close(sockfd);
close(newsockfd);

}
