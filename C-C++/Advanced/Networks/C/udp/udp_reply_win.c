/*
 * Small test utility for rtp-kernel-test, codec-loader and voip-control.
 *
 * This tool is meant to run on the host side. It echos every package seen from
 * the target.
 *
 * To compile for windows, do the following:
 *    1. Install MinGW by running:
 *      $ sudo apt-get install mingw-w64
 *    2. To compile for 32-bit OS:
 * 	$ i686-w64-mingw32-gcc -o udp_reply_32.exe udp_reply_win.c -lws2_32
 *    3. To compile for 64-bit OS:
 *      $ x86_64-w64-mingw32-gcc -o udp_reply_64.exe udp_reply_win.c -lws2_32
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <winsock.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFLEN 1292
#define PORT 5000

//For ipv6 calls
//#define IPV6_SUPPORT 1

char *cmd;
int port = PORT;


int opterr = 1,             /* if error message should be printed */
    optind = 1,             /* index into parent argv vector */
    optopt,                 /* character checked for validity */
    optreset;               /* reset getopt */
char *optarg;               /* argument associated with option */

#define BADCH   (int)'?'
#define BADARG  (int)':'
#define EMSG    ""

/*
* getopt --
*      Parse argc/argv argument vector.
*/
int getopt(int nargc, char * const nargv[], const char *ostr)
{
  static char *place = EMSG;             /* option letter processing */
  const char *oli;                       /* option letter list index */

  if (optreset || !*place)               /* update scanning pointer */
  {              
    optreset = 0;
    if (optind >= nargc || *(place = nargv[optind]) != '-')
    {
      place = EMSG;
      return (-1);
    }
    if (place[1] && *++place == '-')     /* found "--" */
    {
      ++optind;
      place = EMSG;
      return (-1);
    }
  }                                      /* option letter okay? */
  if ((optopt = (int)*place++) == (int)':' || !(oli = strchr(ostr, optopt)))
  {
      /*
      * if the user didn't specify '-' as an option,
      * assume it means -1.
      */
      if (optopt == (int)'-')
        return (-1);
      if (!*place)
        ++optind;
      if (opterr && *ostr != ':')
        (void)printf("illegal option -- %c\n", optopt);
      return (BADCH);
  }
  if (*++oli != ':')                     /* don't need argument */
  {
    optarg = NULL;
    if (!*place)
      ++optind;
  }
  else                                   /* need an argument */
  {
    if (*place)                          /* no white space */
      optarg = place;
    else if (nargc <= ++optind)          /* no arg */
    {
      place = EMSG;
      if (*ostr == ':')
        return (BADARG);
      if (opterr)
        (void)printf("option requires an argument -- %c\n", optopt);
      return (BADCH);
    }
    else                                 /* white space */
      optarg = nargv[optind];
    place = EMSG;
    ++optind;
  }
  return (optopt);                       /* dump back option letter */
}

void usage(void)
{
	printf("%s [-h] [-p <port>]\n"
	       "\n"
	       "Options:\n"
	       "  -h         show this help\n"
	       "  -p <port>  bind to <port>number instead of %d\n",
	       cmd,
	       PORT);
	exit(EXIT_FAILURE);
}

void parse_cmdline(int argc, char *argv[])
{
	char *options = "hp:";
	int optchar;

	cmd = argv[0];

	while ((optchar = getopt(argc, argv, options)) != -1) {
		switch (optchar) {
		case 'h':
			usage();
			break;
		case 'p':
			port = atoi(optarg);
			break;
		default:
			usage();
		}
	}
}

void diep(char *s)
{
	perror(s);
	exit(1);
}

int main(int argc, char *argv[])
{
	
#ifdef IPV6_SUPPORT	
	struct sockaddr_in6 si_me, si_other;
#else
	struct sockaddr_in si_me, si_other;
#endif 


	int s, i, slen=sizeof(si_other);
	char buf[BUFLEN];
	int len;

	parse_cmdline(argc, argv);

    WSADATA wsaData;

    if(WSAStartup(0x202, &wsaData) == 0)
    {

#ifdef IPV6_SUPPORT	

	if ((s=socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP))==-1)
		diep("socket");

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin6_family = AF_INET6;
	si_me.sin6_port = htons(port);
	//si_me.sin6_addr.s6_addr = htonl(INADDR_ANY);in6addr_any
	//si_me.sin6_addr.s6_addr = in6addr_any;
	memset(si_me.sin6_addr.s6_addr,0,16);

#else
	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
		diep("socket");

	memset((char *) &si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(port);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);

#endif


	if (bind(s, (struct sockaddr *)&si_me, sizeof(si_me))==-1)
		diep("bind");

	for (;;) {
		len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *)&si_other,
		               &slen);
		
		if(len < 0)
			diep("recvfrom()");

		printf(".");
		fflush(stdout);

		len = sendto(s, buf, len, 0, (struct sockaddr *)&si_other,
		             slen);
		if(len < 0)
			diep("sendto()");
	}

	close(s);
        WSACleanup();
    }
    else
        printf("ERROR: Initialization failure.\n");
   return 0;
}
