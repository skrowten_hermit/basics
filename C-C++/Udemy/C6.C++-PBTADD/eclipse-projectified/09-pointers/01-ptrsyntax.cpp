/*
 * 01-ptrsyntax.cpp
 * Program to Demonstrate Pointer Syntax
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include <iostream>
using namespace std;
int main()
{
    int a=10;
    int *p=&a;
    cout<<a<<endl;
    cout<<&a<<endl;
    cout<<p<<endl;
    cout<<*p<<endl;
    return 0;
}
