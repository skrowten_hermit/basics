/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Non-executable Program With Definition of SayHello Function.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program illustrates how to create functions in
                    different source files and how to use them in the "main.cpp".
                    The function sayhello() is defined in "sayhello.cpp" here.
*******************************************************************************/


#include <iostream>


void sayhello(void)
{
    std::string str;

    std::cout << "Tell me your name : ";
    std::cin >> str;

    std::cout << std::endl << "Hello Mr./Ms. " << str << "! Welcome to C++!" << std::endl;
}
