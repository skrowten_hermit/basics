/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Calculate Net Salary of an Employee.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to calculate the net salary of an employee
                    from the data provided by the user as input. The calculated
                    net salary is displayed as an output to the user.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


int main()
{
    float basic;
    float percentAllow;
    float percentDeduct;
    float netSalary;

    cout << "Enter Basic Salary : ";
    cin >> basic;
    cout << "Enter percent of Allowances : ";
    cin >> percentAllow;
    cout << "Enter percent of Deductions : ";
    cin >> percentDeduct;

    netSalary = basic + basic * percentAllow / 100 - basic * percentDeduct / 100;

    cout << "Net Salary is : " << netSalary << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
