/*******************************************************************************
PROGRAM NAME    :   A055-pointers-arrays.c
TITLE           :   Program to Demonstrate Array Processing/Manipulation Using
                    Pointer Arithmetic.
DATE            :   18/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the close relationship between
                    pointers and arrays and how to use pointer arithmetic to
                    access and manipulate array elements.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int values[5] = {15, 672, 23, 249, 58};
    int *pval;

    printf("01. Original Array :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, values[i], i, &values[i],
                i, &values[i]);

    printf("\n02. Original Array (as pointer, *(values + i)) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, *(values + i),
                i, (values + i), i, (values + i));

    pval = values;

    printf("\n03. Pointer pval[i] (fetched from original array) :\n");

    for(int i = 0; i < 5; i++)
        printf("pval[%d] = %6d,\taddress(pval[%d]) = %14d,"
                "\taddress(pval[%d]) = %17p\n", i, pval[i], i, pval + i,
                i, pval + i);

    printf("\n04. Pointer *pval++ (fetched from original array) :\n");

    for(int i = 0; i < 5; i++)
    {
        printf("*pval = %8d,\tpval (address) = %16d,"
                "\tpval (address) = %19p\n", *pval, pval, pval);
        *pval++;
    }

    ////////////////////////////////////////////////////////////////////////////

    printf("\n\nAssigning address of the array (&values) to the pointer "
            "pval....\n");

    pval = &values;         // Assigning the address of a first array element.

    printf("\n*pval = %8d,\tpval (address) = %16d,\tpval (address) = %19p\n",
            *pval, pval, pval);

    // Assigning the address of a specific array element (2nd element here).
    pval = &values[1];

    printf("\nUpdated pval address as pointing to 2nd element "
            "(i.e., &values[1])...\n\n*pval = %8d,\tpval (address) = %16d,"
            "\tpval (address) = %19p\n\n\n", *pval, pval, pval);

    printf("05. Original Array (post pointer updation):\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, values[i], i, &values[i],
                i, &values[i]);

    printf("\n06. Original Array (as pointer, *(values + i), post pointer "
            "updation) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, *(values + i),
                i, (values + i), i, (values + i));

    printf("\n07. Pointer pval[i] (fetched from original array, post pointer "
            "updation) :\n");

    for(int i = 0; i < 5; i++)
        printf("pval[%d] = %6d,\taddress(pval[%d]) = %14d,\taddress(pval[%d]) "
                "= %17p\n", i, pval[i], i, pval + i, i, pval + i);

    printf("\n08. Pointer *pval++ (fetched from original array, post pointer "
            "updation) :\n");

    for(int i = 0; i < 5; i++)
    {
        printf("*pval = %8d,\tpval (address) = %16d,\tpval (address) = %19p\n",
                *pval, pval, pval);
        *pval++;
    }

    ////////////////////////////////////////////////////////////////////////////

    // Assigning the address of a specific array element (2nd element here).
    pval = &values[1];
    // Incrementing the address to point to the next item, i.e., the 3rd array
    // element.
    pval += 1;

//    pval = &values[2];      // Equivalent to the above 2 lines.

    printf("\n\nUpdated pval address as pointing to 3rd element (i.e., "
            "&values[2])...\n\n*pval = %8d,\tpval (address) = %16d,"
            "\tpval (address) = %19p\n\n", *pval, pval, pval);

    printf("Reassigning values to the array....\n");

    for(int i = 0; i < 5; i++)
        values[i] = i + 1;

    printf("\n*pval = %8d,\tpval (address) = %16d,\tpval (address) = %19p\n\n",
            *pval, pval, pval);

    printf("\n09. Original Array (after reassignment of values to array) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, values[i],
                i, &values[i], i, &values[i]);

    printf("\n10. Original Array (as pointer, *(values + i) after reassignment "
            "of values to array) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, *(values + i),
                i, (values + i), i, (values + i));

    printf("\n11. Pointer pval[i] (after reassignment of values to array) :\n");

    for(int i = 0; i < 5; i++)
        printf("pval[%d] = %6d,\taddress(pval[%d]) = %14d,"
                "\taddress(pval[%d]) = %17p\n", i, pval[i], i, pval + i,
                i, pval + i);

    printf("\n12. Omitting as it points to location outside the array...!!!\n");

//    for(int i = 0; i < 5; i++)
//    {
//        printf("*pval = %8d,\tpval (address) = %16d,\tpval (address) = %19p"
//                "\n", *pval, pval, pval);
//        *pval++;
//    }

    ////////////////////////////////////////////////////////////////////////////

    *pval = NULL;         // Assigns value 0 to element 2 of array.

    printf("\n\nAssigning NULL to the current location of pval (which is "
            "pointing to 3rd element i.e., &values[2])...\n\n*pval = %8d,"
            "\tpval (address) = %16d,\tpval (address) = %19p\n\n",
            *pval, pval, pval);

    printf("\n13. Original Array (after NULL assignment to pointer pval) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, values[i],
                i, &values[i], i, &values[i]);

    printf("\n14. Original Array (as pointer, *(values + i) after NULL "
            "assignment to pointer pval) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, *(values + i),
                i, (values + i), i, (values + i));

    printf("\n15. Pointer pval[i] (after NULL assignment to pointer "
            "pval) :\n");

    for(int i = 0; i < 5; i++)
        printf("pval[%d] = %6d,\taddress(pval[%d]) = %14d,"
                "\taddress(pval[%d]) = %17p\n", i, pval[i], i, pval + i,
                i, pval + i);

    printf("\n16. Omitting as it points to location outside the array...!!!\n");

//    for(int i = 0; i < 5; i++)
//    {
//        printf("*pval = %8d,\tpval (address) = %16d,\tpval (address) "
//                "= %19p\n", *pval, pval, pval);
//        *pval++;
//    }

    ////////////////////////////////////////////////////////////////////////////

    // Important to do this - initializes the address to top/first element of
    // array. Otherwise, pval points to 2nd element in values due to the
    // statement pval = &values[1] above. But Why?
    pval = values;

    printf("\n\nReassigning values to the array....\n\n");

    for(int i = 0; i < 5; i++)
        values[i] = i + 101;

    printf("\n17. Original Array (after reassigning the array, before "
            "modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, values[i],
                i, &values[i], i, &values[i]);

    printf("\n18. Original Array (as pointer, *(values + i) after reassigning "
            "the array, before modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, *(values + i),
                i, (values + i), i, (values + i));

    printf("\n19. Pointer pval[i] (after reassigning the array, before "
            "modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
        printf("pval[%d] = %6d,\taddress(pval[%d]) = %14d,"
                "\taddress(pval[%d]) = %17p\n", i, pval[i], i, pval + i,
                i, pval + i);

    printf("\n20. Pointer *pval++ (after reassigning the array, before "
            "modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
    {
        printf("*pval = %8d,\tpval (address) = %16d,"
                "\tpval (address) = %19p\n", *pval, pval, pval);
        *pval++;
    }

    ////////////////////////////////////////////////////////////////////////////

    printf("\n\nRepointing to the first element of the array....\n");

    pval = &values[0];    // Same as the statement pval = values.

    printf("\nThe current location of pval (which is pointing to 1st element "
            "i.e., &values[0]):\n\n*pval\t\t= %5d,\tpval (address) = %15d,"
            "\tpval (address) = %15p\n\n", *pval, pval, pval);

    printf("\nModifying with the help of pointer....\n\n");

    *pval = 15;
    *(pval + 1) = 672;

    printf("*pval\t\t= %5d,\tpval (address) = %15d,\tpval (address) = %p\n",
            *pval, pval, pval);
    printf("*(pval + 1)\t= %5d,\tpval (address) = %15d,\tpval (address) = %p\n",
            *(pval + 1), pval, pval);

    printf("\nThe current location of pval (which is pointing to 1st element "
            "i.e., &values[0]):\n\n*pval\t\t= %5d,\tpval (address) = %15d,"
            "\tpval (address) = %p\n\n", *pval, pval, pval);

    printf("\n21. Original Array (after modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, values[i], i, &values[i],
                i, &values[i]);

    printf("\n22. Original Array (as pointer, *(values + i) after modification "
            "by pointer) :\n");

    for(int i = 0; i < 5; i++)
        printf("values[%d] = %4d,\taddress(values[%d]) = %12d,"
                "\taddress(values[%d]) = %15p\n", i, *(values + i),
                i, (values + i), i, (values + i));

    printf("\n23. Pointer pval[i] (after modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
        printf("pval[%d] = %6d,\taddress(pval[%d]) = %14d,"
                "\taddress(pval[%d]) = %17p\n", i, pval[i], i, pval + i,
                i, pval + i);

    printf("\n24. Pointer *pval++ (after modification by pointer) :\n");

    for(int i = 0; i < 5; i++)
    {
        printf("*pval = %8d,\tpval (address) = %16d,\tpval (address) = %19p\n",
                *pval, pval, pval);
        *pval++;
    }

    printf("\n25. Pointer *pval-- (after walking to the end of array, trying to"
            " get back to the top) :\n");

    for(int i = 0; i < 5; i++)
    {
        *pval--;
        printf("*pval = %8d,\tpval (address) = %16d,\tpval (address) = %19p\n",
                *pval, pval, pval);
    }

    printf("\nThe current location of pval (which is pointing to 1st element "
            "i.e., &values[0]):\n\n*pval\t\t= %5d,\tpval (address) = %15d,"
            "\tpval (address) = %p\n\n", *pval, pval, pval);

    return 0;
}



/*******************************************************************************

01. Original Array :
values[0] =   15,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  672,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =   23,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  249,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =   58,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

02. Original Array (as pointer, *(values + i)) :
values[0] =   15,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  672,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =   23,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  249,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =   58,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

03. Pointer pval[i] (fetched from original array) :
pval[0] =     15,
address(pval[0]) =     2059750944,	address(pval[0]) =    0x7ffe7ac54e20
pval[1] =    672,
address(pval[1]) =     2059750948,	address(pval[1]) =    0x7ffe7ac54e24
pval[2] =     23,
address(pval[2]) =     2059750952,	address(pval[2]) =    0x7ffe7ac54e28
pval[3] =    249,
address(pval[3]) =     2059750956,	address(pval[3]) =    0x7ffe7ac54e2c
pval[4] =     58,
address(pval[4]) =     2059750960,	address(pval[4]) =    0x7ffe7ac54e30

04. Pointer *pval++ (fetched from original array) :
*pval =       15,
pval (address) =       2059750944,	pval (address) =      0x7ffe7ac54e20
*pval =      672,
pval (address) =       2059750948,	pval (address) =      0x7ffe7ac54e24
*pval =       23,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28
*pval =      249,
pval (address) =       2059750956,	pval (address) =      0x7ffe7ac54e2c
*pval =       58,
pval (address) =       2059750960,	pval (address) =      0x7ffe7ac54e30


Assigning address of the array (&values) to the pointer pval....

*pval =       15,
pval (address) =       2059750944,	pval (address) =      0x7ffe7ac54e20

Updated pval address as pointing to 2nd element (i.e., &values[1])...

*pval =      672,
pval (address) =       2059750948,	pval (address) =      0x7ffe7ac54e24


05. Original Array (post pointer updation):
values[0] =   15,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  672,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =   23,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  249,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =   58,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

06. Original Array (as pointer, *(values + i), post pointer updation) :
values[0] =   15,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  672,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =   23,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  249,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =   58,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

07. Pointer pval[i] (fetched from original array, post pointer updation) :
pval[0] =    672,
address(pval[0]) =     2059750948,	address(pval[0]) =    0x7ffe7ac54e24
pval[1] =     23,
address(pval[1]) =     2059750952,	address(pval[1]) =    0x7ffe7ac54e28
pval[2] =    249,
address(pval[2]) =     2059750956,	address(pval[2]) =    0x7ffe7ac54e2c
pval[3] =     58,
address(pval[3]) =     2059750960,	address(pval[3]) =    0x7ffe7ac54e30
pval[4] =  32766,
address(pval[4]) =     2059750964,	address(pval[4]) =    0x7ffe7ac54e34

08. Pointer *pval++ (fetched from original array, post pointer updation) :
*pval =      672,
pval (address) =       2059750948,	pval (address) =      0x7ffe7ac54e24
*pval =       23,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28
*pval =      249,
pval (address) =       2059750956,	pval (address) =      0x7ffe7ac54e2c
*pval =       58,
pval (address) =       2059750960,	pval (address) =      0x7ffe7ac54e30
*pval =    32766,
pval (address) =       2059750964,	pval (address) =      0x7ffe7ac54e34


Updated pval address as pointing to 3rd element (i.e., &values[2])...

*pval =       23,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28

Reassigning values to the array....

*pval =        3,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28


09. Original Array (after reassignment of values to array) :
values[0] =    1,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =    2,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =    3,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =    4,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =    5,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

10. Original Array (as pointer, *(values + i) after reassignment of values
to array) :
values[0] =    1,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =    2,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =    3,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =    4,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =    5,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

11. Pointer pval[i] (after reassignment of values to array) :
pval[0] =      3,
address(pval[0]) =     2059750952,	address(pval[0]) =    0x7ffe7ac54e28
pval[1] =      4,
address(pval[1]) =     2059750956,	address(pval[1]) =    0x7ffe7ac54e2c
pval[2] =      5,
address(pval[2]) =     2059750960,	address(pval[2]) =    0x7ffe7ac54e30
pval[3] =  32766,
address(pval[3]) =     2059750964,	address(pval[3]) =    0x7ffe7ac54e34
pval[4] = -278774016,
address(pval[4]) =     2059750968,	address(pval[4]) =    0x7ffe7ac54e38

12. Omitting as it points to location outside the array...!!!


Assigning NULL to the current location of pval (which is pointing to 3rd element
i.e., &values[2])...

*pval =        0,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28


13. Original Array (after NULL assignment to pointer pval) :
values[0] =    1,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =    2,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =    0,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =    4,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =    5,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

14. Original Array (as pointer, *(values + i) after NULL assignment to
pointer pval) :
values[0] =    1,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =    2,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =    0,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =    4,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =    5,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

15. Pointer pval[i] (after NULL assignment to pointer pval) :
pval[0] =      0,
address(pval[0]) =     2059750952,	address(pval[0]) =    0x7ffe7ac54e28
pval[1] =      4,
address(pval[1]) =     2059750956,	address(pval[1]) =    0x7ffe7ac54e2c
pval[2] =      5,
address(pval[2]) =     2059750960,	address(pval[2]) =    0x7ffe7ac54e30
pval[3] =  32766,
address(pval[3]) =     2059750964,	address(pval[3]) =    0x7ffe7ac54e34
pval[4] = -278774016,
address(pval[4]) =     2059750968,	address(pval[4]) =    0x7ffe7ac54e38

16. Omitting as it points to location outside the array...!!!


Reassigning values to the array....


17. Original Array (after reassigning the array,
before modification by
pointer) :
values[0] =  101,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  102,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =  103,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  104,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =  105,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

18. Original Array (as pointer, *(values + i) after reassigning the array,
before modification by pointer) :
values[0] =  101,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  102,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =  103,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  104,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =  105,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

19. Pointer pval[i] (after reassigning the array, before modification by
pointer) :
pval[0] =    101,
address(pval[0]) =     2059750944,	address(pval[0]) =    0x7ffe7ac54e20
pval[1] =    102,
address(pval[1]) =     2059750948,	address(pval[1]) =    0x7ffe7ac54e24
pval[2] =    103,
address(pval[2]) =     2059750952,	address(pval[2]) =    0x7ffe7ac54e28
pval[3] =    104,
address(pval[3]) =     2059750956,	address(pval[3]) =    0x7ffe7ac54e2c
pval[4] =    105,
address(pval[4]) =     2059750960,	address(pval[4]) =    0x7ffe7ac54e30

20. Pointer *pval++ (after reassigning the array, before modification by
pointer) :
*pval =      101,	pval (address) =       2059750944,
pval (address) =      0x7ffe7ac54e20
*pval =      102,	pval (address) =       2059750948,
pval (address) =      0x7ffe7ac54e24
*pval =      103,	pval (address) =       2059750952,
pval (address) =      0x7ffe7ac54e28
*pval =      104,	pval (address) =       2059750956,
pval (address) =      0x7ffe7ac54e2c
*pval =      105,	pval (address) =       2059750960,
pval (address) =      0x7ffe7ac54e30


Repointing to the first element of the array....

The current location of pval (which is pointing to 1st element
i.e., &values[0]):

*pval		=   101,
pval (address) =      2059750944,	pval (address) =  0x7ffe7ac54e20


Modifying with the help of pointer....

*pval		=    15,
pval (address) =      2059750944,	pval (address) = 0x7ffe7ac54e20
*(pval + 1)	=   672,
pval (address) =      2059750944,	pval (address) = 0x7ffe7ac54e20

The current location of pval (which is pointing to 1st element
i.e., &values[0]):

*pval		=    15,
pval (address) =      2059750944,	pval (address) = 0x7ffe7ac54e20


21. Original Array (after modification by pointer) :
values[0] =   15,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  672,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =  103,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  104,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =  105,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

22. Original Array (as pointer, *(values + i) after modification by pointer) :
values[0] =   15,
address(values[0]) =   2059750944,	address(values[0]) =  0x7ffe7ac54e20
values[1] =  672,
address(values[1]) =   2059750948,	address(values[1]) =  0x7ffe7ac54e24
values[2] =  103,
address(values[2]) =   2059750952,	address(values[2]) =  0x7ffe7ac54e28
values[3] =  104,
address(values[3]) =   2059750956,	address(values[3]) =  0x7ffe7ac54e2c
values[4] =  105,
address(values[4]) =   2059750960,	address(values[4]) =  0x7ffe7ac54e30

23. Pointer pval[i] (after modification by pointer) :
pval[0] =     15,
address(pval[0]) =     2059750944,	address(pval[0]) =    0x7ffe7ac54e20
pval[1] =    672,
address(pval[1]) =     2059750948,	address(pval[1]) =    0x7ffe7ac54e24
pval[2] =    103,
address(pval[2]) =     2059750952,	address(pval[2]) =    0x7ffe7ac54e28
pval[3] =    104,
address(pval[3]) =     2059750956,	address(pval[3]) =    0x7ffe7ac54e2c
pval[4] =    105,
address(pval[4]) =     2059750960,	address(pval[4]) =    0x7ffe7ac54e30

24. Pointer *pval++ (after modification by pointer) :
*pval =       15,
pval (address) =       2059750944,	pval (address) =      0x7ffe7ac54e20
*pval =      672,
pval (address) =       2059750948,	pval (address) =      0x7ffe7ac54e24
*pval =      103,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28
*pval =      104,
pval (address) =       2059750956,	pval (address) =      0x7ffe7ac54e2c
*pval =      105,
pval (address) =       2059750960,	pval (address) =      0x7ffe7ac54e30

25. Pointer *pval-- (after walking to the end of array, trying to get back
to the top) :
*pval =      105,
pval (address) =       2059750960,	pval (address) =      0x7ffe7ac54e30
*pval =      104,
pval (address) =       2059750956,	pval (address) =      0x7ffe7ac54e2c
*pval =      103,
pval (address) =       2059750952,	pval (address) =      0x7ffe7ac54e28
*pval =      672,
pval (address) =       2059750948,	pval (address) =      0x7ffe7ac54e24
*pval =       15,
pval (address) =       2059750944,	pval (address) =      0x7ffe7ac54e20

The current location of pval (which is pointing to 1st element
i.e., &values[0]):

*pval		=    15,
pval (address) =      2059750944,	pval (address) = 0x7ffe7ac54e20


Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
