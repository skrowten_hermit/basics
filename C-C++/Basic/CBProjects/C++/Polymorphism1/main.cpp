/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Virtual Functions.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate virtual functions in
                    polymorphism using a basic example of Car.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class BasicCar
{
    public:
        virtual void start(){cout<<"BasicCar started"<<endl;}
};


class AdvancedCar: public BasicCar
{
    public:
        void start(){cout<<"AdvancedCar Started"<<endl;}
};


int main()
{
    BasicCar *p = new AdvancedCar();
    p->start();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
