/*******************************************************************************
PROGRAM NAME    :   A034-tic-tac-toe.c
TITLE           :   Program to Play Tic-Tac-Toe With 2 Players.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program imitates the tic-tac-toe game, where there are
                    2 players playing against each other. Each player has to
                    tell the program the square to mark his play (Xs and Os).
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <ncurses.h>


char square[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};


int choice, player;

int checkForWin(void);

void displayBoard(void);

void markBoard(char mark);


// main() is where the gameplay is coded.


int main(void)
{
    int gameStatus;
    char mark;

    player = 1; // To start with player 1.

    do
    {
        displayBoard();

        player = (player % 2) ? 1 : 2;

        printf("Player %d, your turn. Enter a square to mark : ", player);
        scanf("%d", &choice);

        mark = (player == 1) ? 'X' : 'O';

        markBoard(mark);

        gameStatus = checkForWin();

        player++;
    }while(gameStatus == -1);

    if (gameStatus == 1)
        printf("\n\n==>\aPlayer %d wins!!!\n\n", --player);
    else
        printf("\n\n==>\aDraw Game!!!\n\n");

    return 0;
}


/*******************************************************************
checkForWin() -- Function to return game status
-------------

 1  -->  The game is OVER and has a RESULT.
-1  -->  Game is IN PROGRESS.
 0  -->  The game is OVER and has NO RESULT.
*******************************************************************/


int checkForWin(void)
{
    int retVal = 0;

    if (square[1] == square[2] && square[2] == square[3])
    {
        retVal = 1;
    }
    else if (square[4] == square[5] && square[5] == square[6])
    {
        retVal = 1;
    }
    else if (square[7] == square[8] && square[8] == square[9])
    {
        retVal = 1;
    }
    else if (square[1] == square[5] && square[5] == square[9])
    {
        retVal = 1;
    }
    else if (square[1] == square[4] && square[4] == square[7])
    {
        retVal = 1;
    }
    else if (square[2] == square[5] && square[5] == square[8])
    {
        retVal = 1;
    }
    else if (square[3] == square[6] && square[6] == square[9])
    {
        retVal = 1;
    }
    else if (square[3] == square[5] && square[5] == square[7])
    {
        retVal = 1;
    }
    else if (square[1] != '1' && square[2] != '2' && square[3] != '3' &&
             square[4] != '4' && square[5] != '5' && square[6] != '6' &&
             square[7] != '7' && square[8] != '8' && square[9] != '9')
    {
        retVal = 0;
    }
    else
    {
        retVal = -1;
    }

    return retVal;
}


/*******************************************************************
displayBoard() --   Function to draw the board of tic tac toe with
-------------       the player's marking.
*******************************************************************/


void displayBoard()
{
    system("clear");

    printf("\n\n\tTic-Tac-Toe Game\n\t----------------\n\n");

    printf("Player 1 (X)  -  Player 2 (O)\n\n\n");

    printf("     |     |     \n");
    printf("  %c  |  %c  |  %c \n", square[1], square[2], square[3]);

    printf("_____|_____|_____\n");
    printf("     |     |     \n");

    printf("  %c  |  %c  |  %c \n", square[4], square[5], square[6]);

    printf("_____|_____|_____\n");
    printf("     |     |     \n");

    printf("  %c  |  %c  |  %c \n", square[7], square[8], square[9]);

    printf("     |     |     \n\n");
}


/*******************************************************************
markBoard() --      Set the board with the correct character, 'X' or
-----------         'O' in the correct spot in the array.
*******************************************************************/


void markBoard(char mark)
{
    int c;

    if (choice == 1 && square[1] == '1')
        square[1] = mark;
    else if (choice == 2 && square[2] == '2')
        square[2] = mark;
    else if (choice == 3 && square[3] == '3')
        square[3] = mark;
    else if (choice == 4 && square[4] == '4')
        square[4] = mark;
    else if (choice == 5 && square[5] == '5')
        square[5] = mark;
    else if (choice == 6 && square[6] == '6')
        square[6] = mark;
    else if (choice == 7 && square[7] == '7')
        square[7] = mark;
    else if (choice == 8 && square[8] == '8')
        square[8] = mark;
    else if (choice == 9 && square[9] == '9')
        square[9] = mark;
    else
    {
        printf("Invalid move!!!\n");

        player--;
        c = getchar();
    }
}



/*******************************************************************************

OUTPUT  :
---------



	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  1  |  2  |  3
_____|_____|_____
     |     |
  4  |  5  |  6
_____|_____|_____
     |     |
  7  |  8  |  9
     |     |

Player 1, your turn. Enter a square to mark : 5


	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  1  |  2  |  3
_____|_____|_____
     |     |
  4  |  X  |  6
_____|_____|_____
     |     |
  7  |  8  |  9
     |     |

Player 2, your turn. Enter a square to mark : 6


	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  1  |  2  |  3
_____|_____|_____
     |     |
  4  |  X  |  O
_____|_____|_____
     |     |
  7  |  8  |  9
     |     |

Player 1, your turn. Enter a square to mark : 3


	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  1  |  2  |  X
_____|_____|_____
     |     |
  4  |  X  |  O
_____|_____|_____
     |     |
  7  |  8  |  9
     |     |

Player 2, your turn. Enter a square to mark : 7


	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  1  |  2  |  X
_____|_____|_____
     |     |
  4  |  X  |  O
_____|_____|_____
     |     |
  O  |  8  |  9
     |     |

Player 1, your turn. Enter a square to mark : 2


	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  1  |  X  |  X
_____|_____|_____
     |     |
  4  |  X  |  O
_____|_____|_____
     |     |
  O  |  8  |  9
     |     |

Player 2, your turn. Enter a square to mark : 1


	Tic-Tac-Toe Game
	----------------

Player 1 (X)  -  Player 2 (O)


     |     |
  O  |  X  |  X
_____|_____|_____
     |     |
  4  |  X  |  O
_____|_____|_____
     |     |
  O  |  8  |  9
     |     |

Player 1, your turn. Enter a square to mark : 8


==>Player 1 wins!!!


Process returned 0 (0x0)   execution time : 250.937 s
Press ENTER to continue.

*******************************************************************************/
