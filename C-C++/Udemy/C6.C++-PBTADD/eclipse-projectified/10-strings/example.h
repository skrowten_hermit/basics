/*
 * example.h
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */

#ifndef EXAMPLE_H_
#define EXAMPLE_H_

namespace exns {

class example {
public:
    example();
    virtual ~example();
};

} /* namespace exns */

#endif /* EXAMPLE_H_ */
