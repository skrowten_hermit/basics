#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */

typedef unsigned char uint8_t;
		
typedef union{
  struct{
	unsigned sunday    : 1;
  	unsigned monday    : 1;
  	unsigned tuesday   : 1;
  	unsigned wednesday : 1;
  	unsigned thursday  : 1;
  	unsigned friday    : 1;  	                        
  	unsigned saturday  : 1;
  } days;
  uint8_t byte_sized;
}weekdays;                            



void main(void) {
  volatile weekdays active_days;    
  active_days.byte_sized=0x36; // 0b00110110
  EnableInterrupts;
  /* include your code here */

  for(;;) {
	  if (active_days.days.thursday)
		  active_days.byte_sized=76;
    __RESET_WATCHDOG();	/* feeds the dog */
  } /* loop forever */
  /* please make sure that you never leave main */
}
