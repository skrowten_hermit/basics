/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program for Linear Search.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to perform linear search on all
                    the elements stored in an array.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int A[10]={2, 4, 6, 8, 12, 3, 5, 7, 9};
    int key;

    cout << "Enter a Key element ";
    cin >> key;

    for(int i = 0; i < 10; i++)
    {
        if(key == A[i])
        {
            cout << "The Key element is found at " << i << endl;
            exit(0);
        }
    }

    cout << "Key element not found" << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
