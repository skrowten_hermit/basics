/*******************************************************************************
PROGRAM NAME    :   A0<last-number>a-add-func.c
TITLE           :   Program to define a Sub-function in a Sample C Application.
DATE            :   <last-date>
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program defines a sub-function add() as part of a
                    sample C application.
*******************************************************************************/



void add(void)
{
    int num1, num2, sum;

    printf("\nEnter 2 numbers : \n");
    scanf("%d %d", &num1, &num2);

    sum = num1 + num2;

    printf("\nSum = %d\n");
}
