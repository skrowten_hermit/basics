/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program for Linear Search using Functions.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate functions by implementing
                    a linear search in the function.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


int Search(int A[], int n, int key)
{
    for(int i = 0; i < n; i++)
        if(key == A[i])
            return i;

    return 0;
}


int main()
{
    int A[]={2, 4, 5, 7, 10, 9, 13};
    int k;

    cout << "Enter an Element to be Searched : ";
    cin >> k;

    int index = Search(A, 7, k);

    cout << "Element " << k << " was found at index : " << index << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
