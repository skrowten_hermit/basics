/*
 * 11-retnbyref.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include<iostream>
using namespace std;

int &fun(int &a)
{
    cout<<a;
    return a;
}

int main()
{
    int x=10;
    fun(x)=25;
    cout<<x;
}
