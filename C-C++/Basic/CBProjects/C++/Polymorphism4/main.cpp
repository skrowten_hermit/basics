/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Function Overriding.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate function overriding using a
                    basic class.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


class Base
{
    public:
        void fun()
        {
            cout << "fun() of Base" << endl;
        }
};


class Derived: public Base
{
    public:
        void fun()
        {
            cout << "fun() of Derived" << endl;
        }
};


int main()
{
    Derived d;
    d.fun();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
