% Figure Plotting Examples
% Clean up the workspace
close all; clear all; clc;

noiseAmplitude = 0.2;

% Surf plotting example
[X,Y] = meshgrid(1:0.5:10,1:20);
Z = sin(X) + cos(Y);
figure;
surf(X,Y,Z);
title('Surf Plot');

% 2-D Line Plot Example
x = 0:0.01:20;
y = sin(x);
noisy_y = y + noiseAmplitude * randn(1, length(y));
figure;
plot(x,noisy_y);
title('Noisy Sine Wave');
figure;
plot(x,y);
title('Clean Sine Wave');

% Histogram Plot Example
Uniform_dist = rand(1,1000);
Normal_dist = randn(1, 1000);

figure;
hist(Uniform_dist);
title('Uniform Distribution');

figure;
hist(Normal_dist);
title('Normal Distribution');


