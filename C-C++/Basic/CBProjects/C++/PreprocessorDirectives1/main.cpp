/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Preprocessor Directives.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to demonstrate different
                    preprocessor directives

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;

#define max(x,y) (x>y?x:y)

#define msg(x) #x

#ifndef PI
    #define PI 3.1425
#endif


int main()
{
    cout << PI;
    cout << max(10,12) << endl;
    cout << msg(hello) << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
