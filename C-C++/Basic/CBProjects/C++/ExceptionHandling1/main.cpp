/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Exception Handling.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate exception handling for a
                    divide by zero exception.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int division(int a, int b)throw(int)
{
    if(b == 0)
        throw 1;

    return a / b;
}


int main()
{
    int x = 10, y = 2, z;

    try
    {
        z = division(x, y);
        cout << z << endl;
    }
    catch(int e)
    {
        cout << "Division by Zero : " << e << endl;
    }

    cout << "Bye!" << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
