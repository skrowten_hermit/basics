/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Overflow.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate overflow.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    char a = 128;
    cout << (int)a << endl;
    char b = 127;
    b++;
    cout << (int)b << endl;
    char c = -129;
    cout << (int)c << endl;
    char d = -128;
    d--;
    cout << (int)d << endl;
    int e = INT_MAX;
    e++;
    cout << (int)e << endl;
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
