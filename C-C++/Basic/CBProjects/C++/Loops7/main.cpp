/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Check if a Number if Prime.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to check if a given number is a prime
                    number.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int n, count = 0;

    cout << "Enter a no. : ";
    cin >> n;

    ;for(int i = 1; i <= n; i++)
    {
        if(n % i == 0)
            count++;
    }

    if(count == 2)
        cout << "It's a Prime\n";
    else
        cout << "It's not a Prime\n";

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
