/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Pass/Call By Value/Reference/Address.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate pass/call by value,
                    pass/call by refernce and pass/call by address on functions.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


// Call by value

void swapVal(int a, int b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
}


// Call by reference

void swapRef(int &a, int &b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
}


// Call by address

void swapAddr(int *x, int *y)
{
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}


int main()
{
    int x = 10, y = 20;
    cout << "Before Swap:\n";
    cout << "x = " << x << ", y = " << y << endl;
    swapV(x, y);
    cout << "After Swap:\n";
    cout << "x = " << x << ", y = " << y << endl;

    int x = 10, y = 20;
    cout << "Before Swap:\n";
    cout << "x = " << x << ", y = " << y << endl;
    swapR(x, y);
    cout << "After Swap:\n";
    cout << "x = " << x << ", y = " << y << endl;

    int x = 10, y = 20;
    cout << "Before Swap:\n";
    cout << "x = " << x << ", y = " << y << endl;
    swap(&x, &y);
    cout << "After Swap:\n";
    cout << "x = " << x << ", y = " << y << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
