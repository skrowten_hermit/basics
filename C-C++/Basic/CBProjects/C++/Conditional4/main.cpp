/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Dynamic Declaration.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate dynamic declaration

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int a = 10, b = 5;
    if(true)
    {
        int c = a + b;
        cout << c << endl;
    }
    {
        int d = a - b;
        if(true)
        {
            cout << d << endl;
        }
    }
    if(int e = a * b)
    {
        cout << e << endl;
    }
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
