/*******************************************************************************
PROGRAM NAME    :   A044-strings-convert.c
TITLE           :   Program to Demonstrate Conversion Of String to Double.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates conversion of a string to double
                    using strtod(), a standard library function (stdlib) in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>     // Required here.
#include <stdbool.h>


int main(void)
{
    double value = 0;
    char str[] = "3.5 2.5 1.26";        // The string to be converted.
    char *pstr = str;                   // Pointer to the string to be converted.
    char *ptr = NULL;                   // Pointer to the character position after conversion.

    while(true)
    {
        value = strtod(pstr, &ptr);     // Convert starting at 'pstr'.

        if(pstr == ptr)                 // Store 'pstr' if no conversion.
            break;                      // Done. Exit.
        else
        {
            printf(" %f\n", value);       // Output the resultant value.
            pstr = ptr;                 // Store the start for the next conversion.
        }
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

 3.500000
 2.500000
 1.260000

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
