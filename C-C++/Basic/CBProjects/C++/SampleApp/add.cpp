/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Non-executable Program With Definition of Add Function.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program illustrates how to create functions in
                    different source files and how to use them in the "main.cpp".
                    The function add() is defined in "add.cpp" here.
*******************************************************************************/


#include <iostream>

using namespace std;


void add(void)
{
    int num1, num2, sum;

    cout << endl << "\nEnter 2 numbers : " << endl;
    cin >> num1 >> num2;

    sum = num1 + num2;

    cout << "\n";
    cout << "Sum = " << sum << endl;
}
