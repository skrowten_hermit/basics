/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Polymorphism using Virtual Functions.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate polymorphism using a class
                    car.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Car
{
    public:
        virtual void start() = 0;
};


class Innova:public Car
{
    public:
        void start(){cout << "Toyota Innova Started...." << endl;}
};


class Swift:public Car
{
    public:
        void start(){cout << "Maruti Suzuki Swift Started...." << endl;}
};


int main()
{
    //Car c;
    Car *p = new Innova();
    p->start();
    p = new Swift();
    p->start();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
