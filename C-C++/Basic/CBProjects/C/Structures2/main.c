/*******************************************************************************
PROGRAM NAME    :   A066-struct-structarray.c
TITLE           :   Program to Demonstrate Array of a Structure.
DATE            :   09/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create an array of a struct
                    - initialize, access them and manipulate them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    struct date
    {
        int day;
        char month[5];
        int year;
    };

    struct date myDates1[5] = {{12, "Jan", 1975}, {8, "Oct", 1943},
                                {22, "Mar", 2012}};
    // The following initialization is same as the above.
//    struct date myDates1[5] = {12, "Jan", 1975, 8, "Oct", 1943,
//                              22, "Mar", 2012};

    struct date myDates2[5] = {[1].month = "Feb", [1].day = 23,
                                [2] = {31, "Aug", 1917}};

    printf();

    printf("\n");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
