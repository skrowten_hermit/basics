/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Simple Conditional Statement.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to demonstrate a simple "if"
                    condition/statement which validates the value given by the
                    user for roll number.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int roll;
    cout << "Enter your Roll No." << endl;
    cin >> roll;
    if(roll > 0)
    {
        cout << "Valid Roll No." << endl;
    }
    else
    {
        cout << "Invalid Roll No." << endl;
    }
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
