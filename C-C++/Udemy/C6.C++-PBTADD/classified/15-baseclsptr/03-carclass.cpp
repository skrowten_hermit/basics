/*
 * 03-carclass.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include<iostream>
using namespace std;

class BasicCar
{
    public:
        void start()
        {
            cout<<"Car started"<<endl;
        }
};

class AdvancedCar: public BasicCar
{
    public:
        void playMusic()
        {
            cout<<"Music Playing"<<endl;
        }
};
