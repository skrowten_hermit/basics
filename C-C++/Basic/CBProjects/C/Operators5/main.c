/*******************************************************************************
PROGRAM NAME    :   A011-sizeof-operations.c
TITLE           :   Program to Demonstrate sizeof() Operator.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates sizeof() operator in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    int x1 = 32;
    float x2 = 19.45;
    long x3 = 9258235;
    double x4 = 35461083.900003;
    char x5 = 'a';
    bool x6 = 0;
    short x7 = 72;

    unsigned int y1 = 49;
//    unsigned float y2 = 2.451;
//    unsigned double y3 = 9734.270001
    unsigned long y2 = 43535;
    long int y3 = 2423;
    long double y4 = 535163.06425;
    long long y5 = 319582;

    enum gender {male, female};
    enum gender myGenderOld, herGenderOld;
    myGenderOld = male;
    herGenderOld = female;
    enum newgender {males, females, trans};
    enum gender myGenderNew, herGenderNew, diffGenderNew;
    myGenderNew = male;
    herGenderNew = female;
    diffGenderNew = trans;

    printf("Size Of (in bytes) Basic DataTypes: \n\t1. int = %zd \
            \n\t2. float = %zd \n\t3. long = %zd \n\t4. double = %zd \
            \n\t5. char = %zd \n\t6. bool = %zd\n\t7. short = %zd\n\n",
            sizeof(x1), sizeof(x2), sizeof(x3), sizeof(x4), sizeof(x5),
            sizeof(x6), sizeof(x7));
    printf("Size Of (in bytes) Composite DataTypes: \n\t1. unsigned int = %u \
            \n\t2. unsigned long = %u \n\t3. long int = %u \
            \n\t4. long double = %u \n\t5. long long = %u \n\n", sizeof(y1),
            sizeof(y2), sizeof(y3), sizeof(y4), sizeof(y5));
    printf("Size Of (in bytes) User-Defined DataTypes: \n\t1. enum1 = %d \
            \n\t2. enum2 = %d \n\n", sizeof(myGenderOld),
            sizeof(diffGenderNew));

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Size Of (in bytes) Basic DataTypes:
	1. int = 4
	2. float = 4
	3. long = 8
	4. double = 8
	5. char = 1
	6. bool = 1
	7. short = 2

Size Of (in bytes) Composite DataTypes:
	1. unsigned int = 4
	2. unsigned long = 8
	3. long int = 8
	4. long double = 16
	5. long long = 8

Size Of (in bytes) User-Defined DataTypes:
	1. enum1 = 4
	2. enum2 = 4


Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
