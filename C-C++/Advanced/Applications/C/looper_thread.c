/*
 * Small utility to estimate how much CPU time is left. It just loops with a
 * very low priority and prints every second how many loops were performed. The
 * conversion to MHz is left as an excersice to the reader.
 *
 * Compile with:
 *
 *      $ arm-linux-gcc looper.c -o looper
 */

#include <sys/time.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/prctl.h>

volatile unsigned long counter, old_counter, measurements, sum, alarm_cnt;
struct timeval checkpoint;
pthread_t looperId;
int loopContinue = 0;

void usage(void)
{
	printf("Usage: looper [#measurements]\n");
	exit(1);
}

void alarm_handler(int signo)
{
	struct timeval cur;
	unsigned long loops = counter - old_counter;
	unsigned long v, secs, usecs;

	// read values
	old_counter = counter;
	gettimeofday(&cur, NULL);

	// output
	secs = cur.tv_sec - checkpoint.tv_sec;
	usecs = secs*1000000 + cur.tv_usec - checkpoint.tv_usec;
	//printf("%ld (%ld, %ld)\n", loops / (usecs >> 10), loops, usecs);
	//printf("%ld\n", loops / (usecs >> 10));
        //if((loops / (usecs >> 10)) < 10000) printf("\nLower than 10000\n");

    alarm_cnt++;
    v= loops / (usecs >> 10);
    sum += v;
	printf("%4ld: +%6ld =%9ld\n", alarm_cnt, v, sum);
	checkpoint = cur;

	if (--measurements == 0) exit(0);
}

void* looper_thread(void *args)
{
	struct itimerval interval;
	struct sigaction action;

	counter = 0;
	old_counter = 0;

	prctl(PR_SET_NAME , "Looper_Th" , 0 , 0 , 0 );

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	// install handler
	memset(&action, 0, sizeof(action));
	action.sa_handler = alarm_handler;
	if (sigaction(SIGALRM, &action, NULL)) {
		perror("sigaction");
		return NULL;
	}

	// start timer
	memset(&interval, 0, sizeof(interval));
	interval.it_interval.tv_sec = 1;
	//interval.it_interval.tv_usec = 20000;
	interval.it_value.tv_sec = 1;
	//interval.it_value.tv_usec = 20000;
	if (setitimer(ITIMER_REAL, &interval, NULL)) {
		perror("settimer");
		return NULL;
	}

	// loop forever at lowest priority possible
	printf("Start looping...\n");
	gettimeofday(&checkpoint, NULL);
	nice(19);
	while(loopContinue)
	{
		counter++;
	}

	printf("Stopped looping...\n");
	return NULL;
}

void looper_thread_start()
{
    pthread_attr_t attr1;
	pthread_attr_setstacksize(&attr1, 256*1024);
	loopContinue = 1;
	pthread_create(&looperId, &attr1,(void*)looper_thread,(void*) NULL);
	pthread_attr_destroy(&attr1);
}

void looper_thread_stop()
{
	struct itimerval interval;	
	memset(&interval, 0, sizeof(interval));
	interval.it_interval.tv_sec = 0;
	interval.it_value.tv_sec = 0;

	if (!loopContinue)
	{
		printf("looper_thread_stop: Looper not started!!\n");
		return;
	}
	
	loopContinue = 0;
	pthread_join(looperId,NULL);	

	if (setitimer(ITIMER_REAL, &interval, NULL)) {
		perror("settimer");
		return;
	}	
}
