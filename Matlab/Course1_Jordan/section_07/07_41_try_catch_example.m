% Try Catch Statement Example

try
    a = notaFunction(5,6);
catch ME
    switch ME.identifier
        case 'MATLAB:UndefinedFunction'
            warning('Function is undefined. Assigning a value of NaN.');
            a = NaN;
        case 'MATLAB:scriptNotAFunction'
            warning(['Attempting tio execute script as a function.'...
                'Running script and assigning output a value of 0.']);
                notaFunction;
                a = 0;
        otherwise 
            rethrow(ME)
    end
end
