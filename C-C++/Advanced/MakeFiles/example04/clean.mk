# Filename : clean.mk.
# Purpose  : clean include Makefile.
# Author   : Sreekanth Sreekumar.
# Date     : 24/08/2020.


clean :
	rm *.o
