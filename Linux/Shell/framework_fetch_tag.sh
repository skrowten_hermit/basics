#!/bin/bash


################################################################################
################################################################################
## File              :: framework_fetch_tag.sh
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 23/01/2019
## Changes made      :: The script for fetching the required git tag from the
##                      Linux VoIP automation branch.
## Changes made Date :: 23/01/2019
## Changes made by   :: Sreekanth S
## Execution example :: ./framework_fetch_tag.sh DVF99_DVF101_AUTO_VER_4.3
################################################################################
################################################################################


arg=$1

echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..33}
echo "VoIP Automation Framework : Tag Fetch"
echo $(printf '=%.0s' {1..100})
echo $'\n'

#Change the working directory, do git commands

cd ~/
git clone git@gitlab.dspg.com:cordless/automation.git --branch voip_automation_linux
git checkout tags/$arg -b voip_automation_linux

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..33}
echo "VoIP Automation Framework : Tag Fetch"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0