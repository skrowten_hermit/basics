% Calculus Problem Examples

% Clean up the environment
clear; close; clc;

% Ordinary Differential Equations (ODE)
tspan = [0 5];
y0 = 1;
[t,y] = ode45(@(t,y) 2*t, tspan, y0);
figure
plot(t,y,'-o')
title('Solution');
