/*******************************************************************************
PROGRAM NAME    :   A074-files-readfilefget.c
TITLE           :   Program to Demonstrate Reading From Files Using fgetc() and
                    fgets().
DATE            :   19/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses functions fgetc() and fgets() to read data
                    from file and display it.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    FILE *pfile = NULL;
    char *filename = "074_readfile.txt";
    char rbuf[100];
    int c;

    pfile = fopen(filename, "r");

    if (pfile == NULL)
    {
        printf("Failed to open %s.\n", filename);
        return(-1);
    }

    printf("Using fgetc():\n\n");

    // Read character by character using fgetc().
    while((c = fgetc(pfile)) != EOF)
        printf("%c", c);

    pfile = fopen(filename, "r");

    printf("\n\nUsing fgets():\n\n");

    // Read character by character using fgets().
    if(fgets(rbuf, 100, pfile) != NULL) // Reads from file (new line not read).
        printf("%s", rbuf);             // Writing content to stdout.

    fclose(pfile);
    pfile = NULL;

    return 0;
}
