frequencyMessage = 50;
amplitude=3;
time = 1:0.001:2;
messageSignal=0.2.*amplitude.*sin(2*pi*frequencyMessage*time);

subplot(3,1,1)
plot(time,messageSignal)
title('Message Signal');

frequencyCarrier = 20;
carrierSignal = amplitude.*sawtooth(2*pi*frequencyCarrier*time);

subplot(3,1,2)
plot(carrierSignal)
title('Carrier Signal');

for i=1:1:length(messageSignal)
   if(messageSignal(i)<carrierSignal(i))
       output(i)=0;
   else
       output(i)=1;
   end
end

subplot(3,1,3)
area(output)
title('Pulse Width Modulated Signal');
