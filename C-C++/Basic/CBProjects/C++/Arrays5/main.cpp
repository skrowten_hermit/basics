/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to find maximum element from an Array.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to calculate the greatest of all
                    the elements stored in an array.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int A[10] = {2, 4, 6, 8, 12, 3, 5, 7, 9, 11};
    int max = INT_MIN;

    for(int i = 0; i <= 10; i++)
    {
        if(A[i] > max)
            max = A[i];
    }

    cout << max << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
