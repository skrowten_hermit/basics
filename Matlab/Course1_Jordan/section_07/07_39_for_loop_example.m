% For Loop Example

% y = mx + b
y = ones(1,100);
m = 4;
b = 3;

for x = 1:100
    y(x) = m*x + b;
end

figure;
plot(y);
title('y = 4x + 3');
xlabel('x');
ylabel('y');
