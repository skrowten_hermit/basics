#include <LPC11xx.h>			/* LPC11xx Peripheral Registers */
#include "system_LPC11xx.h"

#define LED       (1<<9)      /* LED D1 connect to PIO1_9 */

int a;
void delay(int x){
  int i=x, j=x;
  while(j--){
		i=x;
  	while(i--){
			i=i-1;
			//a=i;
			i=i+1;
		}
	}
}

void freqout(int x){
	LPC_TMR16B1 ->TCR |= (1 << 1); //reset
	LPC_TMR16B1 ->TCR &= ~(1 << 0); // stop
	LPC_TMR16B1 ->TCR &= ~(1 << 1); // go
	LPC_TMR16B1 ->MR0 = x/2; // 50% Duty Cycle
	LPC_TMR16B1 ->MR3 = x; // Cycle Length
	LPC_TMR16B1 ->TCR |= (1 << 0); // GO 
}

void init(){
	LPC_SYSCON ->SYSAHBCLKCTRL |= (1 << 8); // Enable Clock for TMR1
	LPC_IOCON ->PIO1_9 |= (1 << 0); // PIN1_9 = CT16B1_MAT0
	LPC_TMR16B1 ->TCR |= (1 << 1); 
	LPC_TMR16B1 ->MR0 = 200; // 50% Duty Cycle
	LPC_TMR16B1 ->PR = 1200;
	LPC_TMR16B1 ->MR3 = 400; // Cycle Length
	LPC_TMR16B1 ->MCR |= (1 << 10); // TC Reset on MR3 Match
	LPC_TMR16B1 ->PWMC |= (1 << 0); // PWM Mode
	LPC_TMR16B1 ->TCR &= ~(1 << 1);
	LPC_TMR16B1 ->TCR |= (1 << 0); // GO 
}
#define TIME 450
int count=0;

void walk(){
	  freqout(364);
		delay(TIME);
	  freqout(343);
		delay(TIME);
	  freqout(324);
		delay(TIME);
	  freqout(243);
		delay(TIME);
	  freqout(0);
		delay(TIME);
	  freqout(364);
		delay(TIME);
	  freqout(343);
		delay(TIME);
	  freqout(324);
		delay(TIME);
	  freqout(243);
		delay(TIME);
	  freqout(0);
		delay(TIME);
	  freqout(485);
		delay(TIME);
	  freqout(0);
		delay(TIME);
}
int main(void){
	init();
	while (1){       // 364  343  324  243  485
	  //freqout(1000);
		walk();
		delay(TIME);
		delay(TIME);
		delay(TIME);
		delay(TIME);
		walk();
	  freqout(243);
		delay(TIME);
		delay(TIME);
		delay(TIME);
		delay(TIME);
	}
	return 0;
}

