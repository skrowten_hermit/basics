/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to find GCD.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to find the Greatest Common Divisor (GCD)
                    of a given 2 numbers.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int m,n;

    cout << "Enter two numbers :" << endl;
    cin >> m >> n;

    while(m != n)
    {
        if(m > n)
            m = m - n;
        else
            n = n - m;
    }

    cout << "GCD is " << m;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
