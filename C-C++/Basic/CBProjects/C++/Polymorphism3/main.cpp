/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Polymorphism.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate polymorphism using a basic
                    example.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


class Base
{
    public:
        virtual void fun1() = 0;

        virtual void fun2() = 0;
};


class Derived :public Base
{
    public:
        void fun1()
        {
            cout << "fun1() of Derived...." << endl;
        }

        void fun2()
        {
            cout << "fun2() of Derived...." << endl;
        }
};


int main()
{
    Derived d;
    d.fun1();
    d.fun2();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
