/*******************************************************************************
PROGRAM NAME    :   A0<last-number>b-hello-func.c
TITLE           :   Program to define a Sub-function in a Sample C Application.
DATE            :   <last-date>
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program defines a sub-function sayhello() as part of a
                    sample C application.
*******************************************************************************/



void sayhello(void)
{
    char str[50];

    printf("Tell me your name : ");
    scanf("%s", str);

    printf("Hello Mr./Ms. %s! Welcome to C++!\n", str);
}
