/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Ellipsis.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate ellipsis (from C++ 11).

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


int sum(int n,...)
{
    va_list list;
    va_start(list, n);

    int x;
    int s = 0;

    for(int i = 0; i < n; i++)
    {
        x = va_arg(list, int);
        s += x;
    }
    return s;
}


int main()
{
    cout << sum(3, 10, 20, 30) << endl;
    cout << sum(5, 1, 2, 3, 4, 5) << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
