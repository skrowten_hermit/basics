/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Find Greatest of 3 Numbers.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to find the greatest of given 3 numbers by
                    the user.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int a, b, c;

    cout << "Enter 3 nos : " << endl;
    cin >> a >> b >> c;

    if(a > b && a > c)
    {
        cout << a << endl;
    }
    else if(b > c)
    {
        cout << b << endl;
    }
    else
    {
        cout << c << endl;
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
