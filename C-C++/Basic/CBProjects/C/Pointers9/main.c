/*******************************************************************************
PROGRAM NAME    :   A058-pointers-ptrstrcpy.c
TITLE           :   Function to Demonstrate String Copy Function Using Arrays
                    and Pointers.
DATE            :   19/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program implements the string copy function - from one
                    string to another string without using string library
                    function in C - using (1) arrays as copyStringArr() and
                    (2) pointers as copyStringPtr().
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/


#include <stdio.h>


void copyStringArr(char from[], char to[]);

void copyStringPtr(char *from, char *to);


int main()
{
    char strSource[] = "A string to be copied.";
    char strDestination[50] = "";

//    copyStringArr(strSource, strDestination);
    copyStringPtr(strSource, strDestination);       // Doesn't work.
    printf("The source is\t\t:\t%s\n", strSource);
    printf("The destination is\t:\t%s\n\n", strDestination);

    return 0;
}


void copyStringArr(char src[], char dst[])
{
    int i;

    printf("The destination was\t:\t%s\n", dst);

    for(i = 0; src[i] != '\0'; i++)
        dst[i] = src[i];

    dst[i] = '\0';

    printf("The source is\t\t:\t%s\n", src);
    printf("The destination is\t:\t%s\n\n", dst);
}


void copyStringPtr(char *src, char *dst)
{
    const char *srcOriginal = src;
    const char *dstOriginal = dst;

    printf("The destination was\t:\t%s\n", dstOriginal);

//    for(; *src != '\0'; src++, dst++)
//        *dst = *src;

    //  The null character is equivalent to 0, so when '\0' is reached, the
    // condition equals to 0 or false and loop is exited.
    while(*src)
        *dst++ = *src++;

    *dst = '\0';
}



/*******************************************************************************

OUTPUT  :
---------

The destination was	:
The source is		:	A string to be copied.
The destination is	:	A string to be copied.


Process returned 0 (0x0)   execution time : 0.006 s
Press ENTER to continue.

*******************************************************************************/
