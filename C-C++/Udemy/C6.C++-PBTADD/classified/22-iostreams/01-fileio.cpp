/*
 * 01-fileio.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include <iostream>
using namespace std;

int main()
{
    ofstream of("Test.txt",ios::trunc);
    of<<"John"<<endl;
    of<<25<<endl;
    of<<"CS"<<endl;
    of.close();


    ifstream ifs("Test.txt");
    string name;
    int roll;
    string branch;
    ifs>>name>>roll>>branch;
    cout<<name<<endl<<roll<<endl<<branch<<endl;
    ifs.close();
}
