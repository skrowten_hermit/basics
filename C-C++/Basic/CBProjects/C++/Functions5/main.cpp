/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Return By Value/Reference/Address.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate Static

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


// Return by value.

int fun(int a)
{
    cout << a;
//    a += 1
    return a + 1;
}


// Return by reference.

int &fun(int &a)
{
    cout << a;
    return a;
}


// Return by address.

int *fun(int n)
{
    int *p = new int[n];

    for(int i = 0; i < n; i++)
        p[i] = i + 1;

    return p;
}


int main()
{
    //by value
    int x = 10, y = 0;
    y = fun(x);
    cout << y;

    //by reference
    int x = 10;
    fun(x) = 25;
    cout << x;

    //by address
    int *ptr = fun(5);
    for(int i = 0; i < 5; i++)
        cout << i << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
