/*******************************************************************************
PROGRAM NAME    :   A033-func-gcd-sqrt.c
TITLE           :   Program to Calculate the GCD, Absolute Value and Square Root
                    Using Functions. [ERROR]
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program calculates the GCD and Square Root using the
                    defined functions gcd() and sqRoot().
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int gcd(int u, int v);

float absVal(float w);

float sqRoot(float t);


int main()
{
    int gcdResult = 0;
    float absvalResult = 0, sqrtResult = 0;

    float f1 = -15.5, f2 = 20, f3 = -5.0;
    int i1 = -716;

    gcdResult = gcd(150, 35);
    printf("The GCD of 150 and 35 is %d\n", gcdResult);

    gcdResult = gcd(1026, 405);
    printf("The GCD of 1026 and 405 is %d\n", gcdResult);

    printf("The GCD of 83 and 240 is %d\n", gcd(83, 240));

    absvalResult = absVal(f1);
    printf("f1 = %.2f\n", f1);
    printf("absVal(f1) = %.2f\n", absvalResult);

    absvalResult = absVal(f2);
    printf("f2 = %.2f\n", f2);
    printf("absVal(f2) = %.2f\n", absvalResult);

    absvalResult = absVal(f3);
    printf("f3 = %.2f\n", f3);
    printf("absVal(f3) = %.2f\n", absvalResult);

    absvalResult = absVal(i1);
    printf("i1 = %d\n", i1);
    printf("absVal(i1) = %.2f\n", absvalResult);

    printf("absVal(-6.0) / 4 = %.2f\n", absVal(-6.0) / 4);

    printf("squareroot(165.0) = %.2f\n", sqRoot(165.0));
    printf("squareroot(-3.0) = %.2f\n", sqRoot(-3.0));
    printf("squareroot(16.0) = %.2f\n", sqRoot(16.0));
    printf("squareroot(525.0) = %.2f\n", sqRoot(525.0));
    printf("squareroot(9.0) = %.2f\n", sqRoot(9.0));

    return 0;
}


int gcd(int x, int y)
{
    int temp;

    while(y != 0)
    {
        temp = x % y;
        x = y;
        y = temp;
    }

    return x;
}


float absVal(float x)
{
    if (x < 0)
        x = -x;

    return x;
}


//float sqRoot(float x)
//{
//    const float epsilon = 0.00001;
//    float guess = 1.0;
//
//    if (x < 0)
//    {
//        printf("Negative arguments passed for square root!\n");
//        return -1.0;
//    }
//
//    while(absVal(guess * guess - x) >= epsilon)
//        guess = (x / guess + guess) / 2.0;
//
//    return guess;
//}


float sqRoot(float x)
{
    const float epsilon = 0.00001;
    float guess = 1.0;
    float retVal = 0.0;

    if (x < 0)
    {
        printf("Negative arguments passed for square root!\n");
        retVal = -1.0;
    }
    else
    {
        while(absVal(guess * guess - x) >= epsilon)
            guess = (x / guess + guess) / 2.0;

        retVal = guess;
    }

    return retVal;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
