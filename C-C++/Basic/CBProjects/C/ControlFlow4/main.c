/*******************************************************************************
PROGRAM NAME    :   A016-switch-case-days.c
TITLE           :   Program to Demonstrate Switch Case Construct.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the use of conditional operator
                    to determine the day of the week.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    enum Weekday {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday};
    enum Weekday today = Monday;

    switch(today)
    {
        case Sunday:
            printf("Today is Sunday!\n");
            break;
        case Monday:
            printf("Today is Monday!\n");
            break;
        case Tuesday:
            printf("Today is Tuesday!\n");
            break;
        case Wednesday:
            printf("Today is Wednesday!\n");
            break;
        case Thursday:
            printf("Today is Thursday!\n");
            break;
        case Friday:
            printf("Today is Friday!\n");
            break;
        case Saturday:
            printf("Today is Saturday!\n");
            break;
        default:
            printf("Whatever!!!\n");
            break;
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Today is Monday!

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
