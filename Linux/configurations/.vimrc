" To keep 1000 commands in history.
set history=1000

" Show the cursor position.
set ruler

" Show incomplete commands.
set showcmd

" Shows menu for command's tab completion.
set wildmenu

" Keep some context above current text.
set scrolloff=5

set hlsearch
set incsearch
set ignorecase

" Asks vim to override ignorecase option if search contains uppercase.
set smartcase

" Automatically wrap text that extends beyond the screen length.
set wrap

" Vim's auto indentation feature does not work properly with text copied from outside of Vim. Press the <F12> key to toggle paste mode on/off.
nnoremap <F12> :set invpaste paste?<CR>
imap <F12> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F12>

"set textwidth=80             " set the MAX text width.
"set formatoptions+=t

" SPACE and TAB substitutions.
"set tabstop=4                              " number of visual spaces per TAB.
"set shiftwidth=4
"set softtabstop=4                          " number of spaces in TAB when editing.
"set expandtab                              " TABs are spaces.
"set noshiftround

" Simpler TAB substitution.
nnoremap <Tab> 4i<Space><Esc>$
inoremap <Tab> <Esc>4i<Space><Esc>$i

" Turns on line numbers.
set number

" Creates backup copy of current file that's being edited.
set backup
set bex=bak " Backup extention as .bak.

" Wraps words on a whole for better readability.
"set lbr

" Source code assists.
set ai    " Auto-indentation.
set si    " Smart-indentation.

" Visual settings.
"set bg=dark
"color slate

" Map keys (mostly single function keys)
"map <F2> i/<Esc>79a*<Esc>oPROGRAM NAME    :<Esc>oTITLE           :<Esc>oDATE            :<Esc>oAUTHOR          :<Esc>oDESCRIPTION     :<Esc>o<Esc>79a*<Esc>i/<Esc>o
map <F2> :call CstyleHeader()<CR>
map <F3> :call SstyleHeader()<CR> 
map <F4> <Esc>i#include<><Esc>i
map <F5> <Esc>iint main()<Esc>o{<Esc>o<Esc>4i<Space><Esc>$o}<CR>

" Compile and apply configuration changes to vimrc.
" source <path_to_vimrc>

" To make a vimrc out of the current file or custom file.
" :mkvimrc! :mkvimrc <path_to_vimrc>

func! CstyleHeader()
    let stars = repeat('*', 79)
    let line1 = 'PROGRAM NAME    :   '
    let line2 = 'TITLE           :   '
    let line3 = 'VERSION         :   '
    let line4 = 'DATE WRITTEN    :   '
    let line5 = 'AUTHOR          :   '
    let line6 = 'DATE MODIFIED   :   '
    let line7 = 'MODIFIED BY     :   '
    let line8 = 'CHANGES MADE    :   '
    let line9 = 'DESCRIPTION     :   '
    let lines = ['/' . stars, line1, line2, line3, line4, line5, line6, line7, line8, line9, stars . '/']
    call append(0, lines)
endfunc

func! SstyleHeader()
    let hashes = repeat('#', 80)
    let line1 = '## PROGRAM NAME  :   '
    let line2 = '## TITLE         :   '
    let line3 = '## VERSION       :   '
    let line4 = '## DATE WRITTEN  :   '
    let line5 = '## AUTHOR        :   '
    let line6 = '## DATE MODIFIED :   '
    let line7 = '## MODIFIED BY   :   '
    let line8 = '## CHANGES MADE  :   '
    let line9 = '## DESCRIPTION   :   '
    let lines = [hashes, hashes, line1, line2, line3, line4, line5, line6, line7, line8, line9, hashes, hashes]
    call append(0, lines)
endfunc
