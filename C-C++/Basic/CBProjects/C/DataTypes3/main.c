/*******************************************************************************
PROGRAM NAME    :   A004-formatting-data.c
TITLE           :   Program to Demonstrate the Different Format Specifiers.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the different format specifiers
                    that can be applied on data and types.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    int iVar = 100;
    float fVar = 331.79;
    double dVar = 8.44e+11;
    char cVar = 'W';
    bool bVar = 0;

    printf("Integer (type 1) = %i\n", iVar);
    printf("Integer (type 2) = %d\n", iVar);
    printf("Float (type 1) = %f\n", fVar);
    printf("Float (type 2) = %.2f\n", fVar);
    printf("Double (type 1) = %e\n", dVar);
    printf("Double (type 2) = %g\n", dVar);
    printf("Char = %c\n", cVar);
    printf("Bool = %u\n", bVar);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Integer (type 1) = 100
Integer (type 2) = 100
Float (type 1) = 331.790009
Float (type 2) = 331.79
Double (type 1) = 8.440000e+11
Double (type 2) = 8.44e+11
Char = W
Bool = 0

Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
