/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to demonstrate serialization.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate serialization

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Student
{
    private:
        string name;
        int roll;
        string branch;

    public:
        Student(){}

        Student(string n, int r, string b)
        {
            name = n;
            roll = r;
            branch = b;
        }

        friend ofstream & operator <<(ofstream &ofs, Student s);

        friend ifstream & operator >>(ifstream &ifs, Student &s);

        friend ostream & operator <<(ostream &os, Student &s);
};


ofstream & operator <<(ofstream &ofs, Student s)
{
    ofs << s.name << endl;
    ofs << s.roll << endl;
    ofs << s.branch << endl;

    return ofs;
}


ifstream & operator >>(ifstream &ifs, Student &s)
{
    ifs >> s.name;
    ifs >> s.roll;
    ifs >> s.branch;

    return ifs;
}


ostream & operator <<(ostream &os, Student &s)
{
    os << "Name : " << s.name << endl;
    os << "Roll : " << s.roll << endl;
    os << "Branch : " << s.branch << endl;

    return os;
}


int main()
{
    ofstream ofs("Test.txt");
    Student s1("John", 10, "CS");
    ofs << s1;
    ofs.close();

    Student s2;
    ifstream ifs("Test.txt");
    ifs >> s1;
    cout << s1;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
