#!/bin/bash


##########################################################################################################################################
##########################################################################################################################################
## File              :: SDK_sanity.sh
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 31/08/2018
## Changes made      :: The SDK script for running any of the suites in batch mode.
## Changes made Date :: 14/09/2018
## Changes made by   :: Sreekanth S
## Execution example ::
## ./SDK_batch.sh sanity-A,config_master_1.txt/performance-B,config_master_2.txt/usanity-C,config_master_3.txt,config_user_1.txt/sanity-D
##########################################################################################################################################
##########################################################################################################################################


echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..40}
echo "Batch Execution Suite"
echo $(printf '=%.0s' {1..100})
echo $'\n'

arg=$1
#echo "Input : $arg"
#echo

if [[ "$arg" == @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    echo "Help info for execution requested...."
    echo $'\n\n'
    echo $(printf '~%.0s' {1..100})
    printf ' %.0s' {1..43}
    echo "Help Info"
    echo $(printf '~%.0s' {1..100})
    echo $'\n\n'
    echo "Usage Description : " | boxes
    echo
    echo -e "./SDK_batch: \n \n executes sanity followed by performance automation using the default configuration files." | boxes -d shell -s 100
    echo
    echo -e "./SDK_batch <test>/<test>/.../<test>: \n \n executes tests one after the other after the '/' using the default configuration files. \n \n eg:- \t ./SDK_batch sanity/bsp/performance/voip-control." | boxes -d shell -s 100
    echo
    echo -e "./SDK_batch <test>-<configuration>/<test>-<configuration>/.../<test>-<configuration>: \n \n executes tests one after the other after the '/' using the configuration files (if required). \n \n eg:- \t (1)./SDK_batch sanity-config_master_1.txt,config_shunra.txt/bsp-config_master_3.txt. \n \t (2)./SDK_batch performance-config_master_2.txt,config_user_1.txt/cmbs." | boxes -d shell -s 100
    echo
    echo -e "Note: \n \n<test> can take the following values (case sensitive) : \n sanity, usanity, performance, voipctrl, interop, bsp, cmbs \n \n<configuration> can take the values (case sensitive) : \n list of configuration files (input file names) separated by a ','. \n \nThe types of configuration files and the naming formats are:\n config_master_<something>\t-->\t master configuration file. \n config_user_<something>\t-->\t user defined test cases configuration file. \n config_shunra_<something>\t-->\t shunra configuration file. \n config_att_<something>\t\t-->\t audiotune settings configuration file." | boxes -d shell -s 100
    echo $'\n\n'
    echo $(printf '~%.0s' {1..100})
    printf ' %.0s' {1..43}
    echo "Help Info"
    echo $(printf '~%.0s' {1..100})
    echo $'\n\n'
    echo "Help info printed...."
elif [[ "$arg" != @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    if [ "$arg" == "" ]; then
        echo "User has not specified any input, executing the default batch execution suite...."
        echo "Executing sanity suite first...."
        echo $'\n\n'
        echo $(printf '=%.0s' {1..100})
        printf ' %.0s' {1..44}
        echo "Sanity Suite"
        echo $(printf '=%.0s' {1..100})
        echo $'\n'
        ./SDK_sanity.sh
        echo "Executing performance suite now...."
        echo $'\n\n'
        echo $(printf '=%.0s' {1..100})
        printf ' %.0s' {1..41}
        echo "Performance Suite"
        echo $(printf '=%.0s' {1..100})
        ./SDK_performance.sh
    else
        echo "Executing a custom batch execution suite...."
        IFS='/' read -ra test_list <<<"$arg"
        for i in "${test_list[@]}"; do
            IFS='-' read -r _ conf_list <<<"$i"
            IFS='-' read -r test_type _ <<<"$i"
            IFS=',' read -ra clist <<<"$conf_list"
            if [ $test_type == "sanity" ]; then
                ./SDK_sanity.sh "${conf_list[@]}"
            elif [ $test_type == "usanity" ]; then
                ./SDK_user-sanity.sh "${conf_list[@]}"
            elif [ $test_type == "performance" ]; then
                ./SDK_performance.sh "${conf_list[@]}"
            elif [ $test_type == "voipctrl" ]; then
                ./SDK_voip-control.sh "${conf_list[@]}"
            elif [ $test_type == "'interop" ]; then
                ./SDK_interop.sh "${conf_list[@]}"
            elif [ $test_type == "bsp" ]; then
                ./SDK_bsp.sh "${conf_list[@]}"
            elif [ $test_type == "cmbs" ]; then
                ./SDK_cmbs.sh "${conf_list[@]}"
            elif [ $test_type == "dvf1100" ]; then
                ./SDK_dvf1100.sh "${conf_list[@]}"
            fi
        done
    fi
fi

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..40}
echo "Batch Execution Suite"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0