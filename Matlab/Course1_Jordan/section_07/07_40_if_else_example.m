% If Else Statement Example

limit = 0.75;
over_limits = 0;

A = rand(10,1)

if any(A > limit)
    disp('There is at least one value above the limit.');
else
    disp('All values are below the limit');
end

for n = 1:10
    if(A(n) > limit)
        over_limits = over_limits + 1;
    end
end

str = sprintf('There were %d values above the limit.', over_limits);
disp(str);
