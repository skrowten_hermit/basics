/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Functions.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate function declaration and
                    usage.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


void display()
{
    cout << "Hello!" << endl;
}


float add(float x, float y)
{
    float z;
    z = x + y;
    return z;
}


int maxim(int a, int b, int c)
{
    if(a > b && a > c)
        return a;
    else if(b > c)
        return b;
    else
        return c;
}


int main()
{
    display();

    float x = 2.3, y = 7.9, z;
    z = add(x, y);
    cout << x << " + " << y << " = " << z << endl;

    int a, b, c, d;
    cout << "Enter 3 nos : " << endl;
    cin >> a >> b >> c;
    d = maxim(a, b, c);
    cout << "Maximum among " << a << ", " << b << ", " << c << " is : " << d << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
