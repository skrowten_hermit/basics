/*******************************************************************************
PROGRAM NAME    :   A064-memalloc-dynstrex.c
TITLE           :   Program to Demonstrate Run-time Allocation of String Size. [ERROR]
DATE            :   24/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to allocate memory using the
                    malloc() and realloc() functions for strings during run-time
                    by sking the user for the size of the string. This shows how
                    memory can be saved with character pointers when compared to
                    character arrays where memory allocation is static.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int size;
    char *text = NULL;

    printf("Enter limit of the text : ");
    scanf("%d", &size);

    text = (char *) malloc(size * sizeof(char));
    printf("(Initialized) text : %s\n", text);
    printf("Size(text) : %d\n", sizeof(text));

//    text = (char *) calloc(size, sizeof(char));
//    printf("text = %s\n", text);

    if (text != NULL)
    {
        printf("\nEnter some text : ");
        scanf(" ");
        gets(text);

        printf("\nInput text : %s\n", text);
        printf("Size(text) : %d\n", sizeof(text));
    }

    free(text);
    text = NULL;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter limit of the text : 25
(Initialized) text :
Size(text) : 8

Enter some text : Anthony

Input text : Anthony
Size(text) : 8

Process returned 0 (0x0)   execution time : 20.342 s
Press ENTER to continue.

*******************************************************************************/
