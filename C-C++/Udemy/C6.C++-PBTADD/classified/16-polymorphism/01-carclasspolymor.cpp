/*
 * 01-carclasspolymor.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include <iostream>
using namespace std;

class Car
{
    public:
        virtual void start()=0;
};

class Innova:public Car
{
    public:
        void start(){cout<<"Innova Started"<<endl;}
};

class Swift:public Car
{
    public:
        void start(){cout<<"Swift Started"<<endl;}
};

int main()
{
    //Car c;
    Car *p=new Innova();
    p->start();
    p=new Swift();
    p->start();
}
