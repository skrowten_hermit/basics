/*******************************************************************************
PROGRAM NAME    :   A009-bitwise-operations.c
TITLE           :   Program to Demonstrate Bitwise Operations.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates different bitwise operations
                    available in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    unsigned int a = 60;    // 0011 1100
    unsigned int b = 13;    // 0000 1101
    int result;

//    result = a & b;         // 0000 1100
    result = a | b;         // 0011 1101
    result = a ^ b;         // 0011 1101
    result = ~a;            // 0011 1101
    result = a >> 2;        // 0011 1101
    result = a << 2;        // 0011 1101

    printf("Result : %d\n", result);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Result : 240

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
