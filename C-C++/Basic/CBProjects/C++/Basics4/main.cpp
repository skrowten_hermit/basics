/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Compound Arithmetic Operator.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate the usage of a compound
                    arithmetic operator.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int sum = 10, x = 5;

    sum += x;
    cout << sum << endl;

    int fact = 10, y = 5;

    fact *= y;
    cout << fact << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
