/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Ways Of Inheritance.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate different ways of
                    inheritance.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Parent
{
    private: int a;

    protected: int b;

    public:
        int c;

        void funParent()
        {
            a = 10;
            b = 5;
            c = 15;
        }
};


class Child: private Parent
{
    private:

    protected:

    public:
    void funChild()
    {
//        a = 10;
        b = 5;
        c = 15;
    }
};


class GrandChild : public Child
{
    public:
        void funGrandChild()
        {
//            a = 10;
//            b = 5;
//            c = 20;
        }
};


int main()
{
    Child c;
    c.a = 10;
    c.b = 5;
    c.c = 20;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
