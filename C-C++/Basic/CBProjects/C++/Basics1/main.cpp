/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Calculate the Area of a Circle.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to calculate the area of a circle by
                    taking the radius as an input from the user. Then an output
                    shows the calculated area.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


int main()
{
    float radius;
    float area;

    cout << "Enter Radius of a Circle : ";
    cin >> radius;

    area = 3.1425 * radius * radius;

    cout << "Area of a Circle is " << area << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
