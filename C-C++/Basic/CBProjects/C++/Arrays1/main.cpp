/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Find Average of Numbers.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to find the avearage of numbers
                    stored in an array. THe input is taken from the user and
                    is used for calculating the average.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int n, i;
    float num[100], sum =0.0, average;

    cout << "Enter the number of inputs : ";
    cin >> n;

    for(i = 0; i < n; ++i)
    {
        cout << i + 1 << ". Enter number: ";
        cin >> num[i];
        sum += num[i];
    }

    average = sum / n;
    cout << "Average = " << average;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
