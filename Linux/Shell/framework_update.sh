#!/bin/bash


################################################################################
################################################################################
## File              :: framework_update.sh
## Description       :: Fetches the latest automation framework files from Git.
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 23/01/2019
## Changes made      :: The script for fetching the latest tag from the Linux
##                      VoIP automation branch. Works as an update script.
## Changes made Date :: 23/01/2019
## Changes made by   :: Sreekanth S
## Execution example :: ./framework_update.sh
################################################################################
################################################################################


echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..29}
echo "VoIP Automation Framework : Update Version"
echo $(printf '=%.0s' {1..100})
echo $'\n'

#Change the working directory, do git commands

arg=$1
#echo "Input : $arg"
#echo

if [[ "$arg" == @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    echo "Help info for execution requested...."
    echo $'\n\n'
    echo $(printf '~%.0s' {1..120})
    printf ' %.0s' {1..53}
    echo "Help Info"
    echo $(printf '~%.0s' {1..120})
    echo $'\n\n'
    echo "Usage Description : " | boxes
    echo
    echo -e "./framework_update.sh: \n \n fetches the latest automation framework files from Git." | boxes -d shell -s 120
    echo
    echo $'\n\n'
    echo $(printf '~%.0s' {1..120})
    printf ' %.0s' {1..53}
    echo "Help Info"
    echo $(printf '~%.0s' {1..120})
    echo $'\n\n'
    echo "Help info printed...."
elif [[ "$arg" != @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    cd ~/
    mv automation Documents/
    git clone git@gitlab.dspg.com:cordless/automation.git --branch voip_automation_linux
fi

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..29}
echo "VoIP Automation Framework : Update Version"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0