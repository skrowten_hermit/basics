/*******************************************************************************
PROGRAM NAME    :   A014-check-sign.c
TITLE           :   Program to Check if a Given Number is +ve or -ve.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program accepts a number from the user as input and
                    checks/determines if it is a positive number or a negative
                    number.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main(void)
{
    int num, sign;

    printf("Enter The Number To Be Tested :\n");
    scanf("%i", &num);

    if (num < 0)
        sign = -1;
    else if (num == 0)
        sign = 0;
    else
        sign = 1;

    printf("\nSign of %d = %i\n", num, sign);
    printf("\nNote:\n------\n");
    printf("-1 = Negative Number\n 0 = Zero\n 1 = Positive Number\n");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Run(1):

Enter The Number To Be Tested :
91

Sign of 91 = 1

Note:
------
-1 = Negative Number
 0 = Zero
 1 = Positive Number

Process returned 0 (0x0)   execution time : 7.627 s
Press ENTER to continue.


Run(2):

Enter The Number To Be Tested :
-10258

Sign of -10258 = -1

Note:
------
-1 = Negative Number
 0 = Zero
 1 = Positive Number

Process returned 0 (0x0)   execution time : 9.433 s
Press ENTER to continue.


Run(3):

Enter The Number To Be Tested :
0

Sign of 0 = 0

Note:
------
-1 = Negative Number
 0 = Zero
 1 = Positive Number

Process returned 0 (0x0)   execution time : 3.340 s
Press ENTER to continue.


Run(4):

Enter The Number To Be Tested :
-0

Sign of 0 = 0

Note:
------
-1 = Negative Number
 0 = Zero
 1 = Positive Number

Process returned 0 (0x0)   execution time : 2.305 s
Press ENTER to continue.

*******************************************************************************/
