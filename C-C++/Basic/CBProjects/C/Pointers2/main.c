/*******************************************************************************
PROGRAM NAME    :   A051-pointers-basicarith.c
TITLE           :   Program to Demonstrate Arithmetic Operations Using Pointers.
DATE            :   15/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates hoe to use pointers in basic
                    arithmetic operations.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    long num1 = 0L, num2 = 0L;
    long *pnum = NULL;

    printf("num1 = %ld\n&num1 = %ld\nnum2 = %ld\n&num2 = %ld\n", num1, &num1,
            num2, &num2);
    printf("*pnum = %ld\n&pnum = %ld\n\n", num1, &num1, num2, &num2);

    pnum = &num1;       // Get the address of num1.
    *pnum = 2L;         // Set num1 to 2.
    ++num2;             // Increment num2.
    num2 += *pnum;      // Add num1 to num2.

    printf("num1 = %ld\n&num1 = %ld\nnum2 = %ld\n&num2 = %ld\n", num1, &num1,
            num2, &num2);
    printf("*pnum = %ld\n&pnum = %ld\n\n", num1, &num1, num2, &num2);
    printf("*pnum + num2 = %ld\n\n", *pnum + num2);

    pnum = &num2;       // Get address of num2.
    ++*pnum;            // Increment num2 indirectly.

    printf("num1 = %ld\n&num1 = %ld\nnum2 = %ld\n&num2 = %ld\n", num1, &num1,
            num2, &num2);
    printf("*pnum = %ld\n&pnum = %ld\n\n", num1, &num1, num2, &num2);
    printf("*pnum + num2 = %ld\n\n", *pnum + num2);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

num1 = 0
&num1 = 140723246702896
num2 = 0
&num2 = 140723246702904
*pnum = 0
&pnum = 140723246702896

num1 = 2
&num1 = 140723246702896
num2 = 3
&num2 = 140723246702904
*pnum = 2
&pnum = 140723246702896

*pnum + num2 = 5

num1 = 2
&num1 = 140723246702896
num2 = 4
&num2 = 140723246702904
*pnum = 2
&pnum = 140723246702896

*pnum + num2 = 8


Process returned 0 (0x0)   execution time : 0.008 s
Press ENTER to continue.

*******************************************************************************/
