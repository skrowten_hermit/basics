/*******************************************************************************
PROGRAM NAME    :   A067-struct-nestedstruct.c
TITLE           :   Program to Demonstrate a Nested Structure.
DATE            :   09/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create, initialize and use
                    a structure containing other structures as members i.e., a
                    nested structure.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    struct date
    {
        int day;
        char month[5];
        int year;
    };

    struct time
    {
        int hours;
        int minutes;
        int seconds;
    };

    struct timeStamp
    {
        struct date sdate;
        struct time stime;
    };

    struct event
    {
        struct dob
        {
            int day;
            char month[5];
            int year;
        }sdob;

        struct time stime;
    };

    struct timeStamp e1_ts;

    e1_ts.sdate.day = 23;
    e1_ts.sdate.month = "Sep";
    e1_ts.sdate.year = 1989;
    e1_ts.stime.hours = 3;
    e1_ts.stime.minutes = 14;
    e1_ts.stime.seconds = 50;

    struct timeStamp e2_ts = {{31, "May", 2011}, {10, 0, 1}};
    struct timeStamp e2_ts = {{.day = 31, .month = "May", .year = 2011},
                                {.hours = 10, .minutes = 0, .seconds = 1}};

    printf("\n");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
