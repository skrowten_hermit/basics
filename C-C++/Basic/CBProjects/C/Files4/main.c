/*******************************************************************************
PROGRAM NAME    :   A077-files-fpostn.c
TITLE           :   Program to Demonstrate File Positions and their Manipulation
                    in a file.
DATE            :   19/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses functions fseek() and ftell() to write data to a file
                    and read strings and integers.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <fcntl.h>


void getfcontents(FILE *filepointer);

void getposcontents(FILE *fptr, fpos_t pos);


int main()
{
    FILE *fp;
    fpos_t postn, cpostn;
    int len;

    fp = fopen("077_posfile.txt", "w+");

    fgetpos(fp, &postn);
    printf("1. File Opened.\nCurrent Position = %d", postn);
    getfcontents(fp);
    getposcontents(fp, postn);

    if (fp == NULL)
    {
        printf("Error!!! Cannot Open File.");
        return(-1);
    }

    fputs("It is just blah, blah, blah!!!! Always.", fp);
    fgetpos(fp, &postn);
    printf("\n\n2. File Written to.\nCurrent Position = %d", postn);
    getfcontents(fp);
    getposcontents(fp, postn);

    fseek(fp, 0, SEEK_END);
    fgetpos(fp, &postn);
    printf("\n\n3. Reached the End.\nCurrent Position = %d", postn);
    getfcontents(fp);
    getposcontents(fp, postn);

    fseek(fp, 10, SEEK_SET);
    fgetpos(fp, &postn);
    printf("\n\n4. Moved to position.\nCurrent Position = %d", postn);
    getfcontents(fp);
    getposcontents(fp, postn);
//    fseek(fp, 10, SEEK_SET);
    fsetpos(fp, &postn);
    fputs(" always ", fp);
    fgetpos(fp, &postn);
    printf("\n\n5. Inserted at position.\nCurrent Position = %d", postn);
    getfcontents(fp);

    len = ftell(fp);
    fclose(fp);

    // Trying to get the filename (as if we didn't know.
//    printf("Total size of %s = %d bytes.", fcntl(fp, F_GETPATH));
    printf("\n\nTotal size of \"077_posfile.txt\" = %d bytes.\n", len);

    return 0;
}


void getfcontents(FILE *fptr)
{
    int c;

    fseek(fptr, 0, SEEK_SET);

    printf("\nFile Contents:\n");

    if(fptr != NULL) // Reads from file.
        while((c = fgetc(fptr)) != EOF)
            printf("%c", c);// Writing content to stdout.
    else
        printf("\nError!!! File NOT read.\n");
}


void getposcontents(FILE *fptr, const fpos_t pos)
{
    int c, cpos = 0;
    char buf[100];

    cpos = (int) pos.__pos;

    printf("cpos=%d\n", cpos);
    printf("\nFile Contents Before Position %d :\n", pos);

    fseek(fptr, 0, SEEK_SET);

    memset(buf, 0, 100);
    fread(buf, 1, cpos, fptr);
    printf("%s\n", buf);

//    if(fptr != NULL) // Reads from file.
//        while((c = fgetc(fptr)) < cpos)
//        {
//            printf("%c", c);// Writing content to stdout.
//        }
//    else
//        printf("\nError!!! File NOT read.\n");

    fseek(fptr, cpos, SEEK_SET);

    printf("\nFile Contents After Position %d :\n", pos);

    if(fptr != NULL) // Reads from file.
        while((c = fgetc(fptr)) != EOF)
            printf("%c", c);// Writing content to stdout.
    else
        printf("\nError!!! File NOT read.\n");
}
