/*
 * 01-simplfunc.cpp
 * Example of Simple Function
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */




#include <iostream>
using namespace std;
void display()
{
    cout<<"Hello";
}

int main()
{
    display();
    return 0;
}
