/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate basic Inheritance Class.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate basic class inheritance'

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Base
{
    public:
        int a;
        void display()
        {
            cout << "Display (from Base) " << a << endl;
        }
};


class Derived:public Base
{
    public:
        void show()
        {
            cout << "Show (from Derived) " << a << endl;
        }
};


int main()
{
    Derived d;
    d.a = 100;
    d.display();
    d.show();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
