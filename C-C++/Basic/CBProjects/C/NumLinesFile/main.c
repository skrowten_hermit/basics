/*******************************************************************************
PROGRAM NAME    :   A078-files-numlines.c
TITLE           :   Program to Count the Number of Lines in a File.
DATE            :   20/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses file function fgetc() to count the number
                    of lines in a file.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define FILENAME "sample.txt"


int main()
{
    FILE *fp = NULL;
    char ch;
    int linesCount = 0;

    fp = fopen(FILENAME, "r");

    if(fp == NULL)
    {
        printf("File Does Not Exist!!!\n");
        return -1;
    }

    while((ch = fgetc(fp)) != EOF)
    {
        if(ch == '\n')
            linesCount++;
    }

    fclose(fp);
    fp = NULL;

    printf("\nTotal Number of Lines in the file \"%s\" : %d\n\n", FILENAME, linesCount);

    return 0;
}
