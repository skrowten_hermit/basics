/*******************************************************************************
PROGRAM NAME    :   A003-enum-datatype.c
TITLE           :   Program to Demonstrate 'enum' Types.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the 'enum' types using a gender
                    example.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    enum gender {male, female};
    enum gender myGender, herGender;
    char myChar = 's';
    char hisChar = 65;
    char oneChar = '\n';
//    myGender = "44"; //error
    myGender = male;
    herGender = female;
    bool isMale = (myGender == herGender);
    printf("Are we same gender ('1' for 'True' and '0' for 'False')?\n%u\n",
            isMale);
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Are we same gender ('1' for 'True' and '0' for 'False')?
0

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
