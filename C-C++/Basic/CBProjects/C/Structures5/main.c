/*******************************************************************************
PROGRAM NAME    :   A069-struct-structptrs.c
TITLE           :   Program to Demonstrate a Structure Pointer and A Structure
                    with Pointer Members.
DATE            :   10/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create a pointer from a
                    structure type, initialize and access them and manipulate
                    them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>


// Typically, structures are defined outside a function.

struct date
{
    int day;
    int month;
    int year;
};

struct intPtrs
{
    int *p1;
    int *p2;
};


int main()
{
    struct date today, *datePtr;

    datePtr = &today;

    datePtr -> month = 9;
    datePtr -> day = 23;
    datePtr -> year = 1989;

//    (*datePtr).month = 9;
//    (*datePtr).day = 23;
//    (*datePtr).year = 1989;

    printf("Birthday is on : %i/%i/%.2i\n", datePtr -> day, datePtr -> month,
            datePtr -> year);

//    printf("Birthday is on : %i/%i/%.2i\n", (*datePtr).day, (*datePtr).month,
//            (*datePtr).year);

////////////////////////////////////////////////////////////////////////////////

    struct intPtrs iPtr;
    int i1 = 100, i2;

    iPtr.p1 = &i1;
    iPtr.p2 = &i2;
    *iPtr.p2 = -97;

    printf("i1 = %i, *iPtr.p1 = %i\n", i1, *iPtr.p1);
    printf("i2 = %i, *iPtr.p2 = %i\n", i2, *iPtr.p2);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
