/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Short Circuit.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to demonstrate short circuit.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int a = 10, b = 5, i = 5;

    if(a > b && ++i <= b)
    {
        cout << i << endl;
    }
    if(a < b || ++i <= b)
    {
        cout << i << endl;
    }
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
