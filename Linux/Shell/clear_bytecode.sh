#!/bin/bash


################################################################################
################################################################################
## File              :: clear_bytecode.sh
## Description       :: Looks for .pyc (bytecode) files and removes them from
##                      the working directory.
## Developer         :: Sreekanth S
## Version           :: v1.0
## Release Date      :: 23/01/2019
## Changes made      :: The script for fetching the latest tag from the Linux
##                      VoIP automation branch. Works as an update script.
## Changes made Date :: 23/01/2019
## Changes made by   :: Sreekanth S
## Execution example :: ./clear_bytecode.sh
################################################################################
################################################################################


echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..23}
echo "VoIP Automation Framework : Clear Python Bytecode(s)"
echo $(printf '=%.0s' {1..100})
echo $'\n'

#Change the working directory, do git commands
cd ../../source/
#echo $PWD

find . -name "*.pyc" -exec rm -f {} \;
echo $'\n'
echo "Bytecode removed...."
echo $'\n'

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..23}
echo "VoIP Automation Framework : Clear Python Bytecode(s)"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0