/*******************************************************************************
PROGRAM NAME    :   A068-struct-nestedstructarr.c
TITLE           :   Program to Demonstrate a Nested Structure Array.
DATE            :   09/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create an array of nested
                    structures, initialize and access them and manipulate them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    struct timeStamp
    {
        struct date
        {
            int day;
            char month[5];
            int year;
        }sdate;

        struct time
        {
            int hours;
            int minutes;
            int seconds;
        }stime;
    };

    printf("\n");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
