/*******************************************************************************
PROGRAM NAME    :   A008-logic-operations.c
TITLE           :   Program to Demonstrate Logical Operations.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates different logical operations
                    available in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    bool a = true;
    bool b = false;
    bool result;

    result = a && b;
    result = a || b;

    printf("Result : %d\n", result);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Result : 1

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
