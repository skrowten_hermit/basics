/*******************************************************************************
PROGRAM NAME    :   A056-pointers-ptrarith.c
TITLE           :   Program to Calculate the Sum of Values in an Array Using
                    Pointer Arithmetic.
DATE            :   18/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program shows

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int arraySum(int array[], const int num);

void validPtrArith(void);

// Is the same as above. The array can be replaced by a pointer.
int arraySumPtr(int *array, const int num);


int main()
{
    int values[10] = {3, 7, -9, 3, 6, -1, 7, 9, 1, -5};
    int array[6] = {1, 2, 3, 4, 5, 10};
    int *parr = &array[1];

    printf("\nThe sum of values = %d\n\n", arraySum(values, 10));
    printf("\nThe sum of values = %d\n\n", arraySum(array, 6));
    printf("\nThe sum of values = %d\n\n", arraySumPtr(array, 5));
    printf("\nThe sum of values = %d\n\n", arraySum(parr, 3));

    for(int i = 0; i < 80; i++)
        printf("*");

    printf("\n\n");

    validPtrArith();

    return 0;
}


// The following way of computing sum enables the function to be at an
// abstracted level and compute for any number passed. It need not know how many
// elements are there in the array and also selectively (sequencing through and)
// compute.
int arraySum(int arr[], const int n)
{
    int sum = 0, *ptr;

    printf("Address of first element of array\t: %d (%p)\n", arr, arr);
    printf("Address of last element of array\t: %d (%p)\n", arr + n, arr + n);

    // Constant pointer, to fix (so that the address doesn't change) and store
    // the address of the last element.
    int *const arrayEnd = arr + n;

    printf("Address arrayEnd\t\t\t: %d (%p)\n", arrayEnd, arrayEnd);

    for(ptr = arr; ptr < arrayEnd; ++ptr)
        sum += *ptr;

    return sum;
}


// Is the same as above. The array can be replaced by a pointer.
int arraySumPtr(int *arr, const int n)
{
    int sum = 0;

    printf("Address of first element of array\t: %d (%p)\n", arr, arr);
    printf("Address of last element of array\t: %d (%p)\n", arr + n, arr + n);

    // Constant pointer, to fix (so that the address doesn't change) and store
    // the address of the last element.
    int *const arrayEnd = arr + n;

    printf("Address arrayEnd\t\t\t: %d (%p)\n", arrayEnd, arrayEnd);

    for(; arr < arrayEnd; ++arr)
        sum += *arr;

    return sum;

}

void validPtrArith(void)
{
    int urn[3] = {1, 2, 3};
    int arr[10] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    int *ptr1 = &arr, *ptr2 = &arr;

    printf("Array urn contents:\n");

    for(int i = 0; i < 3; i++)
        printf("urn[%d] = %7d,\taddress(urn[%d]) = %16d,"
                "\taddress(urn[%d]) = %19p\n", i, urn[i],
                i, &urn[i], i, &urn[i]);

    printf("\nArray arr contents:\n");

    for(int i = 0; i < 10; i++)
        printf("arr[%d] = %7d,\taddress(arr[%d]) = %16d,"
                "\taddress(arr[%d]) = %19p\n", i, arr[i],
                i, &arr[i], i, &arr[i]);

    printf("\n\nPointer contents:\n");

    printf("\n*ptr1 = %8d,\tptr1 (address) = %17d,"
            "\tptr1 (address) = %20p\n", *ptr1, ptr1, ptr1);
    printf("*ptr2 = %8d,\tptr2 (address) = %17d,"
            "\tptr2 (address) = %20p\n\n", *ptr2, ptr2, ptr2);

    ptr1++;                 // Valid.

    printf("\nPointer contents (after ptr1++):\n");
    printf("\n*ptr1 = %8d,\tptr1 (address) = %17d,"
            "\tptr1 (address) = %20p\n", *ptr1, ptr1, ptr1);
    printf("*ptr2 = %8d,\tptr2 (address) = %17d,"
            "\tptr2 (address) = %20p\n\n", *ptr2, ptr2, ptr2);

    ptr2 = ptr1 + 2;        // Valid.

    printf("\nPointer contents (after ptr2 = ptr1 + 2):\n");
    printf("\n*ptr1 = %8d,\tptr1 (address) = %17d,"
            "\tptr1 (address) = %20p\n", *ptr1, ptr1, ptr1);
    printf("*ptr2 = %8d,\tptr2 (address) = %17d,"
            "\tptr2 (address) = %20p\n\n", *ptr2, ptr2, ptr2);

    ptr2 = urn + 1;         // Valid.
    printf("\nPointer contents (after ptr2 = urn + 1):\n");
    printf("\n*ptr1 = %8d,\tptr1 (address) = %17d,"
            "\tptr1 (address) = %20p\n", *ptr1, ptr1, ptr1);
    printf("*ptr2 = %8d,\tptr2 (address) = %17d,"
            "\tptr2 (address) = %20p\n\n", *ptr2, ptr2, ptr2);

    // Invalid. Throws error "lvalue required as increment operand."
//    urn++;
    // Invalid, can't add 2 pointer addresses. Throws error "invalid operands
    // to binary + (have 'int *' and 'int *').
//    ptr2 = ptr2 + ptr1;
    // Invalid, can't add 2 addresses. Throws error "invalid operands to binary
    // + (have 'int *' and 'int *'). This shows that array is also a pointer.
//    ptr2 = urn + ptr1
}



/*******************************************************************************

OUTPUT  :
---------

Address of first element of array	: 2020873776 (0x7ffc78741630)
Address of last element of array	: 2020873816 (0x7ffc78741658)
Address arrayEnd			: 2020873816 (0x7ffc78741658)

The sum of values = 21

Address of first element of array	: 2020873744 (0x7ffc78741610)
Address of last element of array	: 2020873768 (0x7ffc78741628)
Address arrayEnd			: 2020873768 (0x7ffc78741628)

The sum of values = 25

Address of first element of array	: 2020873744 (0x7ffc78741610)
Address of last element of array	: 2020873764 (0x7ffc78741624)
Address arrayEnd			: 2020873764 (0x7ffc78741624)

The sum of values = 15

Address of first element of array	: 2020873748 (0x7ffc78741614)
Address of last element of array	: 2020873760 (0x7ffc78741620)
Address arrayEnd			: 2020873760 (0x7ffc78741620)

The sum of values = 9

********************************************************************************

Array urn contents:
urn[0] =  1,	address(urn[0]) =   2020873652,	address(urn[0]) = 0x7ffc787415b4
urn[1] =  2,	address(urn[1]) =   2020873656,	address(urn[1]) = 0x7ffc787415b8
urn[2] =  3,	address(urn[2]) =   2020873660,	address(urn[2]) = 0x7ffc787415bc

Array arr contents:
arr[0] =  9,	address(arr[0]) =   2020873664,	address(arr[0]) = 0x7ffc787415c0
arr[1] =  8,	address(arr[1]) =   2020873668,	address(arr[1]) = 0x7ffc787415c4
arr[2] =  7,	address(arr[2]) =   2020873672,	address(arr[2]) = 0x7ffc787415c8
arr[3] =  6,	address(arr[3]) =   2020873676,	address(arr[3]) = 0x7ffc787415cc
arr[4] =  5,	address(arr[4]) =   2020873680,	address(arr[4]) = 0x7ffc787415d0
arr[5] =  4,	address(arr[5]) =   2020873684,	address(arr[5]) = 0x7ffc787415d4
arr[6] =  3,	address(arr[6]) =   2020873688,	address(arr[6]) = 0x7ffc787415d8
arr[7] =  2,	address(arr[7]) =   2020873692,	address(arr[7]) = 0x7ffc787415dc
arr[8] =  1,	address(arr[8]) =   2020873696,	address(arr[8]) = 0x7ffc787415e0
arr[9] =  0,	address(arr[9]) =   2020873700,	address(arr[9]) = 0x7ffc787415e4


Pointer contents:

*ptr1 =  9,	ptr1 (address) =   2020873664,	ptr1 (address) = 0x7ffc787415c0
*ptr2 =  9,	ptr2 (address) =   2020873664,	ptr2 (address) = 0x7ffc787415c0


Pointer contents (after ptr1++):

*ptr1 =  8,	ptr1 (address) =   2020873668,	ptr1 (address) = 0x7ffc787415c4
*ptr2 =  9,	ptr2 (address) = 2020873664,	ptr2 (address) = 0x7ffc787415c0


Pointer contents (after ptr2 = ptr1 + 2):

*ptr1 =  8,	ptr1 (address) =   2020873668,	ptr1 (address) = 0x7ffc787415c4
*ptr2 =  6,	ptr2 (address) =   2020873676,	ptr2 (address) = 0x7ffc787415cc


Pointer contents (after ptr2 = urn + 1):

*ptr1 =  8,	ptr1 (address) =   2020873668,	ptr1 (address) = 0x7ffc787415c4
*ptr2 =  2,	ptr2 (address) =   2020873656,	ptr2 (address) = 0x7ffc787415b8


Process returned 0 (0x0)   execution time : 0.003 s
Press ENTER to continue.

*******************************************************************************/
