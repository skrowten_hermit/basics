/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Access Specifiers in Inheritance.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate access specifiers in
                    inheritance.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Base
{
    private: int a;

    protected: int b;

    public:
        int c;

        void funBase()
        {
            a=10;
            b=5;
            c=15;
        }
};


class Derived:public Base
{
    public:
        void funDerived()
        {
            a = 10;
            b = 5;
            c = 15;
        }
};


int main()
{
    Base b;
    b.a = 10;
    b.b = 5;
    b.c = 20;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
