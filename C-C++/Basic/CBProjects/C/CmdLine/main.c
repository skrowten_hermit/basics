/*******************************************************************************
PROGRAM NAME    :   A047-cmdline-arguments.c
TITLE           :   Program to Demonstrate User Input Through Commandline.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program requires the user to supply an argument while
                    executing the program. This is displayed back to the user.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main(int argc, char *argv[])
{
    int nArgs = argc;
    char *arg1 = argv[0];
    char *arg2 = argv[1];

    printf("No. of Arguments : %d\n", nArgs);
    printf("Argument 1 (Program Name) : %s\n", arg1);
    printf("Argument 2 (Command Line Argument) : %s\n", arg2);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Run(1):

$ ./CmdLine Hello!
No. of Arguments : 2
Argument 1 (Program Name) : ./CmdLine
Argument 2 (Command Line Argument) : Hello!

Run(2):

$ ./CmdLine
No. of Arguments : 1
Argument 1 (Program Name) : ./CmdLine
Argument 2 (Command Line Argument) : (null)

*******************************************************************************/
