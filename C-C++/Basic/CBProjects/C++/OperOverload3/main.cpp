/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Stream Operator Overloading in
                    Complex Class.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate stream operator overloading
                    in a class implementing complex numbers.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Complex
{
    private:
        int real;
        int img;

    public:
        Complex(int r = 0, int i = 0)
        {
            real = r;
            img = i;
        }

        friend ostream & operator<<(ostream &out, Complex &c);
};


ostream & operator <<(ostream &out, Complex &c)
{
    out << c.real << " + i" << c.img << endl;
    return out;
}


int main()
{
    Complex c(10, 5);
    cout << c << endl;
    operator << (cout, c);
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
