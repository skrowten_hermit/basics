/*******************************************************************************
PROGRAM NAME    :   A040-strings-searchfuncs.c
TITLE           :   Program to Demonstrate Search in A String.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates a search for a character and a
                    substring in a given string using string library functions.
                    These functions return the index where the string was found.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    char str[] = "The quick brown fox"; // The string on which search is done.
    char ch = 'q';                      // The character to be searched.
    char *pGot_char = NULL;             // Pointer initialized to NULL.
    // Stores address where 'ch' is found. 'ch' will be converted to 'int'
    // (by ASCII value).
    pGot_char = strchr(str, ch);

    printf("Search result : %s\n", pGot_char);

    char text[] = "Every dog has his day"; // The string on which search is done.
    char word[] = "dog";                      // The character to be searched.
    char *pFound = NULL;             // Pointer initialized to NULL.
    // Stores address where 'ch' is found. 'ch' will be converted to 'int'
    // (by ASCII value).
    pFound = strstr(text, word);

    printf("Search result : %s\n", pFound);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Search result : quick brown fox
Search result : dog has his day

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
