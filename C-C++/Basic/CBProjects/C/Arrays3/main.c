/*******************************************************************************
PROGRAM NAME    :   A027-array-init-loop.c
TITLE           :   Program to Demonstrate Array Initialization Using For Loops.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to initialize arrays using for
                    loops.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main(void)
{
    // Initializes first 5 indexes with values.
    int arr_vals[10] = {0, 1, 2, 3, 4};
    int i;

    for (i = 5; i < 10; i++)
        arr_vals[i] = i;    // Initializes remaining indexes with values.

    for (i = 0; i < 10; i++)
        printf("[Array Initialization 1] arr[%d] = %d\n", i, arr_vals[i]);

    for (i = 0; i < 10; i++)
// Overwrites all the above initialized values. Equivalent to arr_vals[i] = {0}.
        arr_vals[i] = 0;

    printf("\n");

    for (i = 0; i < 10; i++)
        printf("[Array Initialization 2] arr[%d] = %d\n", i, arr_vals[i]);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

[Array Initialization 1] arr[0] = 0
[Array Initialization 1] arr[1] = 1
[Array Initialization 1] arr[2] = 2
[Array Initialization 1] arr[3] = 3
[Array Initialization 1] arr[4] = 4
[Array Initialization 1] arr[5] = 5
[Array Initialization 1] arr[6] = 6
[Array Initialization 1] arr[7] = 7
[Array Initialization 1] arr[8] = 8
[Array Initialization 1] arr[9] = 9

[Array Initialization 2] arr[0] = 0
[Array Initialization 2] arr[1] = 0
[Array Initialization 2] arr[2] = 0
[Array Initialization 2] arr[3] = 0
[Array Initialization 2] arr[4] = 0
[Array Initialization 2] arr[5] = 0
[Array Initialization 2] arr[6] = 0
[Array Initialization 2] arr[7] = 0
[Array Initialization 2] arr[8] = 0
[Array Initialization 2] arr[9] = 0

Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
