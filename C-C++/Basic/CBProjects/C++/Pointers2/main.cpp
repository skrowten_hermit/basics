/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Pointer Arithmetic.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate pointer arithmetic.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int A[5] = {2, 4, 6, 8, 10};
    int *p = A, *q = &A[4];

    cout << *p << endl;
    cout << endl;
    p++;
    cout << *p << endl;
    cout << endl;
    p--;
    cout << *p << endl;
    cout << endl;
    cout << p << endl;
    cout << p+2 << endl;
    cout << endl;
    cout << *p << endl;
    cout << *(p + 2) << endl;
    cout << endl;
    cout << q - p << endl;
    cout << p - q << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
