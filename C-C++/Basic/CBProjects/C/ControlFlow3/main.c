/*******************************************************************************
PROGRAM NAME    :   A015-conditional-operator.c
TITLE           :   Program to Demonstrate Conditional Operator.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the use of conditional operator
                    as a substitute for an "if" statement.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    int x = 8, y = 14, z = 0;

    z = y > x ? 1 : 0;  // Same as below if statement

//    if (y > x)
//    {
//        z = 1;
//    }
//    else
//    {
//        z = 0;
//    }

    printf("Evaluated z = %d\n", z);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Evaluated z = 1

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
