/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Find Sum of First n Natural Numbers.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to find the sum of first n natural
                    numbers.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int n, sum = 0;

    cout << "Enter a no :";
    cin >> n;

    ;for(int i = 1; i <= n; i++)
    {
        sum = sum + i;
    }

    cout << "Sum to First " << n << " natural numbers is " << sum;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
