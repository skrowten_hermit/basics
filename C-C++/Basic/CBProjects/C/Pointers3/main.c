/*******************************************************************************
PROGRAM NAME    :   A052-pointers-init.c
TITLE           :   Program to Demonstrate Initialization and Assignment of
                    Pointers.
DATE            :   16/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates initialization and assignment of
                    values to pointers - through in code as well as through
                    scanf() function.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{

    int value = 0;
    // Without initializing, the behaviour may be unpredictable.
//    int *pvalue;
    int *pvalue = &value;

    printf("Input an integer (as pointer through pvalue) : ");
    // This will be a teriible error if *pvalue is uninitialized.
    scanf("%d", pvalue);

    printf("\nYou entered (value) = %d, *pvalue = %d\n", value, *pvalue);
    printf("pvalue = %d, addressof(value) = %d\n", pvalue, &value);
    printf("pvalue = %p, addressof(value) = %p\n", pvalue, &value);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Input an integer (as pointer through pvalue) : 97

You entered (value) = 97, *pvalue = 97
pvalue = -1017649396, addressof(value) = -1017649396
pvalue = 0x7fffc357e70c, addressof(value) = 0x7fffc357e70c

Process returned 0 (0x0)   execution time : 15.986 s
Press ENTER to continue.

*******************************************************************************/
