/*******************************************************************************
PROGRAM NAME    :   A029-array-prime-gen.c
TITLE           :   Program to Generate Prime Numbers Upto 100 Using Arrays.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program generates prime numbers less than 100 using
                    array.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdbool.h>


int main()
{
    int p;
    int i;

    int primes[50] = {0};
    int primeIDX = 2;

    bool isPrime;

    // Hard-code prime numbers.

    primes[0] = 2;
    primes[1] = 3;

    for (p = 5; p <= 100; p += 2)
    {
        isPrime = true;
        for (i = 1; isPrime && p / primes[i] >= primes[i]; i++)
        {
            if (p % primes[i] == 0)
                isPrime = false;
        }
        if (isPrime == true)
        {
            primes[primeIDX] = p;
            ++primeIDX;
        }
    }

    for (i = 0; i < primeIDX; ++i)
        printf("%i  ", primes[i]);

    printf("\n");

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

2  3  5  7  11  13  17  19  23  29  31  37  41  43  47  53  59  61  67  71  73
79  83  89  97

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
