/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to find Factors of a Number.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to find all the factors of a given number.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int n;

    cout << "Enter a no. : ";
    cin >> n;

    for(int i = 1; i <= n; i++)
    {
        if(n % i == 0)
            cout << i <<" ";
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
