/*******************************************************************************
PROGRAM NAME    :   A079-files-convfileupper.c
TITLE           :   Program to Convert the Contents of a File to Upper Case.
DATE            :   20/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses functions fgetc() and fputc() to convert
                    the contents of a given file to upper case using a temporary
                    file.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>

#define FILENAME "sample.txt"


int main()
{
    FILE *fp1 = NULL, *fp2 = NULL;
    char ch;

    fp1 = fopen(FILENAME, "r");

    if (fp1 == NULL)
    {
        printf("File Open Error!!!");
        return -1;
    }

    fp2 = fopen("temp.txt", "w");

    if (fp2 == NULL)
    {
        printf("File Open Error!!!");
        return -1;
    }

    while((ch = fgetc(fp1)) != EOF)
    {
        if (islower(ch))
            ch = ch - 32;

        fputc(ch, fp2);
    }

    fclose(fp1);
    fclose(fp2);

    rename("temp.txt", FILENAME);
    remove("temp.txt");

    fp1 = fopen(FILENAME, "r");

    if (fp1 == NULL)
    {
        printf("File Open Error!!!");
        return -1;
    }

    while((ch = fgetc(fp1)) != EOF)
        printf("%c", ch);

    return 0;
}
