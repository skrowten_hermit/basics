/*******************************************************************************
PROGRAM NAME    :   A076-files-writefilefrintf.c
TITLE           :   Program to Demonstrate Writing Strings and Integers to a
                    file using fprintf().
DATE            :   19/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses functions fputs() to write data to a file
                    and read strings and integers.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    FILE *fp1, *fp2;
    int ch;
    char spaces[] = " ";

    fp1 = fopen("076_writedata1.txt", "w+");

    fputs("ASCII Table\n-----------\n\n", fp1);

    for(ch = 32; ch <= 126; ch++)
    {
//        putw((int)ch, fp1);
//        fputc(' ', fp1);
        fprintf(fp1, "%d - ", ch);
//        fputc('-', fp1);
//        fputc((char)32, fp);
//        fputs(spaces, fp);
        fputc(ch, fp1);
        fputc('\n', fp1);
    }

    fclose(fp1);

    fp2 = fopen("076_writedata2.txt", "w+");

    fputs("This is Sreekanth Attending Jason Fedin's Course.", fp2);
    fputs("I'm happy to be here.", fp2);

    fprintf(fp2, "%s %s %s %s %d", "This", "is", "the", "year", 2019);

    fclose(fp2);

    return 0;
}
