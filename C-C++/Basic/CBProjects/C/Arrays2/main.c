/*******************************************************************************
PROGRAM NAME    :   A026-array-init.c
TITLE           :   Program to Demonstrate Array Inititalization.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to initialize an array and
                    process the values.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define MONTHS 12


int main()
{
    int days[MONTHS] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    // Using designated initializers.
//    int days[MONTHS] = {31, 28, [4] = 31, 30, 31, [1] = 29};
    int idx;

    for (idx = 0; idx < MONTHS; idx++)
        printf("Month %2u has %2u days.\n", idx + 1, days[idx]);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Month  1 has 31 days.
Month  2 has 28 days.
Month  3 has 31 days.
Month  4 has 30 days.
Month  5 has 31 days.
Month  6 has 30 days.
Month  7 has 31 days.
Month  8 has 31 days.
Month  9 has 30 days.
Month 10 has 31 days.
Month 11 has 30 days.
Month 12 has 31 days.

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
