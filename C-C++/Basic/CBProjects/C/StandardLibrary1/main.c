/*******************************************************************************
PROGRAM NAME    :   A065-stanlib-valrange.c
TITLE           :   Program to Display the Limits/Ranges of Different Data
                    Types.
DATE            :   <>
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program displays the limites and range of different
                    data types.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stddef.h>
#include <limits.h>
#include <stdlib.h>


int main()
{
    printf("Value Ranges of Data Types:\n");
    printf("***************************\n\n");

    printf("CHAR_BIT    :   %20d\n", CHAR_BIT);
    printf("CHAR_MAX    :   %20d\n", CHAR_MAX);
    printf("CHAR_MIN    :   %20d\n", CHAR_MIN);
    printf("SCHAR_MAX   :   %20d\n", SCHAR_MAX);
    printf("SCHAR_MIN   :   %20d\n", SCHAR_MIN);
    printf("UCHAR_MAX   :   %20d\n", UCHAR_MAX);
    printf("SHRT_MAX    :   %20d\n", SHRT_MAX);
    printf("SHRT_MIN    :   %20d\n", SHRT_MIN);
    printf("USHRT_MAX   :   %20d\n", (unsigned short) USHRT_MAX);
    printf("INT_MAX     :   %20d\n", INT_MAX);
    printf("INT_MIN     :   %20d\n", INT_MIN);
    printf("UINT_MAX    :   %20u\n", (unsigned int) UINT_MAX);
    printf("LONG_MAX    :   %20ld\n", (long) LONG_MAX);
    printf("LONG_MIN    :   %20ld\n", (long) LONG_MIN);
    printf("ULONG_MAX   :   %20lu\n", (unsigned long) ULONG_MAX);
    printf("LLONG_MAX   :   %20ld\n", (long) LLONG_MAX);
    printf("LLONG_MIN   :   %20ld\n", (long) LLONG_MIN);
    printf("ULLONG_MAX  :   %20lu\n", (long) ULLONG_MAX);

    printf("FLT_MAX     :   %g\n", (float) FLT_MAX);
    printf("FLT_MIN     :   %g\n", (float) FLT_MIN);
    printf("-FLT_MAX    :   %g\n", (float) -FLT_MAX);
    printf("-FLT_MIN    :   %g\n", (float) -FLT_MIN);
    printf("DBL_MAX     :   %g\n", (double) DBL_MAX);
    printf("DBL_MIN     :   %g\n", (double) DBL_MIN);
    printf("-DBL_MAX    :  %g\n", (double) -DBL_MAX);
    printf("Storage size for float : %d \n", sizeof(float));
    printf("Precision value: %d\n", FLT_DIG );

    /* To Use :
    "NULL", offsetof(structure, member), ptrdiff_t, size_t, wchar_t
    */

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
