/*******************************************************************************
PROGRAM NAME    :   A037-strings-props.c
TITLE           :   Program to Demonstrate Character Array/String Properties.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the various properties and
                    behavior of character arrays and strings.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    printf("Initializing Word....\n\n");

    char word1[] = {'H', 'e', 'l', 'l', 'o', '!'};
    printf("(As Charater Array Without size) : %s", word1);
    printf("\nSize : %d \n", strlen(word1));

    char word2[7] = {"Hello!"};
    printf("(As String with Size) : %s", word2);
    printf("\nSize : %d \n", strlen(word2));

    char word3[] = "Hello!";
    printf("(As String without Size) : %s", word3);
    printf("\nSize : %d \n", strlen(word3));

    char input[10];
    printf("\nEnter your name : ");
    scanf("%s", input);
//    scanf("%s", &input);
    printf("Hi %s!\n", input);

    char x1 = 'a';
    char x2[] = "a";

    printf("\nThe 2 inputs are :\n\'%c\'\nand\n\"%s\"\n", x1, x2);

    if (x1 == x2)
        printf("Character \'%c'\ and String \"%s\" are EQUAL!!!\n", x1, x2);
    else
        printf("Character \'%c'\ and String \"%s\" are NOT EQUAL!!!\n", x1, x2);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Initializing Word....

(As Charater Array Without size) : Hello!��wWtU
Size : 12
(As String with Size) : Hello!
Size : 6
(As String without Size) : Hello!
Size : 6

Enter your name : Sreekanth
Hi Sreekanth!

The 2 inputs are :
'a'
and
"a"
Character 'a' and String "a" are NOT EQUAL!!!

Process returned 0 (0x0)   execution time : 9.424 s
Press ENTER to continue.

*******************************************************************************/
