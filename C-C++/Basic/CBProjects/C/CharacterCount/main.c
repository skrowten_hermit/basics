/*******************************************************************************
PROGRAM NAME    :   A059-strings-ufuncs-charcnt.c
TITLE           :   Program to Count the Number of Characters in a Given String.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program counts the number of printable characters in a
                    given string. This uses a character pointers instead of
                    character arrays.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int countChars(const char *string);


int main()
{
    printf("Length of \"String Length Test\" is\t: %2d\n", countChars("String Length Test"));
    printf("Length of \"\" is\t\t\t\t: %2d\n", countChars(""));
    printf("Length of \"Sreekanth\" is\t\t: %2d\n", countChars("Sreekanth"));

    return 0;
}


int countChars(const char *str)
{
    const char *endAddr = str;

    while(*endAddr)
        ++endAddr;

    return endAddr - str;
}



/*******************************************************************************

OUTPUT  :
---------

Length of "String Length Test" is	: 18
Length of "" is				        :  0
Length of "Sreekanth" is		    :  9

Process returned 0 (0x0)   execution time : 0.010 s
Press ENTER to continue.

*******************************************************************************/
