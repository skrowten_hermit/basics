/*******************************************************************************
PROGRAM NAME    :   A041-strings-parsefuncs.c
TITLE           :   Program to Demonstrate String Parsing. [TO DO]
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to parse strings given a
                    delimiter using strtok() function from string library.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    char str[80] = "Hello.I'm fine.I'm Happy";
    const char delim[2] = ".";
    char *token;

    printf("Original Text : %s\n", str);
    token = strtok(str, delim);         // Gets the first token.

    printf("\nParsed Text :\n");
    while(token != NULL)
    {
        printf("%s\n", token);
        token = strtok(NULL, delim);    // How does this work?
//        printf("%s\n\n", token);
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Original Text : Hello.I'm fine.I'm Happy

Parsed Text :
Hello
I'm fine
I'm Happy

Process returned 0 (0x0)   execution time : 0.004 s
Press ENTER to continue.

*******************************************************************************/
