/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program for Infinite Loop.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate an infinite loop.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int i = 0;

    ;for(;;)
    {
        cout<<" Hello";
        i++;
    }
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
