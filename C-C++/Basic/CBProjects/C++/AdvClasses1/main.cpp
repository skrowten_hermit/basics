/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Static.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate Static

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Test
{
    public:
        int a;
        static int count;

        Test()
        {
            a = 10;
            count++;
        }

        static int getCount()
        {
            return count;
        }
};


int Test::count=0;


int main()
{
    Test t1,t2;
    cout << Test::getCount() << endl;
    cout << t1.getCount() << endl;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
