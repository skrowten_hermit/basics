/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Example of Friend Class.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to demonstrate Friend class

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Your;


class My
{
    private:
        int a;

    protected:
        int b;

    public:
        int c;
        friend Your;
};


class Your
{
    public:
        My m;

    void fun()
    {
        m.a = 10;
        m.b = 10;
        m.c = 10;
    }
};


int main()
{
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
