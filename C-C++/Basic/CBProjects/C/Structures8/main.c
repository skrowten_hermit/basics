/*******************************************************************************
PROGRAM NAME    :   A072-struct-employeestruct.c
TITLE           :   Program to Implement an Employee Structure and Use it to
                    Store Data Using Structure Arrays. [TODO]
DATE            :   13/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses a structure for storing employee details
                    and display them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


struct employee {
    char name[30];
    char date[15];
    float salary;
};


int main()
{
    struct employee emp = {"Mike", "16/07/2001", 76909.000f};

    printf("\n Name : %s", emp.name);
    printf("\n Hire Date : %s", emp.date);
    printf("\n Salary : %.2f\n", emp.salary);

    printf("\nEnter Employee Information:");
    printf("\nName : ");
    scanf("%s", emp.name);
    printf("\nHire Date : ");
    scanf("%s", emp.date);
    printf("\nSalary : ");
    scanf("%f", &emp.salary);

    printf("\n Name : %s", emp.name);
    printf("\n Hire Date : %s", emp.date);
    printf("\n Salary : %.2f\n", emp.salary);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
