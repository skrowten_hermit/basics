/*******************************************************************************
PROGRAM NAME    :   A073-struct-itemstruct.c
TITLE           :   Program to Implement an Item Structure and Use it to
                    Read and Store Data.
DATE            :   13/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses a structure for storing item details -
                    name, quantity and price.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>


struct item {
    char *itemName;
    int qty;
    float price;
    float amount;
};


void readItem(struct item *ptritm);

void printItem(struct item *ptritm);


int main()
{
    struct item itm;
    struct item *pItm;

    pItm = &itm;

    pItm->itemName = (char *) malloc(30 * sizeof(char));

    if(pItm == NULL)
        exit(-1);

    readItem(pItm);     // Read an item.

    printItem(pItm);    // Print the read item.

    // Not freeing the entire structure because the entire structure will be
    // deleted on the stack when the function returns. We just want to free
    // what we allocated through malloc() (i.e., what we malloc'ed. Hence, we
    // free only 'itemName'.
    free(pItm->itemName);

    printf("\n");

    return 0;
}


void readItem(struct item *i)
{
    printf("\nEnter Product Name : ");
    scanf("%s", i->itemName);

    printf("\nEnter Price of %s : $", i->itemName);
    scanf("%f", &i->price);

    printf("\nEnter Quantity of %s : ", i->itemName);
    scanf("%d", &i->qty);

    i->amount = (float) i->qty * i->price;

    printf("\n\nAmount of %s is : $%.2f\n\n", i->itemName, i->amount);
}


void printItem(struct item *i)
{
    printf("\n\nThe following are the details of the item \"%s\" :", i->itemName);
    printf("\n Price  \t: $%.2f", i->price);
    printf("\n Quantity \t: %d nos.", i->qty);
    printf("\n Amount \t: $%.2f", i->amount);
}



/*******************************************************************************

OUTPUT  :
---------


Enter Product Name : Cereal

Enter Price of Cereal : $1.99

Enter Quantity of Cereal : 5


Amount of Cereal is : $9.95



The following are the details of the item "Cereal" :
 Price  	: $1.99
 Quantity 	: 5
 Amount 	: $9.95

Process returned 0 (0x0)   execution time : 8.383 s
Press ENTER to continue.

*******************************************************************************/
