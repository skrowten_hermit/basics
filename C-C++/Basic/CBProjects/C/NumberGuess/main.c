/*******************************************************************************
PROGRAM NAME    :   A023-number-guess.c
TITLE           :   Program to Guess the Number Right.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program generates a random number and asks the user to
                    guess it in 5 tries. It also gives a hint if the number is
                    greater than or less than the number.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>     // For srand() function - generates random numbers.
#include <time.h>       // For input to srand(), as seed for the random number
                        // generator.
#define LIMIT 20


int main()
{
    int randNum = 0;
    int guess = 0;
    int numGuesses = 5;

    time_t t;

    // Initialize random number generator.

    srand((unsigned) time(&t));

    // Get the random number.

    randNum = rand() % (LIMIT + 1);

    printf("Welcome!!!\n\nThis is a guessing game.\
            \nI have chosen a number between 0 - 20, which you have to guess.\
            \n\nYou have 5 tries to get it right!\n\n");

    for(; numGuesses > 0; --numGuesses)
    {
        printf("\nYou have %d tr%s left. ", numGuesses,
                numGuesses == 1 ? "y" : "ies");
        printf("Enter a guess : ");
        scanf("%d", &guess);

        if (guess == randNum)
        {
            printf("\n\nYou guessed it right! %d is correct.\
                    \n\nCongrats!!!\n\n", guess);
            return;
        }
        else if (guess < 0 || guess > 20)
        {
            printf("I said the number is between 0 and 20!\n");
        }
        else if (randNum > guess)
        {
            printf("Sorry, %d is wrong. My number is greater than that!\n",
                    guess);
        }
        else if (randNum < guess)
        {
            printf("Sorry, %d is wrong. My number is less than that!\n",
                    guess);
        }
    }

    printf("\n\nYour 5 tries are over. You failed. My number was %d!\n\n\
            Try again later!!!\n\n", randNum);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Welcome!!!

This is a guessing game.
I have chosen a number between 0 - 20, which you have to guess.

You have 5 tries to get it right!


You have 5 tries left. Enter a guess : 5
Sorry, 5 is wrong. My number is greater than that!

You have 4 tries left. Enter a guess : 23
I said the number is between 0 and 20!

You have 3 tries left. Enter a guess : 10
Sorry, 10 is wrong. My number is greater than that!

You have 2 tries left. Enter a guess : 15
Sorry, 15 is wrong. My number is less than that!

You have 1 try left. Enter a guess : 12
Sorry, 12 is wrong. My number is greater than that!


Your 5 tries are over. You failed. My number was 13!

            Try again later!!!


Process returned 0 (0x0)   execution time : 20.402 s
Press ENTER to continue.

*******************************************************************************/
