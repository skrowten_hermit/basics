#include <FixedPoints.h>
#include <FixedPointsCommon.h>

// This is a blinky comparison between fixed and floating point calculations

//#define USE_FLOATING_POINT
#define MEASURE

#define MASK(x) ((unsigned char)(1<<x))

#ifdef USE_FLOATING_POINT
  volatile float w,x,y,z;
#else
  UQ8x8 w,x,y,z;
#endif

#ifdef MEASURE
uint32_t StartTime, EndTime;
#endif

void setup() {                
  DDRB |= MASK(5);   // The LED pin is output. Mask: 00100000  
  Serial.begin(9600);  
  w=0.0;
  x=2.5;
  y=2.9;
  z=1.1;
}
uint32_t i;
void loop() {
  PORTB ^= MASK(5);  // Toggle LED 
  i = 10000;
  #ifdef MEASURE
    StartTime = millis();
  #endif
  
  while (i--){
    x=x+y;
    x=x+z;
    x=x-z;
    x=x+z;
    x=x-z;
    x=x*z;
    x=x-y;
    w=x+y-z/(x+y+z);
    x=x/(y-2*z/4);
    w=x+y-z;
    if(x>100){
      x=x/10;
    }
  }  

  #ifdef MEASURE
    EndTime = millis();
    Serial.print("Took ");
    Serial.print((float)(EndTime - StartTime)*0.001,3);
    Serial.print(" seconds\n");
  #endif
}
