/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Increment ans Decrement Operators.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate increment and decrement
                    operators.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int i = 5, j;
    j = i++;
    cout << j << " " << i << endl;
    int k = 5, l;
    l = ++k;
    cout << l << " " << k << endl;
    int a = 5, b;
    b = 2 * ++a + 2 * a++;
    cout << b << " " << a << endl;
    int c = 5, d;
    d = 2 * c++ + 2 * c++;
    cout << d << " " << c << endl;
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
