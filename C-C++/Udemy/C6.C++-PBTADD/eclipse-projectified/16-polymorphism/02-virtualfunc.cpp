/*
 * 02-virtualfunc.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */



#include <iostream>
using namespace std;

class BasicCar
{
    public:
        virtual void start(){cout<<"BasicCar started"<<endl;}
};

class AdvancedCar: public BasicCar
{
    public:
        void start(){cout<<"AdvanceCar Started"<<endl;}
};

int main()
{
    BasicCar *p=new AdvancedCar();
    p->start();
}
