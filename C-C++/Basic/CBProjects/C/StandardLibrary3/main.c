/*******************************************************************************
PROGRAM NAME    :   A067-stanlib-convertfuncs.c
TITLE           :
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>


int main()
{
{
    /* To Use :
    fclose() feof() fgetc() fgetpos() fgets() fopen() fprintf() fputc() fputs() fscanf() fseek()
    fsetpos() ftell() remove() rename()
    */
    printf("Hello world!\n");
    /* To Use :
    atof() atoi() atol() atos()
    */
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
