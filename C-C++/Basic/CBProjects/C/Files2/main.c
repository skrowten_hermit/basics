/*******************************************************************************
PROGRAM NAME    :   A075-files-readfilefscanf.c
TITLE           :   Program to Demonstrate Reading Strings and Integers from
                    a file.
DATE            :   19/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses functions fputs() to write data to a file
                    and read strings and integers.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    char str1[20], str2[20], str3[10];
    int year;
    FILE *fp;

    fp = fopen("075_readdata.txt", "w+");
    if (fp != NULL)
        fputs("Hello, is this 1989", fp);

    rewind(fp);

    fscanf(fp, "%s %s %s %d", str1, str2, str3, &year);

    printf("Read String 1 |%s|\n", str1);
    printf("Read String 2 |%s|\n", str2);
    printf("Read String 3 |%s|\n", str3);
    printf("Read Integer  |%d|\n", year);

    return 0;
}
