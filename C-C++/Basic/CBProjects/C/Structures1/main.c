/*******************************************************************************
PROGRAM NAME    :   A065-struct-date.c
TITLE           :   Program to Demonstrate Structure Using Date.
DATE            :   07/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create a compound type date
                    using structures, initialize them and manipulate them.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    struct date
    {
        int day;
        char month[5];
        int year;
    } yesterday;

    struct
    {
        int day;
        int month;
        int year;
    } tomorrow;

    struct date someday;

//    struct date today;
//
//    strcpy(today.month, "Oct");
//    today.day = 7;
//    today.year = 2019;

    struct date today = {7, "Oct", 2019};

    strcpy(yesterday.month, "Oct");
    yesterday.day = 6;
    yesterday.year = 2019;

    tomorrow.day = 8;
    tomorrow.month = 10;
    tomorrow.year = 2019;

//    someday = (struct date) {1, "May", 2020};

    struct date bday = {.month = "Sep", .day = 23};

    printf("Today is : %d-%s-%d\n", today.day, today.month, today.year);
    printf("Yesterday was : %d-%s-%d\n", yesterday.day, yesterday.month,
            yesterday.year);
    printf("Tomorrow will be : %d-%d-%d\n", tomorrow.day, tomorrow.month,
            tomorrow.year);
//    printf("Someday will be : %d-%s-%d\n", someday.day, someday.month,
//            someday.year);

    bday.year = 2019;
    printf("Birthday this year : %d-%s-%d\n", bday.day, bday.month, bday.year);
    bday.year = 2018;
    printf("Birthday last year : %d-%s-%d\n", bday.day, bday.month, bday.year);

    printf("size(yesterday) : %d\nsize(today) : %d\nsize(tomorrow) : %d\
            \nsize(someday) : %d\nsize(bday) : %d\n", sizeof(yesterday),
            sizeof(today), sizeof(tomorrow), sizeof(someday), sizeof(bday));

    printf("size(char) : %d\n", sizeof(char));

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Today is : 7-Oct-2019
Yesterday was : 6-Oct-2019
Tomorrow will be : 8-10-2019
Birthday this year : 23-Sep-2019
Birthday last year : 23-Sep-2018
size(yesterday) : 16
size(today) : 16
size(tomorrow) : 12
size(someday) : 16
size(bday) : 16
size(char) : 1

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
