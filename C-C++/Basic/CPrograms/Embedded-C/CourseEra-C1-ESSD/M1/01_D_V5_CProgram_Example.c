/*******************************************************************************
PROGRAM NAME    :   
TITLE           :   
VERSION         :   
DATE WRITTEN    :   
AUTHOR          :   
DATE MODIFIED   :   
MODIFIED BY     :   
CHANGES MADE    :   
DESCRIPTION     :   
*******************************************************************************/



#include<stdio.h>

#define NULL (0)
#define NUMBER_SET_LENGTH (20)
#define HISTOGRAM_LENGTH (6)


int find_average(int *ptr, int count);


void main()
{
     // A set of numbers in the range [0.5].
     int numbers[NUMBER_SET_LENGTH] = {1, 4, 5, 1, 3,
                                       2, 3, 2, 2, 4,
				       0, 4, 5, 3, 1,
				       0, 3, 0, 5, 3};
    
    // Histogram Array.
    int histogram[HISTOGRAM_LENGTH] = {0, 0, 0, 0, 0, 0};
    int average;
    int max;
    int min;
    int i = 0;

    average = find_average(numbers, NUMBER_SET_LENGTH);

    while(i < NUMBER_SET_LENGTH)
    {
        if(i == 0)
        {
            max = numbers[0];    // Must initialize to values in set, not zero.
            min = numbers[0];    // Move to second item and start comparisons.
        }

        if(numbers[i] <= min)
        {
            min = numbers[i];
        }
        else if(numbers[i] >= max)
        {
            max = numbers[i];
        }
    
        histogram[i]++;
    
        i++;
    }
  /* Routines can be added here for Printing Data */
}


int find_average(int * ptr, int count)
{
    int i;
    int average;

    if (ptr == NULL)
    {
        return 0;
    }
  
    if (count <= 0)
    {
        count = 1;
    }
  
    for(i = 0; i < count; i++)
    {
        average += *ptr;
        ptr++;
    }
 
    return (average / count);
}
