/*******************************************************************************
PROGRAM NAME    :   A031-func-multiply-ret.c
TITLE           :   Program to Multiply 2 Nos. Using A Returnable Function
                    multiplyTwoNums().
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program defines a function multiplyTwoNums() which can
                    multiply 2 numbers and returns the result.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int multiplyTwoNums(int x, int y)
{
    int result = x * y;

    return result;
}


int main(void)
{
    int result1 = 0, result2 = 0, result3 = 0;
    int x1 = 10, x2 = 20, x3 = 50;
    int y1 = 20, y2 = 30, y3 = -2;

    result1 = multiplyTwoNums(10, 20);
    printf("The product of %d and %d is : %d\n\n", x1, y1, result1);

    result2 = multiplyTwoNums(20, 30);
    printf("The product of %d and %d is : %d\n\n", x2, y2, result2);

    result3 = multiplyTwoNums(50, -2);
    printf("The product of %d and %d is : %d\n\n", x3, y3, result3);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

The product of 10 and 20 is : 200

The product of 20 and 30 is : 600

The product of 50 and -2 is : -100


Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
