/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to find root of Quadratic Equation.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to calculate the roots of a quadratic
                    equation. The user provides the 3 coefficients of the
                    expression and gets the roots as the output.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>
#include <math.h>

using namespace std;


int main()
{
    int a, b, c;
    float root1, root2;

    cout << "Enter the values of 3 coefficients :" << endl;
    cin >> a >> b >> c;
    root1 = (-b + sqrt((b * b) - (4 * a * c))) / (2 * a);
    root2 = (-b - sqrt((b * b) - (4 * a * c))) / (2 * a);
    cout << root1 << " " << root2 << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
