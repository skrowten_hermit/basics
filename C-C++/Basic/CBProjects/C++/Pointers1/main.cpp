/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Pointer Syntax.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate basic pointers.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int a = 10;
    int *p = &a;

    cout << a << endl;
    cout << &a << endl;
    cout << p << endl;
    cout << *p << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
