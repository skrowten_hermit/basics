/*******************************************************************************
PROGRAM NAME    :   A069-stanlib-miscfuncs.c
TITLE           :
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <stdarg.h>


int main()
{
    /* To Use :
    abs() exit() getenv() qsort() rand() srand() system()
    */
    printf("Hello world!\n");
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
