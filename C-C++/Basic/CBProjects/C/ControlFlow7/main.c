/*******************************************************************************
PROGRAM NAME    :   A019-for-statement-demo.c
TITLE           :   Program to Sum First 'N' Nos. to Demonstrate For Statement.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates a for loop by calculating the sum
                    of first N numbers - N is taken as input from the user. The
                    calculated sum is displayed to the user.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    unsigned long long sum = 0LL;
    unsigned int count = 0;

    printf("Enter the number of integers you want to sum :\n");
    scanf("%u", &count);

//    for (unsigned int i = 1; i <= count; ++i)
//        sum += i;

    // 'for statement' : Same as the above loop.
    for (unsigned int i = 1; i <= count; sum += i++);

    printf("The sum of the first %u numbers is : %d\n", count, sum);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter the number of integers you want to sum :
10
The sum of the first 10 numbers is : 55

Process returned 0 (0x0)   execution time : 4.314 s
Press ENTER to continue.

*******************************************************************************/
