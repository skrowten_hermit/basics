/*
 * 02-rectclass.cpp
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */




#include<iostream>
using namespace std;

class Rectangle
{
    public:
        void area()
        {
            cout<<"Area of Rectangle"<<endl;
        }
};

class Cuboid: public Rectangle
{
    public:
        void volume()
        {
        cout<<"Volume of Cuboid"<<endl;
        }
};
