/*******************************************************************************
PROGRAM NAME    :   A066-stanlib-convertfuncs.c
TITLE           :
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>


int main()
{
    /* To Use :
    strcat() strchr() strcmp() strcpy() strlen() strncat() strncmp() strncpy() strrchr() strstr() strtok()
    */
    printf("Hello world!\n");
    /* To Use :
    tolower() toupper() isalnum() isalpha() isblank() isdigit() isgraph() isspace() ispunct() isxdigit()
    */
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
