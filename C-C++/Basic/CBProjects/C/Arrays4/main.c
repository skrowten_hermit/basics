/*******************************************************************************
PROGRAM NAME    :   A028-2d-array-demo.c
TITLE           :   Program to Demonstrate a Multi-Dimensional Array. [TODO]
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates some operations on a 2D array
                    using loops.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

int main()
{
    int mArr[4][5];

    printf("Hello world!\n");
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
