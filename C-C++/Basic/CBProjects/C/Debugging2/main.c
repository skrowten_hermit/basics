/*******************************************************************************
PROGRAM NAME    :   A049-debug-prog2-avg.c
TITLE           :   Program to Demonstrate Debugging Character Arrays.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates debugging of strings using
                    character arrays.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define NO_OF_ITEMS 999;


int main()
{
//    char *cptr;           // Wrong way. Causes segmentation fault at line 17.
    char *cptr[1];          // Correct way.

    int a = 0, number;

    char text1 = 'a';
    char text2[5];
    char text3[4] = "hello";
    char text4[5] = "hello";
    printf("sizeof(text1) = %d\n", sizeof(text1));
    printf("sizeof(text2) = %d\n", sizeof(text2));
    printf("sizeof(text3) = %d\n", sizeof(text3));
    printf("sizeof(text4) = %d\n", sizeof(text4));

    printf("a = %d\n", a);

    // Causes segmentation fault if size (memory allocation information) is not
    // mentioned in declaration.
    *cptr = 'X';

    printf("cptr = %c\n", *cptr);

    if(a = 2)
        printf("a = %d\n", a);     // Always executes.
    else
        printf("Your turn!\n");     // Never executes.

    // Correct way. Causes segmentation fault if %c is replaced by %s.
    printf("text1 = %c\n", text1);
    text1 = "a";
    // Wrong way. Causes segmentation fault because %c was replaced by %s.
//    printf("text1 = %s\n", text1);
    // Prints junk. %s was replaced by %c.
    printf("text1 = %c\n", text1);

    printf("text2 = %s\n", text2);    // Prints nothing.
    // Wrong way. Throws Error: assignment to expression with array type.
//    text2 = "hell";
    printf("text3 = %s\n", text3);    // Prints 4 characters.
    printf("text4 = %s\n", text4);    // Prints 5 characters.

    printf("Enter a number (> 0) = ");
    // Wrong. Causes segmentation fault. Must take a reference (with &).
//    scanf("%d", number);
    scanf("%d", &number);
    printf("Number = %d\n", number);

    a = number;
    number = a * NO_OF_ITEMS;

    printf("Total Number = %d\n", number);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

sizeof(text1) = 1
sizeof(text2) = 5
sizeof(text3) = 4
sizeof(text4) = 5
a = 0
cptr = X
a = 2
text1 = a
text1 = �
text2 =
text3 = hell
text4 = hello
Enter a number (> 0) = 7
Number = 7
Total Number = 6993

Process returned 0 (0x0)   execution time : 4.230 s
Press ENTER to continue.

*******************************************************************************/
