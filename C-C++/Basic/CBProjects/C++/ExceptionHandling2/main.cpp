/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate User-Defined Exception Handling.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate user-defined exception
                    handling for stack operations.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include<iostream>

using namespace std;


class StackOverFlow:exception{};

class StackUnderFlow:exception{};


class Stack
{
    private:
        int *stk;
        int top = -1;
        int size;

    public:
        Stack(int sz)
        {
            size = sz;
            stk = new int[size];
        }

        void push(int x)
        {
            if(top == size - 1)
                throw StackOverFlow();
            top++;
            stk[top] = x;
        }

        int pop()
        {
            if(top == -1)
                throw StackUnderFlow();

            return stk[top--];
        }
};


int main()
{
    Stack s(5);

    s.push(2);
    s.push(3);
    s.push(4);
    s.push(10);
    s.push(9);
    s.push(8);
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
