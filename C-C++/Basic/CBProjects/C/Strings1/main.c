/*******************************************************************************
PROGRAM NAME    :   A036-strings-null-term.c
TITLE           :   Program to Demonstrate the NULL Terminator.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program

                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main(void)
{
    /*
    Only the characters before '\0' is printed because the compiler treats
    '\0' as the end of the string. All the characters after '\0' are discarded.
    Even the '\n'!
    */
    printf("The character \0 is used to terminate a string.\n\n");
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

The character

Process returned 0 (0x0)   execution time : 0.001 s
Press ENTER to continue.

*******************************************************************************/
