/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Array Declaration & Initialization.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is a program to demonstrate declaration and
                    initialization of array.
                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int A[5];
    cout << A << endl;

    int B[5] = {2, 4};
    cout << B << endl;

    int C[] = {2, 4, 6, 8, 10};
    cout << C << endl;

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
