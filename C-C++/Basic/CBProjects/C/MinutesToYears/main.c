/*******************************************************************************
PROGRAM NAME    :   A006-minutes-to-daysyears.c
TITLE           :   Program to Convert Minutes to Days and Years.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program accepts value in minutes from user and converts
                    it into days and years.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    long minutesEntered = 0;
    double years = 0.0;
    double days = 0.0;
    double minutesInYear = 0.0;

    printf("Please Enter The Number Of Minutes:\n");
    scanf("%d", &minutesEntered);
    printf("Minutes Entered = %d\n", minutesEntered);

    minutesInYear = (60 * 24 * 365);
    years = minutesEntered / minutesInYear;
    days = (minutesEntered / 60.0) / 24.0;

    printf("\n%d minutes is approximately %f years and %f days.\n",
            minutesEntered, years, days);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Please Enter The Number Of Minutes:
500000
Minutes Entered = 500000

500000 minutes is approximately 0.951294 years and 347.222222 days.

Process returned 0 (0x0)   execution time : 5.366 s
Press ENTER to continue.

*******************************************************************************/
