/*******************************************************************************
PROGRAM NAME    :   A048-debug-prog1-avg.c
TITLE           :   Program to Demonstrate Debugging Using Average Example.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program is used to demonstrate the debugging using the
                    average calculation example.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


void print(int num1, int num2, int num3, double average);

double average(int a, int b, int);


int main(void)
{
    int n1, n2, n3;

    printf("Enter 3 numbers :\n");
    scanf("%d %d %d", &n1, &n2, &n3);

    print(n1, n2, n3, average(n1, n2, n3));

    return 0;
}


void print(int n1, int n2, int n3, double avg)
{
    printf("\nMean of %d, %d and %d is %f.\n", n1, n2, n3, avg);
}


double average(int n1, int n2, int n3)
{
    double sum;

    sum = n1 + n2 + n3;

    return sum / 3.0;
}



/*******************************************************************************

OUTPUT  :
---------

Enter 3 numbers :
3
6
5

Mean of 3, 6 and 5 is 4.666667.

Process returned 0 (0x0)   execution time : 5.047 s
Press ENTER to continue.

*******************************************************************************/
