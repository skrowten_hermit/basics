#!/bin/bash


##########################################################################################################################################
##########################################################################################################################################
## File                 :: SDK_sanity.sh
## Description          :: The SDK sanity suite script.
## Developer            :: Sreekanth S
## Version              :: v1.0
## Release Date         :: 31/08/2018
## Changes made         :: Added execution example and header.
## Changes made Date    :: 14/09/2018
## Changes made by      :: Sreekanth S
## Execution example    :: ./SDK_sanity.sh A,config_master_1.txt/B,config_master_2.txt/C,config_master_3.txt/D
##########################################################################################################################################
##########################################################################################################################################


cd ../source
#echo $PWD

echo $'\n\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..44}
echo "Sanity Suite"
echo $(printf '=%.0s' {1..100})
echo $'\n'

arg=$1
#echo "Input : $arg"
#echo

if [[ "$arg" == @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    echo "Help info for execution requested...."
    echo $'\n\n'
    echo $(printf '~%.0s' {1..100})
    printf ' %.0s' {1..43}
    echo "Help Info"
    echo $(printf '~%.0s' {1..100})
    echo $'\n\n'
    echo "Usage Description : " | boxes
    echo
    echo -e "./SDK_sanity: \n \n executes sanity suite using the default configuration file 'config.txt' (and 'config_shunra.txt', \n 'config_att.txt' if user enables network impairments and audiotune settings respectively in \n 'config.txt')." | boxes -d shell -s 100
    echo
    echo -e "./SDK_sanity <configuration>/<configuration>/.../<configuration>: \n \n executes sanity tests one after the other following the '/'. \n \n eg:- \t /SDK_sanity A,config_master_1.txt,config_shunra.txt/B,config_master_3.txt,config_att_1.txt." | boxes -d shell -s 100
    echo
    echo -e "Note: \n \n<configuration> can take the values (case sensitive) : \nlist of configuration files (input file names) separated by a ','. \n \nThe types of configuration files and the naming formats are:\n config_master_<something>\t-->\t master configuration file. \n config_shunra_<something>\t-->\t shunra configuration file. \n config_att_<something>\t\t-->\t audiotune settings configuration file." | boxes -d shell -s 100
    echo $'\n\n'
    echo $(printf '~%.0s' {1..100})
    printf ' %.0s' {1..43}
    echo "Help Info"
    echo $(printf '~%.0s' {1..100})
    echo $'\n\n'
    echo "Help info printed...."
elif [[ "$arg" != @(-h|-H|-help|-Help|-HELP|--h|--H|--help|--Help|--HELP) ]]; then
    if [[ "$arg" == "" ]]; then
        echo "No custom config files detected, using the default config file, config.txt...."
        python dvf_automation_primitive_ops.py 0
    else
        echo "Custom config file(s) given...."
        IFS='/' read -ra conf_list <<<"$arg"
        arglen=${#conf_list[@]}
        echo "Number of Inputs : $arglen"
        for i in "${!conf_list[@]}"; do
            conf_list[$i]="${conf_list[$i]//,/$' '}"
            echo "Config file sets user wants to use : " "${conf_list[$i]}"
        done
        for i in "${!conf_list[@]}"; do
            python dvf_automation_primitive_ops.py 0 ${conf_list[i]}
        done
    fi
fi

echo $'\n'
echo $(printf '=%.0s' {1..100})
printf ' %.0s' {1..44}
echo "Sanity Suite"
echo $(printf '=%.0s' {1..100})
echo $'\n\n'
exit 0