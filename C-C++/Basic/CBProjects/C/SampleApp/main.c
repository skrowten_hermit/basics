/*******************************************************************************
PROGRAM NAME    :   A0<last-number>-sample-application.c
TITLE           :   Program to Demonstrate a Sample C Application.
DATE            :   <last-date>
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates how to create a sample C
                    application with more than one files (other than the main.c)
                    .c (programs with functions) and .h (header file(s)).
*******************************************************************************/



#include <stdio.h>


void add(void);

void sayhello(void);


int main(void)
{
    sayhello();
    add();

    return 0;
}
