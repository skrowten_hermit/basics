#include <stdio.h>
#include <math.h>

char equals( float x, float y){
  const float EPSILON=0.000001;
  float diff = fabs((x-y)/x);
  return (diff<EPSILON);
}


int main(){
  volatile float a,b;
  
  a=0.3f;
  a+=0.6f;  // a= 0.9
  b=0.7f;
  b+=0.2f;  // b= 0.9
  
  printf("a=%3.20f\nb=%3.20f\n",a,b);

  if(equals(a,b))
    printf("a equals b!!!");
  else
    printf("a is not equal to b!!!");

}





