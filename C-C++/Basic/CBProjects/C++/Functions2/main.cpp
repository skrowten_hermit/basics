/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Demonstrate Static Variables.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate static variables and their
                    usage in functions.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


void fun()
{
    static int v = 0;
    int a = 10;
    v++;
    cout << "a = " << a << "\nv = " << v << endl;
}


int main()
{
    fun();
    fun();
    fun();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
