/*******************************************************************************
PROGRAM NAME    :   A045-strings-ufuncs-basics.c
TITLE           :   Program to Calculate the String Length, Concatenate &
                    Compare 2 Strings.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program implements 3 differents string functions -
                    (1). strLength() - to calculate the length (the number of
                    characters) in a given string.
                    (2). concatString() - to concatenate 2 given strings.
                    (3). equalString() - to check if 2 given strings are same
                    (i.e., equivalent).
                    All the above are impllemented using character arrays.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>
#include <stdbool.h>


int strLength(const char string[]);

void concatString(char result[], const char string1[], const char string2[]);

bool equalString(const char string1[], const char string2[]);


int main()
{
    const char word1[] = "Sreekanth";
    const char word2[] = "Sreekumar";
    const char word3[] = "Sreekuttan";

    printf("Length of \"%s\" = %d\n", word1, strLength(word1));
    printf("Length of \"%s\" = %d\n", word2, strLength(word2));
    printf("Length of \"%s\" = %d\n", word3, strLength(word3));

    char result[50];

    concatString(result, word1, word2);

    printf("\nConcatenation of \"%s\" and \"%s\" : %s\n", word1, word2, result);

    printf("\nEquality of \"%s\" and \"%s\" : %d\n", word1, word2,
            equalString(word1, word2));
    printf("Equality of \"%s\" and \"%s\" : %d\n", word1, word1,
            equalString(word1, word1));

    return 0;
}


int strLength(const char str[])     // Declared as const because we don't want
{                                   // it to be modified.
    int cnt = 0;

    while(str[cnt] != '\0')
        ++cnt;

    return cnt;
}


void concatString(char result[], const char str1[], const char str2[])
{
    int i, j;

    for(i = 0; str1[i] != '\0'; ++i)
        result[i] = str1[i];

    for(j = 0; str2[j] != '\0'; ++j)
        result[i + j] = str2[j];

    result[i + j] = '\0';
}


bool equalString(const char str1[], const char str2[])
{
    int i = 0;
    bool isEquals = false;

    while(str1[i] == str2[i] && str1[i] != '\0' && str2[i] != '\0')
        ++i;

    if(str1[i] == '\0' && str2[i] == '\0')
        isEquals = true;
    else
        isEquals = false;

    return isEquals;
}



/*******************************************************************************

OUTPUT  :
---------

Length of "Sreekanth" = 9
Length of "Sreekumar" = 9
Length of "Sreekuttan" = 10

Concatenation of "Sreekanth" and "Sreekumar" : SreekanthSreekumar

Equality of "Sreekanth" and "Sreekumar" : 0
Equality of "Sreekanth" and "Sreekanth" : 1

Process returned 0 (0x0)   execution time : 0.011 s
Press ENTER to continue.

*******************************************************************************/
