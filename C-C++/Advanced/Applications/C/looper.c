/*
 * Small utility to estimate how much CPU time is left. It just loops with a
 * very low priority and prints every second how many loops were performed. The
 * conversion to MHz is left as an excersice to the reader.
 *
 * Compile with:
 *
 *      $ arm-linux-gcc looper.c -o looper
 */

#include <sys/time.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>


volatile unsigned long counter, old_counter, measurements, sum, alarm_cnt,intervalms = 1000;
struct timeval checkpoint;

/* Following variables are used for DDR Performance counter measuring ERF_HIF_RD_OR_WR, which gives READ & WRITE counts of DDR */
unsigned long prev_ddr_cntr , current_ddr_cntr;

void usage(void)
{
	printf("Usage: looper [#interval] [#measurements]\n");
	exit(1);
}

void setup_ddr_counter()
{
	/* Select the '0' counter */
	system("echo 0  >  /sys/devices/platform/7201000.ddr-perf/ddr_perf_cnt/select");
	/* Use the PERF_HIF_RD_OR_WR for getting the READ & WRITE count of DDR */
	system("echo  0 >   /sys/devices/platform/7201000.ddr-perf/ddr_perf_cnt/target");
	/* Enable the preference counter */
	system("echo 1 >  /sys/devices/platform/7201000.ddr-perf/ddr_perf_cnt/enable");
	/* Read the count of operations */
	system("cat /sys/devices/platform/7201000.ddr-perf/ddr_perf_cnt/count");
}

void read_ddr_counter(unsigned long *value)
{
	FILE *fp = NULL;
	//system("cat /sys/devices/platform/7201000.ddr-perf/ddr_perf_cnt/count");
	fp = fopen("/sys/devices/platform/7201000.ddr-perf/ddr_perf_cnt/count","r");
	if(fp) {
		fscanf(fp, "%lu" , value);
		//printf("we got the count as '%lu'\n" , *value);
		fclose(fp);
	} else {
		printf("We failed to read the file !!!!\n");
	}
	fp = NULL;
}

void alarm_handler(int signo)
{
	struct timeval cur;
	unsigned long loops = counter - old_counter;
	unsigned long v, secs, usecs;
	unsigned long ddr_diff=0, prev_diff=0;
	float  percentage_utilzation=0, mbps_utilization=0;

	// read values
	old_counter = counter;
	gettimeofday(&cur, NULL);

	// output
	secs = cur.tv_sec - checkpoint.tv_sec;
	usecs = secs*1000000 + cur.tv_usec - checkpoint.tv_usec;
	//printf("%ld (%ld, %ld)\n", loops / (usecs >> 10), loops, usecs);
	//printf("%ld\n", loops / (usecs >> 10));
        //if((loops / (usecs >> 10)) < 10000) printf("\nLower than 10000\n");

    alarm_cnt++;
    v= loops / (usecs >> 10);
    sum += v;

	read_ddr_counter(&current_ddr_cntr);
	if(current_ddr_cntr < prev_ddr_cntr){
		/* There is wrap around happned, updated properly */
		/* As it is a Unsigned long integer is used, max value of it is "4,294,967,295" */
		prev_diff = 4294967295ul - prev_ddr_cntr;
		ddr_diff = current_ddr_cntr + prev_diff;
	} else {
		ddr_diff = current_ddr_cntr - prev_ddr_cntr;
	}

	/* Calculate the percentage of usage */
	/*As we clock DDR for 533MHz so divide with this and each burst ddr_diff (of 8 transaction will complete in 4 clocks), so multiply with 4 */
	percentage_utilzation = (((float)ddr_diff*4)/(float)(533000000))*100;

	/* Calculate the MBPS (Mega Bytes Per Sec) utilization of DDR Bus */
	mbps_utilization = ((float)( (float)ddr_diff * 16 )/(1024*1024));

	printf("%4ld: +%6ld = %9ld\t\t DDR usage %lu (%.2f\%) (%.4f MBPS)\n", alarm_cnt, v, sum , ddr_diff,percentage_utilzation,mbps_utilization);

	prev_ddr_cntr = current_ddr_cntr;
	checkpoint = cur;

	if (--measurements == 0) exit(0);
}

int main(int argc, char **argv)
{
	struct itimerval interval;
	struct sigaction action;

	counter = 0;
	old_counter = 0;
	prev_ddr_cntr = 0;
	current_ddr_cntr = 0;

	if (argc == 3)
	{
		intervalms = atoi(argv[1]);
		measurements = atoi(argv[2]);
	}
	else if (argc == 2)
		intervalms = atoi(argv[1]);
	else if (argc != 1)
		usage();

	// install handler
	memset(&action, 0, sizeof(action));
	action.sa_handler = alarm_handler;
	if (sigaction(SIGALRM, &action, NULL)) {
		perror("sigaction");
		return 1;
	}

	// start timer
	memset(&interval, 0, sizeof(interval));
	interval.it_interval.tv_sec = intervalms/1000; // get seconds from intervalms
	interval.it_interval.tv_usec = (intervalms % 1000)*1000;
	interval.it_value.tv_sec = intervalms/1000; // get seconds from intervalms
	interval.it_value.tv_usec = (intervalms % 1000)*1000;
	printf("Seting vals %d %d %d %d\n", interval.it_interval.tv_sec, interval.it_interval.tv_usec, interval.it_value.tv_sec, interval.it_value.tv_usec);
	if (setitimer(ITIMER_REAL, &interval, NULL)) {
		perror("settimer");
		return 1;
	}

	setup_ddr_counter();
	/* Here read the DDR counter, and set as prev_ddr_cntr so in next run, we will get the diff */
	read_ddr_counter(&prev_ddr_cntr);

	// loop forever at lowest priority possible
	printf("Start looping...\n");
	gettimeofday(&checkpoint, NULL);
	nice(-1);
	for (;;) counter++;

	// never reached
	return 0;
}

