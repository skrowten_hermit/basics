/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to demonstrate read and write files.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate basic file read and write
                    operation.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


int main()
{
    ifstream ifs("Test.txt");
    ofstream ofs("Test.txt",ios::trunc);

    string name;
    int roll;
    string branch;

    ifs >> name >> roll >> branch;
    cout << name << endl << roll << endl << branch << endl;
    ifs.close();

    ofs << "John" << endl;
    ofs << 25 << endl;
    ofs << "CS" << endl;
    ofs.close();
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
