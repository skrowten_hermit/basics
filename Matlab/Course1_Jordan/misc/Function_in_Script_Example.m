% Average and Median Calculator
x = 1:20;
n = length(x);
avg = mymean(x,n);
med = mymedian(x,n);

function a = mymean(v,n)
% MYMEAN Function calculates the mean of a vector x

a = sum(v)/n;
end

function m = mymedian(v,n)
% MYMEDIAN calculates the median of vector v

w = sort(v);

if(rem(n,2) == 1)
    m = w((n + 1) / 2);
else
    m = (w(n/2) + w(n/2 + 1)) / 2;
end
end


