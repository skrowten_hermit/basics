/*******************************************************************************
PROGRAM NAME    :   A061-func-swap-val-ref.c
TITLE           :   Program to Demonstrate Pass/Call-By-Value and
                    Pass/Call-By-Reference.
DATE            :   20/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the pass/call-by-value and the
                    pass/call-by-reference using 2 separate functions to swap 2
                    given numbers.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/


#include <stdio.h>


void swapbyval(int num1, int num2);

void swapbyref(int *num1, int *num2);


int main()
{
    int a = 5, b = 10;
    int c = 9, d = 6;

    printf("Before swap by value:\na = %5d,\tb = %5d\n", a, b);
    swapbyval(a, b);
    printf("After swap by value:\na = %5d,\tb = %5d\n\n", a, b);   // Swap doesn't happen because it's "passed by value".

    printf("Before swap by value:\nc = %5d,\td = %5d\n", c, d);
    swapbyref(&c, &d);
    printf("After swap by value:\nc = %5d,\td = %5d\n\n", c, d);   // Swap is successful because it's "passed by reference".

    return 0;
}


void swapbyval(int x, int y)
{
    int temp;

    temp = x;
    x = y;
    y = temp;

    return;
}


void swapbyref(int *x, int *y)
{
    int temp;

    temp = *x;
    *x = *y;
    *y = temp;

    return;
}



/*******************************************************************************

OUTPUT  :
---------

Before swap by value:
a =     5,	b =    10
After swap by value:
a =     5,	b =    10

Before swap by value:
c =     9,	d =     6
After swap by value:
c =     6,	d =     9


Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
