/*******************************************************************************
PROGRAM NAME    :   A053-pointers-ptrconst.c
TITLE           :   Program to Demonstrate Pointer Constants.
DATE            :   17/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the use of pointer constants in 3
                    different flavours - (1) Pointer to a constant, (2) Constant
                    pointer and (3) Constant pointer to a constant.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main()
{
    long val1 = 999999L, val2 = 888888L;
    const long *pval = &val1;           // Pointer to a constant.
    long *const pvalue = &val1;         // Constant pointer.
    const long *const ptr = &val1;      // Constant pointer to a constant.

    printf("Values \n");
    printf("*pval = %d,\n*pvalue = %d,\n*ptr = %d,\nval1 = %d,\nval2 = %d\n",
            *pval, *pvalue, *ptr, val1, val2);

    printf("Address \n");
    printf("pval = %d,\npvalue = %d,\nptr = %d,\n\naddressof(val1) = %d,\
            \naddressof(val2) = %d\n", pval, pvalue, ptr, &val1, &val2);

    // Trying to change value of *pval. Can't be done. Throws error "assignment
    // of read-only location *pval".
//    *pval = 777777L;
    // Trying to change value of pvalue. Can't be done. Throws error "assignment
    // of read-only variable pvalue".
//    pvalue = &val2;
    // Trying to change value of *ptr. Can't be done. Throws error "assignment
    // of read-only location *ptr".
//    *ptr = 777777L;
    // Trying to change value of ptr. Can't be done. Throws error "assignment
    // of read-only variable ptr".
//    ptr = &val2;

    printf("Trying to change value pval and *pvalue....");

    pval = &val2;
    *pvalue = 777777L;

    printf("Values \n");
    printf("*pval = %d,\n*pvalue = %d,\n*ptr = %d,\nval1 = %d,\nval2 = %d\n",
            *pval, *pvalue, *ptr, val1, val2);

    printf("Address \n");
    printf("pval = %d,\npvalue = %d,\nptr = %d,\n\naddressof(val1) = %d,\
            \naddressof(val2) = %d\n", pval, pvalue, ptr, &val1, &val2);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------
Values
*pval = 999999,
*pvalue = 999999,
*ptr = 999999,
val1 = 999999,
val2 = 888888
Address
pval = 2014477760,
pvalue = 2014477760,
ptr = 2014477760,

addressof(val1) = 2014477760,
addressof(val2) = 2014477768
Trying to change value pval and *pvalue....Values
*pval = 888888,
*pvalue = 777777,
*ptr = 777777,
val1 = 777777,
val2 = 888888
Address
pval = 2014477768,
pvalue = 2014477760,
ptr = 2014477760,

addressof(val1) = 2014477760,
addressof(val2) = 2014477768

Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.


*******************************************************************************/
