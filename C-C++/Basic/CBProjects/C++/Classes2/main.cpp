/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Example of Class with all functions for Rectangle class.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to demonstrate a class and its functions
                    using a class Rectangle and all it's possible/related
                    functions.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


class Rectangle
{
    private:
        int length;
        int breadth;

    public:
        Rectangle();

        Rectangle(int l,int b);

        Rectangle(Rectangle &r);

        int getLength(){return length;}

        int getBreadth(){return breadth;}

        void setLength(int l);

        void setBreadth(int b);

        int area();

        int perimeter();

        bool isSquare();

        ~Rectangle();

//        int area()
//        {
//            return length * breadth;
//        }
//
//        int perimeter()
//        {
//            return 2 * (length + breadth);
//        }
};


int main()
{
    Rectangle r1(10, 10);

    cout << "Length of r1 = " << r1.length << endl;
    cout << "Breadth of r1 = " << r1.breadth << endl;
    cout << "Length of r1 = " << r1.getLength() << endl;
    cout << "Breadth of r1 = " << r1.getBreadth() << endl;
    cout << "Area of r1 = " << r1.area() << endl;

    if(r1.isSquare())
        cout << "Yes, r1 is a square!" << endl;
}


Rectangle::Rectangle()
{
    length = 1;
    breadth = 1;

    cout << "Creating a Unit (1x1) Rectangle...." << endl;
}


Rectangle::Rectangle(int l, int b)
{
    length = l;
    breadth = b;

    cout << "Creating (" << length << "x" << breadth <<")" << " Rectangle...." << endl;
}


Rectangle::Rectangle(Rectangle &r)
{
    length = r.length;
    breadth = r.breadth;
}


void Rectangle::setLength(int l)
{
    length = l;
}


void Rectangle::setBreadth(int b)
{
    breadth = b;
}


int Rectangle::area()
{
    return length * breadth;
}


int Rectangle::perimeter()
{
    return 2 * (length + breadth);
}


bool Rectangle::isSquare()
{
    return length == breadth;
}


Rectangle::~Rectangle()
{
    cout << "Rectangle Destroyed....";
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
