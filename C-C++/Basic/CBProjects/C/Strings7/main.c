/*******************************************************************************
PROGRAM NAME    :   A042-strings-miscfuncs.c
TITLE           :   Program to Count the Letters, Digits and Punctuations.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program counts the number of letters, digits and
                    punctuations in a given string using string library
                    functions in C.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main()
{
    char buf[100];
    int nLetters = 0, nDigits = 0, nPunct = 0;

    printf("Enter an interesting string (less than %d) characters (without space) :\n", 100);
    scanf("%s", buf);

    int i = 0;

    while(buf[i])
    {
        if(isalpha(buf[i]))
            ++nLetters;
        else if(isdigit(buf[i]))
            ++nDigits;
        else if(ispunct(buf[i]))
            ++nPunct;
        ++i;
    }

    printf("\nYour string contained %d letters, %d digits and %d punctuation characters!\n", nLetters, nDigits, nPunct);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Run(1):

Enter an interesting string (less than 100) characters :
Hi. This is C. Number 1 here.

Your string contained 2 letters, 0 digits and 1 punctuation characters!

Process returned 0 (0x0)   execution time : 48.273 s
Press ENTER to continue.


Run(2):

Enter an interesting string (less than 100) characters (without space) :
Hi.I'mNewToC.Number1Here.

Your string contained 20 letters, 1 digits and 4 punctuation characters!

Process returned 0 (0x0)   execution time : 63.297 s
Press ENTER to continue.

*******************************************************************************/
