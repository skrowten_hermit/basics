/*
 * 04-compound-arith.cpp
 *Compound Arithmetic Operator
 *
 *  Created on: 02-Apr-2020
 *      Author: sreekanth.s
 */





#include <iostream>
using namespace std;
int main()
{
    int sum=10,x=5;
    sum+=x;
    cout<<sum<<endl;
    int fact=10,y=5;
    fact*=y;
    cout<<fact<<endl;
    return 0;
}
