/*******************************************************************************
PROGRAM NAME    :   A020-while-loops-demo.c
TITLE           :   Program to Demonstrate While Loop, Do-While Loop.
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstates while loop and do-while loop by
                    taking user inputs.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int main(void)
{
    int count = 1, num = 0;

    printf("Starting a while loop to print first 5 numbers....\n");
    while (count <= 5)
    {
        printf("%i\n", count);
        ++count;
    }

    printf("\nEnter inputs ('-1' to stop) :\n");
    scanf("%d", &num);

    while (num != -1)
    {
        scanf("%d", &num);
    }

    printf("\nStarting a while loop to print numbers upto 3....\n");

    do
    {
        printf("Num = %d\n", num);
        num++;
    }
    while(num < 4);

    printf("\nEnter inputs ('0' to stop) :\n");

    do
        scanf("%d", &num);
    while (num != 0);

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------

Starting a while loop to print first 5 numbers....
1
2
3
4
5

Enter inputs ('-1' to stop) :
8
-7
4
3
10
235
-1

Starting a while loop to print numbers upto 3....
Num = -1
Num = 0
Num = 1
Num = 2
Num = 3

Enter inputs ('0' to stop) :
8
9
-1
-7
29
0

Process returned 0 (0x0)   execution time : 43.847 s
Press ENTER to continue.

*******************************************************************************/
