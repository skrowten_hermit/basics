/*******************************************************************************
PROGRAM NAME    :   A080-files-printfilerev.c
TITLE           :   Program to Print the Contents of a File in Reverse.
DATE            :   20/10/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program uses functions fseek() and ftell() to write data to a file
                    and read strings and integers.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>

#define FILENAME "sample.txt"


int main()
{
    FILE *fp;
    int cnt = 0, i = 0;

    fp = fopen(FILENAME, "r");

    if (fp == NULL)
    {
        printf("File Open Error!!!");
        return -1;
    }

    // Move position to end of the file.
    fseek(fp, 0, SEEK_END);

    // Get the position of file pointer.
    cnt = ftell(fp);

    while (i < cnt)
    {
        i++;
        fseek(fp, -i, SEEK_END);
        printf("%c", fgetc(fp));
    }

    printf("\n");

    return 0;
}
