#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#define MAXSIZE 200
main()
{
int sockfd,newsockfd,retval;
socklen_t actuallen;
int recedbytes,sentbytes;
struct sockaddr_in serveraddr,clientaddr;
char temp[MAXSIZE],red[MAXSIZE];
char buff[MAXSIZE];
int a=0;
sockfd=socket(AF_INET,SOCK_STREAM,0);
if(sockfd==-1)
{
printf("\n socket creation error");
}
serveraddr.sin_family=AF_INET;
serveraddr.sin_port=htons(3388);
serveraddr.sin_addr.s_addr=htonl(INADDR_ANY);
retval=bind(sockfd, (struct sockaddr*)&serveraddr,sizeof(serveraddr));
if(retval==-1)
{
printf("Binding error");
close(sockfd);
}
retval=listen(sockfd,1);
if(retval==-1)
{
close(sockfd);
}
actuallen=sizeof(clientaddr);
newsockfd=accept(sockfd,(struct sockaddr*)&clientaddr,&actuallen);
if(newsockfd==-1)
{
close(sockfd);
}

while(1)
{
recedbytes=recv(newsockfd,buff,sizeof(buff),0);
if(recedbytes==-1)
{
close(sockfd);
close(newsockfd);
}
puts(buff);
if(strcmp(buff,"stop")==0)
{
break;
}
else
{

/*///////////file io//////////*/
FILE *fp;
int flag=0;
fp=fopen(buff,"r");
if(fp==NULL)
{
strcpy(temp,"FILE IS NOT PRESENT AT THE SERVER");
sentbytes=send(newsockfd,temp,sizeof(temp),0);
}
else
{
int c=0;
fscanf(fp,"%s",temp);
/*////////// read from file//////////*/
while(!feof(fp))
{
if(c==0)
{
strcpy(red,temp);
strcat(red," ");
c++;
}
else
{
strcat(red,temp);
strcat(red," ");
}
fscanf(fp,"%s",temp);
}

//printf("%s",red);
strcpy(buff,red);
//printf("%s",buff);
fclose(fp);
sentbytes=send(newsockfd,buff,sizeof(buff),0);
if(sentbytes==-1)
{
printf("!!");
close(sockfd);
}
}

}
}


close(sockfd);
close(newsockfd);

}
