% Envelope Detection Method
A = 0.5;
t = 0:0.0001:1;
modulatedSignal = [A+cos(2*pi*50*t)].*cos(2*pi*1000*t);

subplot(2,1,1);
plot(t,modulatedSignal,'g');

set(gca,'xlim',[0 0.1]);

title('Modulated signal');
xlabel('Seconds'); 
ylabel('Amplitude');

y = hilbert(modulatedSignal);
envelop = abs(y);

subplot(2,1,2)
plot(t,modulatedSignal,'g');
hold on;
plot(t,envelop,'b','linewidth',2);
plot(t,-envelop,'b','linewidth',2);
title('Modulated signal + Envelop');
xlabel('Seconds');
ylabel('Amplitude');
set(gca,'xlim',[0 0.1]);