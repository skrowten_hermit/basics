/*******************************************************************************
PROGRAM NAME    :   A032-func-global-var.c
TITLE           :   Program to Demonstrate Global Variable in Functions. [TODO]
DATE            :   05/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates the usage of a global variable in
                    Functions.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>


int myGlobal = 0;


int main()
{
    printf("Hello world!\n");
    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
