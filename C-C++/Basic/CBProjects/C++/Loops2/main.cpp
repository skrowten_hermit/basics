/*******************************************************************************
PROGRAM NAME    :
TITLE           :   Program to Test all Loops.
DATE            :
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This is a program to test all types of loops available in
                    C++.

                    This has been taken from the Udemy Course --> Learn C++
                    Programming : Beginner to Advance - Deep Dive in C++ by
                    Abdul Bari. Mostly the program is unchanged with minimalist
                    changes if there are any at all.
*******************************************************************************/


#include <iostream>

using namespace std;


int main()
{
    int a = 0;

    while(a < 10)
    {
        cout << "a " << a;
        a++;
    }

    int b = 0;

    do
    {
        cout << "b " << b;
        b++;
    }while(b < 10);

    for(int c = 0; c < 10; i++)
    {
        cout << "c " << c;
    }

    return 0;
}



/*******************************************************************************

OUTPUT  :
---------



*******************************************************************************/
