/*******************************************************************************
PROGRAM NAME    :   A039-strings-manipfuncs.c
TITLE           :   To Demonstrate Some Basic String Manipulation Functions.
DATE            :   08/09/2019.
AUTHOR          :   Sreekanth Sreekumar.
DESCRIPTION     :   This program demonstrates some basic string manipulation
                    functions from the C library.
                    This has been taken from the Udemy Course --> C Programming
                    For Beginners - Master the C Language by Jason Fedin. Mostly
                    the program is unchanged with minimalist changes if there
                    are any at all.
*******************************************************************************/



#include <stdio.h>
#include <string.h>


int main(void)
{
    char myString[100] = "my string";

    printf("strlen():\n\n");
    printf("The length of the string \"%s\" is : %d\n\n\n", myString,
            strlen(myString));

    char strTest[] = "test string";

//    str = "new test string";  // This is NOT ALLOWED!

//    printf("Test string created as \"%s\"\n", str);
//
//    strcpy(myString, "Reinitializing my string");             //ERROR!
//    printf("The new reinitialized string is : \"%s\"\n", myString);

//    strcpy(myString, str);                                   //ERROR!
//    printf("The copied string is : \"%s\"\n", myString);
//    strncpy(myString, str, sizeof(myString) - 1);            //ERROR!
//    printf("The copied string is : \"%s\"\n", myString);

    printf("strcat():\n\n");
    printf("strTest : %s\n", strTest);
    printf("myString : %s\n", myString);
    strcat(myString, " AS ");
    printf("myString (after concat) : %s\n", myString);
    printf("strTest (after concat) : %s\n\n", strTest);
    // Doesn't produce the desired output!!! If we do not define the size of the
    // character array for the string.
    strcat(myString, strTest);

    printf("Concatenated new string (myString + strTest) : %s\n\n\n", myString);

    char src[50], dst[50];

    printf("strcpy():\n\n");
    strcpy(src, "This is source ");
    strcpy(dst, "This is destination");
    printf("src : %s\n", src);
    printf("dst : %s\n\n\n", dst);

    printf("strcat():\n\n");
    strcat(src, dst);
    printf("New src : %s\n", src);
    printf("New dst : %s\n\n\n", dst);

    printf("strcpy():\n\n");
    strcpy(src, "This is source ");
    strcpy(dst, "This is destination");
    printf("New src : %s\n", src);
    printf("New dst : %s\n\n\n", dst);

    printf("strncat():\n\n");
    strncat(src, dst, 15);
    printf("New src : %s\n", src);
    printf("New dst : %s\n\n\n", dst);

    printf("strncmp():\n\n");
    printf("strcmp(\"A\",\"A\") is : ");
    printf("%2d\n", strcmp("A", "A"));
    printf("strcmp(\"A\",\"B\") is : ");
    printf("%2d\n", strcmp("A", "B"));
    printf("strcmp(\"B\",\"A\") is : ");
    printf("%2d\n", strcmp("B", "A"));
    printf("strcmp(\"Z\",\"a\") is : ");
    printf("%2d\n", strcmp("Z", "a"));
    printf("strcmp(\"APPLE\",\"APPLE\") is : ");
    printf("%2d\n", strcmp("APPLE", "APPLE"));
    printf("strcmp(\"apples\",\"apple\") is : ");
    printf("%2d\n", strcmp("apples", "apple"));
    printf("strcmp(\"apple\",\"apples\") is : ");
    printf("%2d\n\n", strcmp("apple", "apples"));

    if (strncmp("astronomy", "astro", 5) == 0)
        printf("\nFound \"astro\" in \"astronomy\"!\n\n");
    if (strncmp("astounding", "astro", 5) == 0)
            printf("\nFound \"astro\" in \"astounding\"!\n\n");

        return 0;
    }



/*******************************************************************************

OUTPUT  :
---------

strlen():

The length of the string "my string" is : 9


strcat():

strTest : test string
myString : my string
myString (after concat) : my string AS
strTest (after concat) : test string

Concatenated new string (myString + strTest) : my string AS test string


strcpy():

src : This is source
dst : This is destination


strcat():

New src : This is source This is destination
New dst : This is destination


strcpy():

New src : This is source
New dst : This is destination


strncat():

New src : This is source This is destina
New dst : This is destination


strncmp():

strcmp("A","A") is :  0
strcmp("A","B") is : -1
strcmp("B","A") is :  1
strcmp("Z","a") is : -1
strcmp("APPLE","APPLE") is :  0
strcmp("apples","apple") is :  1
strcmp("apple","apples") is : -1


Found "astro" in "astronomy"!


Process returned 0 (0x0)   execution time : 0.002 s
Press ENTER to continue.

*******************************************************************************/
