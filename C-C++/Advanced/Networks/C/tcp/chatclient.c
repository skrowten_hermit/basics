#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/stat.h>
#include<fcntl.h>
#define MAXSIZE 50
main()
{
pid_t pid;
int sockfd,retval;
int recedbytes,sentbytes;
struct sockaddr_in serveraddr;
char buff[MAXSIZE],read[MAXSIZE];
sockfd=socket(AF_INET,SOCK_STREAM,0);
if(sockfd==-1)
{
printf("\n socket creation error");
}
fflush(stdout);
//printf("%i",sockfd);
serveraddr.sin_family=AF_INET;
serveraddr.sin_port=htons(3387);
serveraddr.sin_addr.s_addr=inet_addr("127.0.0.1");
retval=connect(sockfd,(struct sockaddr*)&serveraddr,sizeof(serveraddr));
if(retval==-1)
{
printf("\n connection error");
}
pid=fork();
////////////send////////////
if(pid==-1)
{
perror("fork failure");
}
else if(pid==0)
{
while(1)
{
//printf("enter the text:\n");

gets(buff);

sentbytes=send(sockfd,buff,sizeof(buff),0);
if(sentbytes==-1)
{
printf("!!");
close(sockfd);
}
if(strcmp(buff,"stop")==0)
{
break;
close(sockfd);
}
}
}
else
{
////////////recv////////////
while(1)
{
recedbytes=recv(sockfd,buff,sizeof(buff),0);
if(recedbytes==-1)
{
close(sockfd);
}
puts(buff);
if(strcmp(buff,"stop")==0)
{
break;
close(sockfd);
}
}
}
close(sockfd);



}
